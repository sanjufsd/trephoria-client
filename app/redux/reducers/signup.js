import { SIGNUP_USER_DETAILS, REMOVE_SIGNUP_USER_DETAILS } from '../constants';

const init = {};
const signupReducer = (state = init, { type, payload }) => {
  switch (type) {
    case SIGNUP_USER_DETAILS:
      return { ...state, ...payload };
    case REMOVE_SIGNUP_USER_DETAILS:
      return {};
    default:
      return state;
  }
};

export default signupReducer;
