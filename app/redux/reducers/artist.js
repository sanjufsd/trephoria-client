import _ from 'lodash';
import { SELECTED_ARTIST } from '../constants';

const defaultState = {
  artist: {},
  artistBio: {},
};
const artistReducer = (state = defaultState, { type, payload }) => {
  const newState = _.cloneDeep(payload);
  let prevObj = {};
  switch (type) {
    case SELECTED_ARTIST:
      prevObj = {
        artist: newState.artist,
        artistBio: newState.artistBio,
        ...payload,
      };
      window.localStorage.setItem('activeArtist', JSON.stringify(prevObj));

      return prevObj;
    default:
      return state;
  }
};

const activeArtistInitialState = {
  activeArtist: defaultState,
};

export { artistReducer, activeArtistInitialState };
