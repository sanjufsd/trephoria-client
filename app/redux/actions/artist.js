import { SELECTED_ARTIST } from '../constants';

export const selectedArtist = payload => ({
  type: SELECTED_ARTIST,
  payload,
});
