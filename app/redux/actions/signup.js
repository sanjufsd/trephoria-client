import { SIGNUP_USER_DETAILS, REMOVE_SIGNUP_USER_DETAILS } from '../constants';

export const signupDetails = payload => ({
  type: SIGNUP_USER_DETAILS,
  payload,
});

export const removeSignupDetails = () => ({
  type: REMOVE_SIGNUP_USER_DETAILS,
});
