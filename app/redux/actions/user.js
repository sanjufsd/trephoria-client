import { SAVE_USER } from '../constants';

export const saveUser = payload => ({
  type: SAVE_USER,
  payload,
});
