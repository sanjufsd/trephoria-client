/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from 'utils/history';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import signupReducer from './redux/reducers/signup';
import userReducer from './redux/reducers/user';
import { artistReducer } from './redux/reducers/artist';
/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    language: languageProviderReducer,
    router: connectRouter(history),
    signup: signupReducer,
    user: userReducer,
    activeArtist: artistReducer,
    ...injectedReducers,
  });

  return rootReducer;
}
