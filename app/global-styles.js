import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }
  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  #app {
    min-height: 100%;
    min-width: 100%;
  }
  .app {
    min-height: 100vh;
    width: 100%;
    position: relative;
  }
  p,
  label {
    line-height: 1.5em;
  }
  .slick-slider{
    z-index: 2;
    margin: 0 -10px;
  }
  .custom-control-label {
    color: #2b2b2b;
    font-size: 12px;
  }
  .custom-checkbox, .custom-radio {
    .custom-control-label::before {
      top: 0 !important;
      outline: 0 !important;
      box-shadow: none !important;
      border-color: #e0e3e5;
    }
    .custom-control-input:checked ~ .custom-control-label::after {
      top: 0 !important;
      outline: 0 !important;
      box-shadow: none !important;
    }
    .custom-control-input:checked ~ .custom-control-label::before {
      border-color: #9b5ccd;
      background-color: #9b5ccd;
    }
  }
  .transparentButton {
    width: 100%;
    background: transparent;
    padding: 0;
    border: 0;
    outline: 0 !important;
    text-align: left;
  }
  .myConcerts {
    .modal-header {
      padding: 15px 30px;
      .modal-title {
        font-size: 18px;
        font-weight: bold;
      }
      button {
        outline: 0 !important;
      }
    }
    .modal-body {
      padding: 0;
    }
  }
  .slick-slide img {
    display: inline-block !important;
  }
`;

export const Div = styled.div``;
export const Table = styled.table``;
export const THead = styled.thead``;
export const TBody = styled.tbody``;
export const TRow = styled.tr``;
export const THeading = styled.th``;
export const TData = styled.td``;
