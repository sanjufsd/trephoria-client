import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';

const Filter = props => {
  const { colorType } = props;
  return (
    <S.FilterWrapper colorType={colorType}>
      <S.FilterIcon
        src={colorType === 'primary' ? c.filterColoredIcon : c.filterWhiteIcon}
      />{' '}
      Filter
    </S.FilterWrapper>
  );
};

Filter.defaultProps = {
  colorType: 'primary',
};

Filter.propTypes = {
  colorType: PropTypes.string,
};

export default Filter;
