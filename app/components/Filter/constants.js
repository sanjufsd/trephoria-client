/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const filterWhiteIcon = `${apiUrl}/images/filter_icon.png`;
export const filterColoredIcon = `${apiUrl}/images/genre_filter.png`;
