import styled from 'styled-components';

export const FilterWrapper = styled.button`
  border-radius: 20px;
  border: 1px solid #983fcc;
  outline: 0 !important;
  background: transparent;
  padding: 5px 15px 4px;
  font-size: 11px;
  color: ${props => (props.colorType === 'primary' ? '#983fcc' : 'white')};
`;

export const FilterIcon = styled.img`
  width: 10px;
  vertical-align: middle;
  margin: -2px 5px 0 0;
`;
