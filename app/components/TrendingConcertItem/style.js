import styled from 'styled-components';

export const ConcertItem = styled.div`
  background: #2a262b;
  border-radius: 6px;
  z-index: 2;
`;

export const TitleWrapper = styled.div`
  padding: 20px;
`;

export const Title = styled.h3`
  font-size: 18px;
  line-height: 1.17;
  margin-bottom: 2px;
`;

export const Capacity = styled.p`
  font-size: 11px;
  color: #747474;
  margin: 0;
`;

export const ConcertImage = styled.div`
  position: relative;
  overflow: hidden;
  border-radius: 5px;
  img {
    width: 100%;
    height: 200px;
    object-fit: cover;
  }
`;

export const City = styled.label`
  position: absolute;
  top: 20px;
  left: 20px;
  border-radius: 3px;
  background-color: #6f6eff;
  padding: 8px 17px;
  font-size: 12px;
  margin: 0;
`;

export const Img = styled.img`
  width: 100%;
  height: 200px;
  object-fit: cover;
`;

export const ConcertDescription = styled.div`
  padding: 20px;
  a {
    width: 100%;
    height: 40px;
    border-radius: 20px;
    border: solid 1px #b04dea;
    background-color: rgba(176, 77, 234, 0.16);
    text-align: center;
    font-size: 12px;
    padding: 10px 0;
    display: block;
    color: white;
    text-decoration: none;
    outline: 0;
  }
`;

export const Description = styled.p`
  font-size: 12px;
  line-height: 1.33;
  margin-bottom: 20px;
`;

export const ConcertLocation = styled.div`
  padding: 20px;
  border-top: 1px solid #424242;
`;

export const Location = styled.p`
  font-size: 12px;
  line-height: 1;
  margin: 0 0 3px;
`;

export const Phone = styled.span`
  opacity: 0.5;
  font-size: 12px;
  line-height: 1;
`;
