import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as S from './style';

const TrendingConcertItem = props => {
  const {
    item: {
      title,
      capacity,
      image,
      city,
      description,
      upcomingConcrets,
      location,
      phone,
      url,
    },
  } = props;

  return (
    <S.ConcertItem>
      <S.TitleWrapper>
        <S.Title>{title}</S.Title>
        <S.Capacity>(Capacity: {capacity})</S.Capacity>
      </S.TitleWrapper>
      <S.ConcertImage>
        <S.City>{city}</S.City>
        <S.Img src={image} alt="" />
      </S.ConcertImage>
      <S.ConcertDescription>
        <S.Description>{description}</S.Description>
        <Link to={url}>{upcomingConcrets} Upcoming Concerts</Link>
      </S.ConcertDescription>
      <S.ConcertLocation>
        <S.Location>{location}</S.Location>
        <S.Phone>{phone}</S.Phone>
      </S.ConcertLocation>
    </S.ConcertItem>
  );
};

TrendingConcertItem.propTypes = {
  item: PropTypes.object,
};

export default TrendingConcertItem;
