import styled from 'styled-components';

export const Wrapper = styled.div`
  position: absolute;
  left: ${props => (props.position === 'left' ? '-70px' : 'auto')};
  right: ${props => (props.position === 'right' ? '-70px' : 'auto')};
  top: 50%;
  transform: translate(0, -50%);
`;

export const Button = styled.button`
  background: transparent;
  border: 0;
  outline: 0 !important;
`;

export const Img = styled.img`
  width: 50px;
`;
