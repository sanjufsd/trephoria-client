import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';

const SlickSliderArrow = props => {
  const { onClick, icon, position } = props;
  return (
    <S.Wrapper position={position}>
      <S.Button onClick={onClick}>
        <S.Img src={icon} alt="" />
      </S.Button>
    </S.Wrapper>
  );
};

SlickSliderArrow.propTypes = {
  onClick: PropTypes.func,
  icon: PropTypes.string,
  position: PropTypes.string,
};

export default SlickSliderArrow;
