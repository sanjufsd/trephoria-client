import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import * as S from './style';
import * as u from '../../config/url';
import ErrorBoundary from '../ErrorBoundary';
import Header from '../Header';
import Search from '../Search';
import UserTab from '../UserTab';
import ActivitiesContent from '../../containers/User/Activities/content';
import ConcertsContent from '../../containers/User/Concerts/content';
import ArtistsContent from '../../containers/User/Artists/content';
import BookmarksContent from '../../containers/User/Bookmarks/content';

const UserWrapper = props => {
  const {
    location: { pathname },
  } = props;
  let component = '';
  if (pathname === u.userActivities) {
    component = <ActivitiesContent {...props} />;
  } else if (pathname === u.userConcerts) {
    component = <ConcertsContent {...props} />;
  } else if (pathname === u.userArtists) {
    component = <ArtistsContent {...props} />;
  } else if (pathname === u.userBookmarks) {
    component = <BookmarksContent {...props} />;
  }
  return (
    <S.Wrapper>
      <ErrorBoundary>
        <Header {...props} bg />
      </ErrorBoundary>
      <ErrorBoundary>
        <Search full />
      </ErrorBoundary>
      <S.UserWrapper>
        <Container>
          <Row>
            <Col lg="3">
              <ErrorBoundary>
                <UserTab activeUrl={pathname} />
              </ErrorBoundary>
            </Col>
            <Col lg="9">
              <ErrorBoundary>
                {component && <ErrorBoundary>{component}</ErrorBoundary>}
              </ErrorBoundary>
            </Col>
          </Row>
        </Container>
      </S.UserWrapper>
    </S.Wrapper>
  );
};

UserWrapper.propTypes = {
  location: PropTypes.object,
};

export default UserWrapper;
