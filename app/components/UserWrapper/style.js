import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 80px 0 0 0;
  min-height: calc(100vh - 97px);
`;

export const UserWrapper = styled.div`
  margin-top: 30px;
`;
