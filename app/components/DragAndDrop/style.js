import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  border: 1px dashed #ff5853;
  border-radius: 8px;
  padding: 80px;
  text-align: center;
  background: #fffafa;
  position: relative;
  overflow: hidden;
  ${props =>
    props.dragging &&
    css`
      &:after {
        content: 'Drop here';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: #fffafa;
        z-index: 10;
        font-size: 13px;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #0909096b;
      }
    `}
`;

export const UploadIcon = styled.img``;

export const DragAndDropText = styled.p`
  font-size: 16px;
  margin: 15px 0 5px;
`;

export const Or = styled.p`
  font-size: 12px;
  color: #b9b9b9;
  margin: 0;
`;

export const ChooseFiles = styled.div`
  border-radius: 20px;
  background-color: #111111;
  color: white;
  font-size: 14px;
  position: relative;
  overflow: hidden;
  width: 130px;
  margin: 15px auto 0;
  padding: 8px 0;
`;

export const FileInput = styled.input`
  position: absolute;
  width: 100%;
  height: 100%;
  opacity: 0;
`;

export const ChooseFiletext = styled.span``;

export const Note = styled.p`
  opacity: 0.3;
  font-size: 12px;
  color: #090909;
  margin: 5px 0 0;
`;
