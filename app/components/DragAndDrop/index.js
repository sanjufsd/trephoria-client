// import React, { Fragment, useState, useEffect, useRef } from 'react';
// import * as S from './style';
// import * as c from './constants';

// const fileTypes = ['image/png', 'image/jpeg', 'application/pdf'];

// const DragAndDrop = () => {
//   const [dragging, setDragging] = useState(false);
//   const [files, setFiles] = useState([]);
//   const [ignoredFiles, setIgnoredFiles] = useState([]);
//   const [dragCounter, setCounter] = useState(0);
//   const uploadRef = useRef(null);

//   useEffect(() => {
//     uploadRef.current.addEventListener('dragenter', handleDragIn);
//     uploadRef.current.addEventListener('dragleave', handleDragOut);
//     uploadRef.current.addEventListener('dragover', handleDrag);
//     uploadRef.current.addEventListener('drop', handleDrop);
//     return () => {
//       uploadRef.current.removeEventListener('dragenter', handleDragIn);
//       uploadRef.current.removeEventListener('dragleave', handleDragOut);
//       uploadRef.current.removeEventListener('dragover', handleDrag);
//       uploadRef.current.removeEventListener('drop', handleDrop);
//     };
//   }, []);

//   const handleDrag = e => {
//     e.preventDefault();
//     e.stopPropagation();
//   };

//   const handleDragIn = e => {
//     e.preventDefault();
//     e.stopPropagation();
//     setCounter(dragCounter + 1);
//     if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
//       setDragging(true);
//     }
//   };

//   const handleDragOut = e => {
//     e.preventDefault();
//     e.stopPropagation();
//     setCounter(dragCounter - 1);
//     if (dragCounter === 0) {
//       setDragging(false);
//     }
//   };

//   const handleDrop = e => {
//     e.preventDefault();
//     e.stopPropagation();
//     setDragging(false);
//     if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
//       setFiles(e.dataTransfer.files);
//       e.dataTransfer.clearData();
//       setCounter(0);
//     }
//   };

//   const handleChange = e => {
//     const inputFiles = e.target.files;
//     if (inputFiles.length > 0) {
//       setFiles(inputFiles);
//     }
//   };

//   console.log({ files });

//   return (
//     <Fragment>
//       <S.Wrapper ref={uploadRef} dragging={dragging}>
//         <S.UploadIcon src={c.uploadIcon} />
//         <S.DragAndDropText>Drag & Drop files here</S.DragAndDropText>
//         <S.Or>or</S.Or>
//         <S.ChooseFiles>
//           <S.FileInput type="file" onChange={handleChange} />
//           <S.ChooseFiletext>Choose Files</S.ChooseFiletext>
//         </S.ChooseFiles>
//       </S.Wrapper>
//       <S.Note>*Upload JPG or PDF</S.Note>
//     </Fragment>
//   );
// };

// export default DragAndDrop;
