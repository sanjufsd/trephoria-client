/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const closeRed = `${apiUrl}/images/close-red-white.svg`;
export const plsuIcon = `${apiUrl}/images/plus.svg`;
export const icons = [
  {
    title: 'Facebook',
    image: `${apiUrl}/images/facebook.svg`,
  },
  {
    title: 'Instagram',
    image: `${apiUrl}/images/instagram.svg`,
  },
  {
    title: 'Twitter',
    image: `${apiUrl}/images/twitter.svg`,
  },
  {
    title: 'Spotify',
    image: `${apiUrl}/images/spotify.svg`,
  },
  {
    title: 'Bandsintown',
    image: `${apiUrl}/images/bandsintown.svg`,
  },
];
