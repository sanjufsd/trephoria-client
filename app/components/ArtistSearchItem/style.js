import styled from 'styled-components';

export const Wrapper = styled.div``;

export const ArtistImage = styled.div`
  position: relative;
  overflow: hidden;
`;

export const BgImage = styled.img`
  width: 100%;
`;

export const SocialLink = styled.a`
  position: absolute;
  bottom: 5px;
  left: 5px;
  width: 25px;
  height: 25px;
  display: block;
  z-index: 1;
`;

export const Socialicon = styled.img`
  width: 25px;
  vertical-align: top;
`;

export const Name = styled.h3`
  font-size: 10px;
  text-align: center;
  margin: 8px 0 0;
  font-weight: bold;
  letter-spacing: 0.2px;
`;

export const AddLikeButton = styled.button`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  background: white;
  border: 0;
  padding: 10px 0;
  width: 35px;
  height: 35px;
  border-radius: 50%;
  outline: 0 !important;
  text-align: center;
`;

export const PlusIcon = styled.img`
  width: 15px;
  vertical-align: top;
`;

export const RemoveLikeButton = styled.button`
  position: absolute;
  top: 5px;
  right: 5px;
  padding: 0;
  border: 0;
  outline: 0 !important;
  border-radius: 50%;
  background: transparent;
  width: 25px;
  height: 25px;
  z-index: 1;
`;

export const RemoveIcon = styled.img`
  vertical-align: top;
  width: 100%;
`;

export const Layer = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #772d8a96;
  z-index: 0;
`;
