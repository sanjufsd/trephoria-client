import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';

const ArtistSearchItem = props => {
  const {
    item,
    item: { id, name, image, social },
    addArtist,
    removeArtist,
    selectedArtists,
  } = props;
  const icon = c.icons.find(_item => _item.title === social);
  const selected = selectedArtists.find(_item => _item.id === id);
  return (
    <S.Wrapper>
      <S.ArtistImage>
        <S.BgImage src={image} />
        <S.SocialLink href="#0">
          <S.Socialicon src={icon.image} />
        </S.SocialLink>
        {!selected && (
          <S.AddLikeButton
            onClick={() => {
              addArtist(item);
            }}
          >
            <S.PlusIcon src={c.plsuIcon} />
          </S.AddLikeButton>
        )}
        {selected && (
          <S.RemoveLikeButton
            onClick={() => {
              removeArtist(item);
            }}
          >
            <S.RemoveIcon src={c.closeRed} />
          </S.RemoveLikeButton>
        )}
        {selected && <S.Layer />}
      </S.ArtistImage>
      <S.Name>{name}</S.Name>
    </S.Wrapper>
  );
};

ArtistSearchItem.propTypes = {
  item: PropTypes.object,
  addArtist: PropTypes.func,
  removeArtist: PropTypes.func,
  selectedArtists: PropTypes.array,
};

export default ArtistSearchItem;
