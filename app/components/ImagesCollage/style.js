import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -5px 20px;
`;

export const ImageWrapper = styled.div`
  width: calc(33.3333% - 10px);
  height: 80px;
  border-radius: 10px;
  overflow: hidden;
  margin: 5px;
  position: relative;
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Button = styled.button`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1;
  width: 100%;
  height: 100%;
  background: #00000085;
  border: 0;
  outline: 0;
  color: white;
  font-size: 15px;
`;
