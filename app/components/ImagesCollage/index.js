import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';

const ImagesCollage = props => {
  const { images } = props;
  const [moreOption, setMoreOption] = useState(false);
  const [limitImages, setImages] = useState(images);
  useEffect(() => {
    if (images.length > 6) {
      setImages(images.slice(0, 6));
      setMoreOption(true);
    }
  }, []);
  return (
    <S.Wrapper>
      {limitImages.map((item, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <S.ImageWrapper key={index}>
          <S.Image src={item.image} />
          {moreOption && index === 5 && (
            <S.Button
              onClick={() => {
                setImages(images);
                setMoreOption(false);
              }}
            >
              +{images.length - limitImages.length}
            </S.Button>
          )}
        </S.ImageWrapper>
      ))}
    </S.Wrapper>
  );
};

ImagesCollage.propTypes = {
  images: PropTypes.array,
};

export default ImagesCollage;
