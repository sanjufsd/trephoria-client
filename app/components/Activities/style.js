import styled, { css } from 'styled-components';

export const ActivityWrapper = styled.div`
  margin-bottom: 30px;
`;

export const ActivityHead = styled.div`
  margin-bottom: 13px;
`;

export const ActivityTitle = styled.h3`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 0;
  display: inline-block;
`;

export const ActivityFilter = styled.div`
  float: right;
`;

export const ActivityFilterTabs = styled.ul`
  list-style: none;
  display: block;
  padding: 0;
  margin: 0 0 13px;
`;

export const ActivityFilterTab = styled.li`
  font-size: 10px;
  color: #686868;
  display: inline-block;
  opacity: 0.5;
  border-radius: 20px;
  border: solid 1px #686868;
  padding: 3px 15px;
  margin: 0 10px 0 0;
  cursor: pointer;
  ${props =>
    props.active &&
    css`
      border-color: #983fcc;
      color: #983fcc;
      opacity: 1;
    `}
`;

export const ActivityContent = styled.div`
  border: 1px solid #eee;
  padding: 20px;
`;

export const ActivityGroup = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -5px;
`;

export const ActivityItem = styled.div`
  width: 25%;
  padding: 0 5px;
`;

export const LoadMore = styled.div`
  text-align: center;
`;

export const LoadMoreButton = styled.button`
  color: #983fcc;
  background: transparent;
  font-size: 12px;
  border: 0;
  outline: 0 !important;
`;

export const Wrapper = styled.div`
  border: solid 1px #e4e4e4;
  background-color: #ffffff;
  position: relative;
  overflow: hidden;
  margin-bottom: 10px;
`;

export const Top = styled.div`
  position: relative;
  min-height: 20px;
`;

export const MainImage = styled.img`
  width: 100%;
`;

export const SocialLink = styled.a`
  position: absolute;
  top: 0;
  right: 0;
`;

export const SocialIcon = styled.img``;

export const Bottom = styled.div`
  padding: 15px;
`;

export const TitleWrapper = styled.div`
  display: inline-block;
  width: 100%;
  position: relative;
  margin-bottom: 10px;
`;

export const DisplayPic = styled.img`
  height: 30px;
  display: inline-block;
  margin-right: 10px;
`;

export const Title = styled.h4`
  font-size: 12px;
  font-weight: bold;
  color: #3e4c5b;
  text-transform: uppercase;
  display: inline-block;
`;

export const DropDown = styled.div`
  position: absolute;
  right: 0;
  top: 0;
`;

export const DropDownIcon = styled.img`
  cursor: pointer;
  padding: 0 0 0 10px;
`;

export const Description = styled.p`
  font-size: 10px;
  color: #989898;
  margin-bottom: 10px;
`;

export const Tags = styled.ul`
  font-size: 9px;
  color: #000000;
  list-style: none;
  padding: 0;
  margin: 0;
`;

export const Tag = styled.li`
  display: inline-block;
  margin-right: 10px;
`;
