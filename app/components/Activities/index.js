/* eslint-disable react/no-array-index-key */
import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import Filter from '../Filter';
import Activity from './activity';
import ErrorBoundary from '../ErrorBoundary';

const columnBlock = () => {
  const windowWidth = window.innerWidth;
  let size = 4;
  if (windowWidth < 1200) {
    size = 3;
  } else if (windowWidth < 992) {
    size = 2;
  }
  return Array.from({ length: size }).map((item, index) => index);
};

const Activities = props => {
  const column = columnBlock();
  const { filterTab } = props;
  const [activeTab, setActiveTab] = useState(1);
  return (
    <S.ActivityWrapper>
      <S.ActivityHead>
        {filterTab ? (
          <S.ActivityTitle>All Activities</S.ActivityTitle>
        ) : (
          <Fragment>
            <S.ActivityTitle>Band Activities</S.ActivityTitle>
            <S.ActivityFilter>
              <ErrorBoundary>
                <Filter />
              </ErrorBoundary>
            </S.ActivityFilter>
          </Fragment>
        )}
      </S.ActivityHead>
      {filterTab && (
        <S.ActivityFilterTabs>
          {c.activityFilters.map(item => (
            <S.ActivityFilterTab
              key={item.id}
              active={activeTab === item.id}
              onClick={() => setActiveTab(item.id)}
            >
              {item.title}
            </S.ActivityFilterTab>
          ))}
        </S.ActivityFilterTabs>
      )}
      <S.ActivityContent>
        <S.ActivityGroup>
          {column.map(col => {
            const items = c.activities.filter(
              (item, index) => index % column.length === col,
            );
            return (
              <S.ActivityItem key={col}>
                {items.map((item, index) => (
                  <Fragment key={index}>
                    <ErrorBoundary>
                      <Activity item={item} />
                    </ErrorBoundary>
                  </Fragment>
                ))}
              </S.ActivityItem>
            );
          })}
        </S.ActivityGroup>
        <S.LoadMore>
          <S.LoadMoreButton>Load more</S.LoadMoreButton>
        </S.LoadMore>
      </S.ActivityContent>
    </S.ActivityWrapper>
  );
};

Activities.propTypes = {
  filterTab: PropTypes.bool,
};

export default Activities;
