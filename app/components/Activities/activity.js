/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';

const Activity = props => {
  const {
    item: {
      mainImage,
      profilePic,
      title,
      description,
      tags,
      instagram,
      facebook,
      spotify,
      twitter,
    },
  } = props;
  let sociallink = '';
  let socialIcon = '';
  if (instagram) {
    sociallink = instagram;
    socialIcon = c.socialIcon.instagram;
  } else if (facebook) {
    sociallink = facebook;
    socialIcon = c.socialIcon.facebook;
  } else if (twitter) {
    sociallink = twitter;
    socialIcon = c.socialIcon.twitter;
  } else if (spotify) {
    sociallink = spotify;
    socialIcon = c.socialIcon.spotify;
  }
  return (
    <S.Wrapper>
      {(mainImage || sociallink) && (
        <S.Top>
          {mainImage && <S.MainImage src={mainImage} />}
          {sociallink && (
            <S.SocialLink href={sociallink}>
              <S.SocialIcon src={socialIcon} />
            </S.SocialLink>
          )}
        </S.Top>
      )}
      <S.Bottom>
        <S.TitleWrapper>
          <S.DisplayPic src={profilePic} />
          <S.Title>{title}</S.Title>
          <S.DropDown>
            <S.DropDownIcon src={c.dropdownIcon} />
          </S.DropDown>
        </S.TitleWrapper>
        <S.Description>{description}</S.Description>
        <S.Tags>
          {tags &&
            tags.map((item, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <S.Tag key={index}>{item}</S.Tag>
            ))}
        </S.Tags>
      </S.Bottom>
    </S.Wrapper>
  );
};

Activity.propTypes = {
  item: PropTypes.object,
};

export default Activity;
