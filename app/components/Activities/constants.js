/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const socialIcon = {
  facebook: `${apiUrl}/images/facebook_feed.svg`,
  instagram: `${apiUrl}/images/instagram_feed.svg`,
  spotify: `${apiUrl}/images/spotify_feed.svg`,
  twitter: `${apiUrl}/images/twitter_feed.svg`,
};
export const dropdownIcon = `${apiUrl}/images/dropdownIcon.svg`;
export const activities = [
  {
    mainImage: `${apiUrl}/images/artist/2.jpg`,
    profilePic: `${apiUrl}/images/artist/10.jpg`,
    title: 'Group',
    description: 'Lorem Ipsum is simply dummy text',
    tags: ['#tags', '#socialtags', '#specialtags'],
  },
  {
    mainImage: `${apiUrl}/images/artist/6.jpg`,
    profilePic: `${apiUrl}/images/artist/8.jpg`,
    title: 'USER NAME',
    description: 'Lorem Ipsum is simply dummy text',
    tags: ['#tags', '#socialtags', '#specialtags'],
    instagram: '#0',
  },
  {
    profilePic: `${apiUrl}/images/artist/9.jpg`,
    title: 'CONCERT',
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    tags: ['#tags', '#socialtags', '#specialtags'],
  },
  {
    profilePic: `${apiUrl}/images/artist/10.jpg`,
    title: 'GROUP',
    description: 'Lorem Ipsum is simply dummy text',
    tags: ['#tags', '#socialtags', '#specialtags'],
  },
  {
    mainImage: `${apiUrl}/images/artist/3.jpg`,
    profilePic: `${apiUrl}/images/artist/5.jpg`,
    title: 'ARTIST',
    description: 'Lorem Ipsum is simply dummy text',
    tags: ['#tags', '#socialtags', '#specialtags'],
    spotify: '#0',
  },
  {
    profilePic: `${apiUrl}/images/artist/7.jpg`,
    title: 'ARTIST',
    description: 'Lorem Ipsum is simply dummy text',
    twitter: '#0',
  },
  {
    profilePic: `${apiUrl}/images/artist/9.jpg`,
    title: 'CONCERT',
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    tags: ['#tags', '#socialtags', '#specialtags'],
  },
  {
    mainImage: `${apiUrl}/images/artist/11.jpg`,
    profilePic: `${apiUrl}/images/artist/12.jpg`,
    title: 'ARTIST',
    description: 'Lorem Ipsum is simply dummy text',
    tags: ['#tags', '#socialtags', '#specialtags'],
    facebook: '#0',
  },
];
export const activityFilters = [
  { id: 1, title: 'My Activities' },
  { id: 2, title: 'All' },
  { id: 3, title: 'Facebook' },
  { id: 4, title: 'Spotify' },
  { id: 5, title: 'Twitter' },
  { id: 6, title: 'Trephoria' },
];
