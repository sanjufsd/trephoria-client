import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';

export const Loader = props => {
  const { color, bgColor, padding } = props;
  return (
    <S.ScreenLoader bgColor={bgColor} padding={padding}>
      <S.SpinnerGlow className="spinner-grow" color={color} />
    </S.ScreenLoader>
  );
};

Loader.propTypes = {
  color: PropTypes.string,
  bgColor: PropTypes.string,
  padding: PropTypes.string,
};
