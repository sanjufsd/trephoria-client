import styled from 'styled-components';

export const ScreenLoader = styled.div`
  position: ${props => (props.padding ? 'relative' : 'absolute')};
  z-index: 10;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${props => props.bgColor || '#ffffff78'};
  padding: ${props => props.padding || 0};
`;

export const SpinnerGlow = styled.div`
  background: ${props => props.color || '#983fcc'};
`;
