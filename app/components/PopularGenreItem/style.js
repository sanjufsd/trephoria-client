/* eslint-disable */
import styled from 'styled-components';

export const Item = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: ${props =>
    [0, 6].includes(props.position) ? 'flex-end' : 'flex-start'};
  align-items: ${props =>
    [0, 2].includes(props.position) ? 'flex-end' : 'flex-start'};
  a {
    display: block;
    width: ${props => [0, 8].includes(props.position) ? '140px' : ([2, 6].includes(props.position) ? '180px' : '100%')};
    height: ${props => [0, 8].includes(props.position) ? '140px' : ([2, 6].includes(props.position) ? '180px' : '100%')};
    text-decoration: none !important;
    color: black !important;
    position: relative;
    transform: translate(
      0,
      ${props => ([1, 4].includes(props.position) ? '60px' : 0)}
    );
    transition: all 0.5s ease-in-out;
    z-index: 1;
  }
  a:hover > img ~ div {
    opacity: 1;
  }
  a:hover {
    transform: translate(
      0,
      ${props => ([1, 4].includes(props.position) ? '60px' : 0)}
    ) scale(1.05);
  }
`;

export const MainImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const TitleWrapper = styled.div`
  position: absolute;
  bottom: 0;
  background: white;
  width: 100%;
  text-align: center;
  padding: 15px 0;
  opacity: 0;
  transition: all 0.5s ease-in-out;
`;

export const Title = styled.div`
  display: inline-block;
  font-size: ${props =>
    [0, 2, 6, 8].includes(props.position) ? '10px' : '15px'};
  margin: 0 10px 0 0;
  text-transform: uppercase;
`;

export const Icon = styled.img`
  width: ${props => ([0, 2, 6, 8].includes(props.position) ? '15px' : '25px')};
`;
