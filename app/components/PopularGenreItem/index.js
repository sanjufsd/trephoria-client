import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as c from './constants';
import * as S from './style';
import * as u from '../../config/url';

const PopularGenreItem = props => {
  const {
    item: { title, image },
    position,
    _image,
  } = props;
  const url = title.toLowerCase();
  return (
    <S.Item position={position}>
      <Link to={`${u.singleGenre}/${url}`}>
        <S.MainImg src={image || _image} alt="" />
        <S.TitleWrapper>
          <S.Title position={position}>{title}</S.Title>
          <S.Icon position={position} src={c.popularMusicGenreArrow} alt="" />
        </S.TitleWrapper>
      </Link>
    </S.Item>
  );
};

PopularGenreItem.propTypes = {
  item: PropTypes.object,
  position: PropTypes.number,
  _image: PropTypes.string,
};

export default PopularGenreItem;
