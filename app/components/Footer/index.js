import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';

const Footer = () => (
  <S.Wrapper>
    <Container>
      <Row>
        <Col lg="4">
          <S.Social>
            {c.social.map(item => (
              <S.SocialLink href={item.url} key={item.title}>
                <S.SocialIcon src={item.icon} alt={item.title} />
              </S.SocialLink>
            ))}
          </S.Social>
        </Col>
        <Col lg="4">
          <S.CopyRightText>{c.copyright}</S.CopyRightText>
        </Col>
        <Col lg="4">
          <S.Logo>
            <Link to={u.homepage}>
              <S.LogoIcon src={c.logo} alt="" />
            </Link>
          </S.Logo>
        </Col>
      </Row>
    </Container>
  </S.Wrapper>
);

export default Footer;
