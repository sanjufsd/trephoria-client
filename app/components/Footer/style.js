import styled from 'styled-components';

export const Wrapper = styled.footer`
  background-color: #1d1d1d;
  padding: 30px 0;
  border-top: 10px solid black;
  position: relative;
  z-index: 2;
`;

export const CopyRightText = styled.p`
  margin: 5px 0 0;
  font-size: 14px;
  text-align: center;
  color: #ffffff;
`;

export const Social = styled.div``;

export const SocialLink = styled.a`
  display: inline-block;
  margin: 0 30px 0 0;
`;

export const SocialIcon = styled.img`
  height: 18px;
`;

export const Logo = styled.div`
  text-align: right;
  a {
    display: inline-block;
  }
`;

export const LogoIcon = styled.img`
  width: 130px;
`;
