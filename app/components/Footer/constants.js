/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const social = [
  {
    title: 'Facebook',
    icon: `${apiUrl}/images/facebook.png`,
    url: '#0',
  },
  {
    title: 'Twitter',
    icon: `${apiUrl}/images/twitter.png`,
    url: '#0',
  },
  {
    title: 'Instagram',
    icon: `${apiUrl}/images/instagram.png`,
    url: '#0',
  },
];
export const logo = `${apiUrl}/images/logo.png`;
export const copyright = '©2019 Trephoria. All rights reserved.';
