import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
} from 'reactstrap';
import * as S from './style';
import * as G from '../../global-styles';
import * as c from './constants';
import * as u from '../../config/url';
import DetectLocation from './DetectLocation';
import ErrorBoundary from '../ErrorBoundary';
import { scrollToDiv } from '../ScrollToDiv';
import MyConcerts from './myConcerts';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      isOpen: false,
      myConcert: false,
    };
  }

  componentDidMount() {
    this.activeOnScroll();
    window.addEventListener('scroll', this.activeOnScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.activeOnScroll);
  }

  activeOnScroll = () => {
    const windowTop = window.pageYOffset || window.scrollY;
    if (windowTop > 20) {
      this.setState({ active: true });
    } else {
      this.setState({ active: false });
    }
  };

  handleToggle = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen,
    }));
  };

  handleScrollToDiv = id => {
    const {
      location: { pathname },
      history: { push },
    } = this.props;
    if (pathname === u.homepage) {
      scrollToDiv(id);
    } else {
      push(u.homepage);
      setTimeout(() => {
        scrollToDiv(id);
      }, 100);
    }
  };

  toggleMyConcerts = status => {
    this.setState({ myConcert: status });
  };

  render() {
    const { active, isOpen, myConcert } = this.state;
    const {
      bg,
      user,
      location: { pathname },
    } = this.props;
    return (
      <Fragment>
        <S.HeaderContainer active={active} bg={bg}>
          <Container>
            <Navbar expand="md">
              <Link to={u.homepage} className="navbar-brand">
                <S.Logo src={c.logo} alt="" />
              </Link>
              <NavbarToggler
                onClick={() => {
                  this.handleToggle();
                }}
              />
              <ErrorBoundary>
                <DetectLocation />
              </ErrorBoundary>
              {!(user && user.userId) ? (
                <Collapse isOpen={isOpen} navbar>
                  <Nav className="ml-auto" navbar>
                    {c.navLinks.map(item => (
                      <NavItem key={item.name}>
                        <S.NavLink
                          onClick={() => {
                            this.handleScrollToDiv(item.id);
                          }}
                        >
                          {item.name}
                        </S.NavLink>
                      </NavItem>
                    ))}
                    <NavItem>
                      <S.NavButton>
                        <Link to={u.login}>Login/Sign up</Link>
                      </S.NavButton>
                    </NavItem>
                  </Nav>
                </Collapse>
              ) : (
                <S.AuthNav>
                  <G.Div>
                    <Link
                      to={u.userActivities}
                      className={pathname === u.userActivities ? 'active' : ''}
                    >
                      Home
                    </Link>
                    <S.ConcertButton
                      onClick={() => {
                        this.toggleMyConcerts(true);
                      }}
                    >
                      My Concerts
                    </S.ConcertButton>
                    <S.Notification>
                      <S.NotificationButton>
                        <S.NotificationIcon src={c.notificationIcon} />
                      </S.NotificationButton>
                    </S.Notification>
                    <S.UserDropdown>
                      <S.UserDropdownButton>
                        <S.UserProfilePic src={c.profilePic} />
                        <S.DropdownIcon src={c.dropdownArrow} />
                      </S.UserDropdownButton>
                    </S.UserDropdown>
                  </G.Div>
                </S.AuthNav>
              )}
            </Navbar>
          </Container>
        </S.HeaderContainer>
        {myConcert && (
          <MyConcerts setPopup={this.toggleMyConcerts} pathname={pathname} />
        )}
      </Fragment>
    );
  }
}

Header.propTypes = {
  bg: PropTypes.bool,
  location: PropTypes.object,
  history: PropTypes.object,
  user: PropTypes.object,
};

export default Header;
