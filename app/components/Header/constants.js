/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const logo = `${apiUrl}/images/logo.png`;
export const navLinks = [
  { name: 'Home', id: 'home' },
  { name: 'About Us', id: 'about_us' },
  { name: 'How it works', id: 'how_it_works' },
];

export const detectText = 'Detect';
export const detectPointer = `${apiUrl}/images/detect_pointer.png`;
export const detectPointerGrey = `${apiUrl}/images/detect_pointer_1.png`;
export const detectLocation = `${apiUrl}/images/detect_location.png`;
export const detectLocationGrey = `${apiUrl}/images/detect_location_1.png`;
export const profilePic = `${apiUrl}/images/dummy-profile-pic.jpg`;
export const notificationIcon = `${apiUrl}/images/user-notification.svg`;
export const dropdownArrow = `${apiUrl}/images/dropdown-arrow.svg`;

export const currentLocationText = 'Current Location';
export const popularLocationText = 'Popular Location';
export const popularLocations = ['Abu Dhabi', 'Bahrain', 'Qatar', 'Al Ain'];

// my Concerts
export const images = [
  { id: 1, image: `${apiUrl}/images/concert-banner-1.jpg` },
  { id: 2, image: `${apiUrl}/images/concert-banner-2.jpg` },
  { id: 3, image: `${apiUrl}/images/concert-banner-3.jpg` },
];
export const concertTabs = [
  { id: 1, title: 'Upcoming Concerts' },
  { id: 2, title: 'Completed Concerts' },
];
export const concertLeftArrow = `${apiUrl}/images/trending_concert_left_arrow.svg`;
export const concertRightArrow = `${apiUrl}/images/trending_concert_right_arrow.png`;
export const concertLists = [
  {
    date: 30,
    month: 'Aug',
    title: 'Bumbershoot 2019',
    image: `${apiUrl}/images/concert_01.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 14,
    month: 'Sep',
    title: 'Drake (21 + Event)',
    image: `${apiUrl}/images/concert_02.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 22,
    month: 'Sep',
    title: `Hot 97's On Da Reggae Soca Tip`,
    image: `${apiUrl}/images/concert_03.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 30,
    month: 'Aug',
    title: 'Bumbershoot 2019',
    image: `${apiUrl}/images/concert_01.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 14,
    month: 'Sep',
    title: 'Drake (21 + Event)',
    image: `${apiUrl}/images/concert_02.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 22,
    month: 'Sep',
    title: `Hot 97's On Da Reggae Soca Tip`,
    image: `${apiUrl}/images/concert_03.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
];
