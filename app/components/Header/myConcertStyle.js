import styled, { css } from 'styled-components';

export const ConcertBanners = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -3px;
`;

export const ConcertBanner = styled.div`
  width: 33.3333%;
  padding: 0 3px;
`;

export const ConcertImage = styled.img`
  width: 100%;
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: left top;
`;

export const ConcertWrapper = styled.div`
  padding: 35px 140px;
`;

export const ConcertHead = styled.div`
  > a,
  > button {
    float: right;
    border: 1px solid #983fcc;
    color: #983fcc;
    text-decoration: none;
    font-size: 11px;
    padding: 5px 15px;
    border-radius: 20px;
  }
`;

export const ViewAllButton = styled.button`
  background: transparent;
  outline: 0 !important;
`;

export const ConcertTabs = styled.ul`
  display: inline-block;
  margin: 0;
  padding: 0;
  list-style: none;
  width: calc(100% - 100px);
`;

export const ConcertTab = styled.li`
  font-size: 16px;
  display: inline-block;
  color: #979797;
  cursor: pointer;
  &:first-child {
    width: 175px;
  }
  &:last-child {
    padding-left: 20px;
    border-left: 1px solid #979797;
  }
  ${props =>
    props.active &&
    css`
      opacity: 1;
      font-weight: bold;
      color: black;
    `}
`;

export const ConcertSlider = styled.div`
  margin: 30px 0 0;
`;

export const ConcertItem = styled.div`
  padding: 0 5px;
`;
