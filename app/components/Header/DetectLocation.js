import React, { Fragment, Component } from 'react';
import * as S from './style';
import * as c from './constants';

class DetectLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      dropdown: false,
    };
  }

  handleChange = e => {
    const { value } = e.target;
    this.setState({ location: value });
  };

  handleFocus = () => {
    this.setState({ dropdown: true });
  };

  handleBlur = () => {
    this.setState({ dropdown: false });
  };

  render() {
    const { dropdown, location } = this.state;
    return (
      <Fragment>
        <S.DetectContainer dropdown={dropdown}>
          <S.DetectInputWrapper>
            <S.DetectInputPointer src={c.detectPointer} alt="" />
            <S.DetectInput
              type="text"
              placeholder="Type the location…"
              value={location}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
            />
            <S.DetectButton>
              {c.detectText}
              <S.DetectButtonIcon src={c.detectLocation} alt="" />
            </S.DetectButton>
          </S.DetectInputWrapper>
          {dropdown && (
            <S.DetectDropdown>
              <S.DetectCurrentLocation>
                <S.DetectLocationIcon src={c.detectLocationGrey} alt="" />
                {c.currentLocationText}
              </S.DetectCurrentLocation>
              <S.PopularLocationTitle>
                {c.popularLocationText}
              </S.PopularLocationTitle>
              <S.PopularLocationWrapper>
                {c.popularLocations.map((item, index) => (
                  // eslint-disable-next-line react/no-array-index-key
                  <S.PopularLocationWrapperItem key={index}>
                    <S.PopularLocationPointer
                      src={c.detectPointerGrey}
                      alt=""
                    />
                    {item}
                  </S.PopularLocationWrapperItem>
                ))}
              </S.PopularLocationWrapper>
            </S.DetectDropdown>
          )}
        </S.DetectContainer>
        {dropdown && <S.DetectFade onClick={this.handleBlur} />}
      </Fragment>
    );
  }
}

export default DetectLocation;
