import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import Slider from 'react-slick';
import * as S from './myConcertStyle';
import * as c from './constants';
import * as u from '../../config/url';
import SlickSliderArrow from '../SlickSliderArrow';
import ErrorBoundary from '../ErrorBoundary';
import ConcertItem from '../ConcertItem';

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  nextArrow: <SlickSliderArrow icon={c.concertRightArrow} position="right" />,
  prevArrow: <SlickSliderArrow icon={c.concertLeftArrow} position="left" />,
};

const MyConcerts = props => {
  const { setPopup, pathname } = props;
  const [activeTab, setTab] = useState(1);
  return (
    <Modal
      isOpen
      toggle={() => {
        setPopup(false);
      }}
      className="myConcerts"
      size="xl"
      centered
    >
      <ModalHeader
        toggle={() => {
          setPopup(false);
        }}
      >
        My Concerts
      </ModalHeader>
      <ModalBody>
        <S.ConcertBanners>
          {c.images.map(item => (
            <S.ConcertBanner key={item.id}>
              <S.ConcertImage src={item.image} />
            </S.ConcertBanner>
          ))}
        </S.ConcertBanners>
        <S.ConcertWrapper>
          <S.ConcertHead>
            <S.ConcertTabs>
              {c.concertTabs.map(item => (
                <S.ConcertTab
                  key={item.id}
                  active={activeTab === item.id}
                  onClick={() => {
                    setTab(item.id);
                  }}
                >
                  {item.title}
                </S.ConcertTab>
              ))}
            </S.ConcertTabs>
            {pathname === u.userConcerts ? (
              <S.ViewAllButton
                onClick={() => {
                  setPopup(false);
                }}
              >
                View All
              </S.ViewAllButton>
            ) : (
              <Link to={u.userConcerts}>View All</Link>
            )}
          </S.ConcertHead>
          <S.ConcertSlider>
            <ErrorBoundary>
              <Slider {...settings}>
                {c.concertLists.map((item, index) => (
                  // eslint-disable-next-line react/no-array-index-key
                  <S.ConcertItem key={index}>
                    <ErrorBoundary>
                      <ConcertItem item={item} />
                    </ErrorBoundary>
                  </S.ConcertItem>
                ))}
              </Slider>
            </ErrorBoundary>
          </S.ConcertSlider>
        </S.ConcertWrapper>
      </ModalBody>
    </Modal>
  );
};

MyConcerts.propTypes = {
  setPopup: PropTypes.func,
  pathname: PropTypes.string,
};

export default MyConcerts;
