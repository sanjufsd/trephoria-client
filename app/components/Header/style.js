import styled from 'styled-components';

export const HeaderContainer = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  background: ${props =>
    props.active || props.bg ? '#211d22' : 'transparent'};
  z-index: 999;
  padding: 20px 0;
  transition: all 0.5s ease-in-out;
  .navbar {
    padding: 0;
  }
  .nav-item {
    .nav-link {
      font-size: 14px;
      color: white !important;
      padding: 9px 15px !important;
    }
  }
`;

export const Logo = styled.img`
  width: 133px;
`;

export const NavLink = styled.button`
  background: transparent;
  outline: 0 !important;
  color: white;
  border: 0;
  font-size: 14px;
  padding: 10px;
`;

export const NavButton = styled.div`
  a {
    border-radius: 20px;
    background-color: #983fcc;
    font-size: 14px;
    color: white;
    border: 0;
    padding: 9px 20px;
    margin: 0 0 0 10px;
    outline: 0 !important;
    text-decoration: none !important;
    display: inline-block;
  }
`;

export const DetectContainer = styled.div`
  background: black;
  padding: 10px;
  border-radius: ${props => (props.dropdown ? '10px 10px 0 0' : '10px')};
  position: relative;
  width: 285px;
  margin: 0 0 0 20px;
  z-index: 2;
`;

export const DetectInputWrapper = styled.div``;

export const DetectInputPointer = styled.img`
  width: 15px;
  margin: 0 10px 0 0;
`;

export const DetectInput = styled.input`
  border: 0;
  background: transparent;
  color: white;
  font-size: 15px;
  margin: 0 15px 0 0;
  border-right: 1px solid #979797;
  outline: 0;
  width: 155px;
  padding: 0 15px 0 0;
`;

export const DetectButton = styled.button`
  background: transparent;
  border: 0;
  color: white;
  font-size: 15px;
  padding: 0;
  outline: 0 !important;
`;

export const DetectButtonIcon = styled.img`
  width: 20px;
  margin: 0 0 0 5px;
`;

export const DetectDropdown = styled.div`
  position: absolute;
  background: white;
  left: 0;
  width: 100%;
  top: 44px;
  border-radius: 0 0 10px 10px;
`;

export const PopularLocationTitle = styled.h6`
  font-size: 12px;
  color: #9c9c9c;
  padding: 0 15px;
  margin: 15px 0 0;
`;

export const PopularLocationWrapper = styled.ul`
  padding: 10px 8px;
  margin: 0;
  list-style: none;
`;

export const PopularLocationWrapperItem = styled.li`
  font-size: 14px;
  cursor: pointer;
  line-height: 37px;
`;

export const PopularLocationPointer = styled.img`
  width: 35px;
`;

export const DetectCurrentLocation = styled.div`
  font-size: 14px;
  padding: 5px 15px;
  border-bottom: 1px solid #e4e4e4;
  cursor: pointer;
  height: 43px;
`;

export const DetectLocationIcon = styled.img`
  width: 34px;
  margin: 0 0 0 -8px;
`;

export const DetectFade = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  z-index: 1;
`;

export const AuthNav = styled.div`
  display: flex;
  flex-grow: 1;
  > div {
    display: flex;
    flex-direction: row;
    margin: 0 0 0 auto;
    > a,
    > button {
      font-size: 14px;
      color: white;
      text-decoration: none !important;
      padding: 8px 15px;
      display: inline-block;
      position: relative;
    }
    > a.active {
      color: #ff5853;
      &:after {
        content: '';
        position: absolute;
        width: 100%;
        background: #ff5853;
        height: 2px;
        left: 0;
        bottom: -23px;
      }
    }
  }
`;

export const ConcertButton = styled.button`
  background: transparent;
  border: 0;
  outline: 0 !important;
`;

export const Notification = styled.div`
  margin: 0 0 0 20px;
`;

export const NotificationButton = styled.button`
  background: transparent;
  outline: 0 !important;
  border: 0 !important;
  padding: 0;
`;

export const NotificationIcon = styled.img``;

export const UserDropdown = styled.div`
  margin: 0 0 0 30px;
`;

export const UserDropdownButton = styled.button`
  background: transparent;
  outline: 0 !important;
  border: 0 !important;
  padding: 0;
`;

export const UserProfilePic = styled.img`
  height: 35px;
  width: 35px;
  object-fit: cover;
  border-radius: 50%;
`;

export const DropdownIcon = styled.img`
  margin: 0 0 0 10px;
`;
