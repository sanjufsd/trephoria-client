import styled from 'styled-components';

export const Wrapper = styled.div`
  border-radius: 8px;
  border: solid 1px #e6e6e6;
  text-align: center;
  padding: 20px 0;
  position: relative;
`;

export const Icon = styled.img`
  width: 32px;
  margin: 0 0 10px;
`;

export const Title = styled.p`
  font-size: 12px;
  margin: 0;
`;

export const Button = styled.button`
  border-radius: 20px;
  border: 0 !important;
  background-color: ${props => (props.active ? '#37b487' : '#983fcc')};
  color: white;
  font-size: 10px;
  width: 90px;
  display: block;
  margin: 15px auto 0;
  padding: 5px 0 3px;
  outline: 0 !important;
`;

export const WhiteTick = styled.img`
  margin: 0 5px 0 0;
`;

export const DisConnect = styled.div`
  position: absolute;
  top: 5px;
  right: 5px;
  cursor: pointer;
`;

export const CloseIcon = styled.img``;
