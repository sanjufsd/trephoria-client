import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';

const SocialConnect = props => {
  const {
    item,
    item: { title, icon },
    selectedSocialAccounts,
    handleSocialConnect,
    handleSocialDisconnect,
  } = props;
  const active = selectedSocialAccounts.some(social => social.title === title);
  return (
    <Fragment>
      <S.Wrapper>
        <S.Icon src={icon} />
        <S.Title>{title}</S.Title>
        {active && (
          <S.DisConnect
            onClick={() => {
              handleSocialDisconnect(item);
            }}
          >
            <S.CloseIcon src={c.closeRed} />
          </S.DisConnect>
        )}
      </S.Wrapper>
      <S.Button
        active={active}
        onClick={() => {
          handleSocialConnect(item);
        }}
        disabled={active}
      >
        {active && <S.WhiteTick src={c.whiteTick} />}
        {active ? 'Connected' : 'Connect'}
      </S.Button>
    </Fragment>
  );
};

SocialConnect.propTypes = {
  item: PropTypes.object,
  selectedSocialAccounts: PropTypes.array,
  handleSocialConnect: PropTypes.func,
  handleSocialDisconnect: PropTypes.func,
};

export default SocialConnect;
