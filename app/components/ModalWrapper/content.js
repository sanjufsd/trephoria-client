/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import ErrorBoundary from '../ErrorBoundary';
import SocialLoginItem from '../SocialLoginItem';
import LoginFormWrapper from '../../containers/Login/form';
import SignupFormWrapper from '../../containers/Signup/form';
import ForgotPasswordForm from '../../containers/ForgotPassword/form';
import ResetPasswordForm from '../../containers/ResetPassword/form';
import PasswordSuccess from '../../containers/ResetPassword/success';
import SignupSuccess from '../../containers/Signup/success';

const ContentWrapper = props => {
  const { pathname } = props;
  let title = '';
  let rightImg = '';
  if (pathname === u.login) {
    title = 'Login';
    rightImg = c.loginRightImg;
  } else if (pathname === u.signup) {
    title = 'Sign Up';
    rightImg = c.signupRightImg;
  } else if (pathname === u.forgotPassword) {
    rightImg = c.resetRightImg;
  } else if (pathname === u.resetPassword) {
    rightImg = c.resetRightImg;
  } else if (pathname === u.passwordSuccess) {
    rightImg = c.passwordSuccessRightImg;
  }
  return (
    <Fragment>
      <Row>
        <Col lg="5">
          {[u.login, u.signup].includes(pathname) ? (
            <Fragment>
              <S.LoginSocial>
                <S.LoginSocialTitle>{title} With Trephoria</S.LoginSocialTitle>
                {c.socialLogin.map(item => (
                  <ErrorBoundary key={item.title}>
                    <SocialLoginItem item={item} type={title} />
                  </ErrorBoundary>
                ))}
              </S.LoginSocial>
              <S.OrWrapper>
                <S.Or>or</S.Or>
              </S.OrWrapper>
              {title === 'Login' ? (
                <Fragment>
                  <ErrorBoundary>
                    <LoginFormWrapper eye={c.passwordEye} {...props} />
                  </ErrorBoundary>
                  <S.OtherLink>
                    <Link to={u.signup}>New To Trephoria ? Sign Up</Link>
                  </S.OtherLink>
                </Fragment>
              ) : (
                <Fragment>
                  <ErrorBoundary>
                    <SignupFormWrapper eye={c.passwordEye} {...props} />
                  </ErrorBoundary>
                  <S.OtherLink>
                    <Link to={u.login}>Already on Trephoria ? Login</Link>
                  </S.OtherLink>
                </Fragment>
              )}
            </Fragment>
          ) : pathname === u.forgotPassword ? (
            <S.ForgotWrapper>
              <S.ForgotIcon src={c.forgotIcon} alt="" />
              <S.ForgotText>
                To reset your Password, please enter your registered Email Id
              </S.ForgotText>
              <ErrorBoundary>
                <ForgotPasswordForm />
              </ErrorBoundary>
            </S.ForgotWrapper>
          ) : pathname === u.resetPassword ? (
            <S.ForgotWrapper>
              <S.ForgotIcon src={c.resetIcon} alt="" />
              <S.ForgotText>
                Enter your unique password to keep your account secured
              </S.ForgotText>
              <ErrorBoundary>
                <ResetPasswordForm />
              </ErrorBoundary>
            </S.ForgotWrapper>
          ) : pathname === u.passwordSuccess ? (
            <S.ForgotWrapper>
              <ErrorBoundary>
                <PasswordSuccess />
              </ErrorBoundary>
            </S.ForgotWrapper>
          ) : (
            ''
          )}
        </Col>
        <Col lg="7">
          {/* eslint-disable-next-line prettier/prettier */}
          {[u.login, u.forgotPassword, u.resetPassword].includes(pathname) ? (
            <S.LoginImgWrapper>
              <S.LoginImg src={rightImg} alt="" />
            </S.LoginImgWrapper>
          ) : pathname === u.signup ? (
            <S.SignupImgWrapper>
              <S.SignupImg src={rightImg} alt="" />
            </S.SignupImgWrapper>
          ) : pathname === u.passwordSuccess ? (
            <S.SuccessImgWrapper>
              <S.SuccessImg src={rightImg} alt="" />
            </S.SuccessImgWrapper>
          ) : (
            ''
          )}
        </Col>
      </Row>
      {pathname === u.signupSuccess && (
        <S.SignupSuccessWrapper>
          <ErrorBoundary>
            <SignupSuccess />
          </ErrorBoundary>
        </S.SignupSuccessWrapper>
      )}
    </Fragment>
  );
};

ContentWrapper.propTypes = {
  pathname: PropTypes.string,
};

export default ContentWrapper;
