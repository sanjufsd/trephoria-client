/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import ForgotHeading from '../../containers/ForgotPassword/heading';
import ResetHeading from '../../containers/ResetPassword/heading';
import ErrorBoundary from '../ErrorBoundary';

const HeadingWrapper = props => {
  const { pathname, history } = props;
  return (
    <Fragment>
      {[u.login, u.signup].includes(pathname) ? (
        <S.Heading>
          <S.HeadingTabs>
            <Link to={u.login} className={pathname === u.login ? 'active' : ''}>
              Login
            </Link>{' '}
            <Link
              to={u.signup}
              className={pathname === u.signup ? 'active' : ''}
            >
              Sign Up
            </Link>
          </S.HeadingTabs>
          <S.HeadingClose
            onClick={() => {
              history.push(u.homepage);
            }}
          >
            <S.HeadingCloseIcon src={c.modalClose} alt="" />
          </S.HeadingClose>
        </S.Heading>
      ) : pathname === u.forgotPassword ? (
        <ErrorBoundary>
          <ForgotHeading history={history} />
        </ErrorBoundary>
      ) : pathname === u.resetPassword ? (
        <ErrorBoundary>
          <ResetHeading history={history} />
        </ErrorBoundary>
      ) : (
        ''
      )}
    </Fragment>
  );
};

HeadingWrapper.propTypes = {
  history: PropTypes.object,
  pathname: PropTypes.string,
};

export default HeadingWrapper;
