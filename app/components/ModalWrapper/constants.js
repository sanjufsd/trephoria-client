/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const modalBg = `${apiUrl}/images/login_bg.png`;
export const modalClose = `${apiUrl}/images/login_close.png`;
export const modalCircle = `${apiUrl}/images/login_circle.png`;
export const loginEye = `${apiUrl}/images/login_eye.png`;
export const facebookIcon = `${apiUrl}/images/login_facebook_white.png`;
export const messageIcon = `${apiUrl}/images/login_message_white.png`;
export const spotifyIcon = `${apiUrl}/images/login_spotify_white.png`;

export const socialLogin = [
  {
    title: 'Facebook',
    color: '#3b5998',
    icon: `${apiUrl}/images/login_facebook_white.png`,
  },
  {
    title: 'Spotify',
    color: '#00c846',
    icon: `${apiUrl}/images/login_spotify_white.png`,
  },
  {
    title: 'Gmail',
    color: '#d0021b',
    icon: `${apiUrl}/images/login_message_white.png`,
  },
];

export const loginRightImg = `${apiUrl}/images/login_right_image.png`;
export const signupRightImg = `${apiUrl}/images/signup_right_image.png`;

export const passwordEye = `${apiUrl}/images/login_eye.png`;

export const forgotIcon = `${apiUrl}/images/forgot_password_icon.png`;
export const resetIcon = `${apiUrl}/images/reset_password_icon.png`;
export const resetRightImg = `${apiUrl}/images/reset_password_right_image.png`;
export const passwordSuccessRightImg = `${apiUrl}/images/password_success_image.png`;
