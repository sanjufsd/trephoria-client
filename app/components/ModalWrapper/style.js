import styled from 'styled-components';

export const ModalContainer = styled.div`
  background-image: url(${props => props.background});
  padding: 130px 0 80px;
  background-size: cover;
  background-position: left bottom;
  background-repeat: no-repeat;
  position: relative;
  min-height: 100vh;
  .container {
    height: 100%;
  }
`;

export const ModalFlex = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const ModalCenter = styled.div`
  width: 100%;
  padding: 0 100px;
`;

export const ModalBgCircle = styled.img`
  position: absolute;
  right: 0;
  top: calc(50% + 50px);
  width: 200px;
  transform: translate(0, -50%);
`;

export const ModalBlock = styled.div`
  border-radius: 4px;
  box-shadow: 0 0 50px 0 rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  overflow: hidden;
  position: relative;
`;

export const ModalHeading = styled.div`
  box-shadow: 0 0 50px 0 rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  height: 55px;
`;

export const Heading = styled.div`
  display: inline-block;
  width: 100%;
  padding: 0 50px;
`;

export const HeadingTabs = styled.div`
  display: inline-block;
  font-size: 14px;
  color: #ababab;
  a {
    font-size: 14px;
    color: #ababab;
    padding: 16px 10px;
    display: inline-block;
    border-bottom: 2px solid transparent;
    text-decoration: none !important;
  }
  a.active {
    color: black;
    border-bottom: 2px solid #ff5853;
  }
`;

export const HeadingClose = styled.button`
  float: right;
  background: transparent;
  cursor: pointer;
  outline: 0 !important;
  border: 0 !important;
  padding: 15px 0;
`;

export const HeadingCloseIcon = styled.img`
  width: 15px;
`;

export const ModalContent = styled.div``;

export const ModalContentSpace = styled.div`
  padding-left: 50px;
`;

export const LoginSocial = styled.div`
  padding: 30px 0 10px;
`;

export const LoginSocialTitle = styled.h2`
  font-size: 14px;
  font-weight: 500;
  color: black;
  margin: 0 0 20px;
`;

export const OrWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  :after {
    content: '';
    width: calc(50% - 20px);
    position: absolute;
    height: 1px;
    background: #e7e7e7;
    left: 0;
    top: 9px;
  }
  :before {
    content: '';
    width: calc(50% - 20px);
    position: absolute;
    height: 1px;
    background: #e7e7e7;
    right: 0;
    top: 9px;
  }
`;

export const Or = styled.span`
  font-size: 14px;
  color: #929699;
  background: white;
  display: inline-block;
  width: 30px;
  text-align: center;
`;

export const LoginImgWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  padding: 20px 0;
  justify-content: center;
`;

export const LoginImg = styled.img`
  width: 300px;
  max-width: 100%;
`;

export const SignupImgWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
`;

export const SignupImg = styled.img`
  width: calc(100% - 60px);
  max-width: 100%;
`;

export const OtherLink = styled.div`
  margin: 0 0 20px;
  a {
    font-size: 12px;
    color: #070707;
    text-decoration: none;
  }
`;

export const ForgotWrapper = styled.div`
  padding: 80px 0;
`;

export const ForgotIcon = styled.img`
  width: 65px;
  margin: 0 0 25px;
`;

export const ForgotText = styled.p`
  font-size: 14px;
  font-weight: 500;
  color: #000000;
  margin-bottom: 10px;
`;

export const SuccessImgWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
`;

export const SuccessImg = styled.img`
  width: 100%;
  transform: translate(20px, 20px);
`;

export const SignupSuccessWrapper = styled.div``;
