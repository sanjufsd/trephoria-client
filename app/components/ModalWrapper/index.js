/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import Header from '../Header';
import ErrorBoundary from '../ErrorBoundary';
import HeadingWrapper from './heading';
import ContentWrapper from './content';

const ModalWrapper = props => {
  const {
    location: { pathname },
    history,
    headingComponent,
    contentComponent,
  } = props;
  const availableHeadings = [
    u.login,
    u.signup,
    u.forgotPassword,
    u.resetPassword,
  ];
  const availableContents = [
    ...availableHeadings,
    ...[u.signupSuccess, u.passwordSuccess],
  ];
  return (
    <Fragment>
      <ErrorBoundary>
        <Header {...props} />
      </ErrorBoundary>
      <S.ModalContainer background={c.modalBg}>
        <Container>
          <S.ModalFlex>
            <S.ModalCenter>
              <S.ModalBlock>
                {(availableHeadings.includes(pathname) || headingComponent) && (
                  <S.ModalHeading>
                    <ErrorBoundary>
                      {headingComponent || (
                        <HeadingWrapper pathname={pathname} history={history} />
                      )}
                    </ErrorBoundary>
                  </S.ModalHeading>
                )}
                <S.ModalContent>
                  {availableContents.includes(pathname) && (
                    <S.ModalContentSpace>
                      <ErrorBoundary>
                        <ContentWrapper pathname={pathname} {...props} />
                      </ErrorBoundary>
                    </S.ModalContentSpace>
                  )}
                  {contentComponent && (
                    <ErrorBoundary>{contentComponent}</ErrorBoundary>
                  )}
                </S.ModalContent>
              </S.ModalBlock>
            </S.ModalCenter>
          </S.ModalFlex>
        </Container>
        <S.ModalBgCircle src={c.modalCircle} alt="" />
      </S.ModalContainer>
    </Fragment>
  );
};

ModalWrapper.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
  headingComponent: PropTypes.element,
  contentComponent: PropTypes.element,
};

export default ModalWrapper;
