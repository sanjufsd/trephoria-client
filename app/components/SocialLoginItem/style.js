import styled from 'styled-components';

export const SocialItem = styled.div`
  font-size: 12px;
  background: ${props => props.bgColor};
  color: white;
  border-radius: 8px;
  margin: 0 0 10px;
  padding: 0 10px;
  cursor: pointer;
`;

export const Img = styled.img`
  width: 40px;
`;
export const Title = styled.span`
  display: inline-block;
  vertical-align: middle;
`;
