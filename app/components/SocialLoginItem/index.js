/* eslint-disable no-console */
/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import SpotifyLogin from 'react-spotify-login';
import * as S from './style';
import {
  appUrl,
  facebookAppId,
  googleClientId,
  spotifyClientId,
} from '../../config/apikey';
import ErrorBoundary from '../ErrorBoundary';

const Button = props => {
  const {
    item: { color, icon, title },
    type,
  } = props;
  return (
    <S.SocialItem bgColor={color} {...props}>
      <S.Img src={icon} />
      <S.Title>
        {type} With {title}
      </S.Title>
    </S.SocialItem>
  );
};

const SocialLoginItem = props => {
  const {
    item: { title },
  } = props;
  const fbResponse = fb => {
    console.log({ fb });
  };
  const googleResponse = google => {
    console.log({ google });
  };
  const spotifyResponse = spotify => {
    console.log({ spotify });
  };
  return (
    <Fragment>
      {title === 'Facebook' ? (
        <ErrorBoundary>
          <FacebookLogin
            appId={facebookAppId}
            autoLoad={false}
            callback={fbResponse}
            // renderProps - onClick, isDisabled, isProcessing, isSdkLoaded
            render={renderProps => (
              <Button onClick={renderProps.onClick} {...props} />
            )}
          />
        </ErrorBoundary>
      ) : title === 'Gmail' ? (
        <ErrorBoundary>
          <GoogleLogin
            clientId={googleClientId}
            onSuccess={googleResponse}
            onFailure={googleResponse}
            cookiePolicy="single_host_origin"
            render={renderProps => (
              <Button
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
                {...props}
              />
            )}
          />
        </ErrorBoundary>
      ) : title === 'Spotify' ? (
        <ErrorBoundary>
          <SpotifyLogin
            clientId={spotifyClientId}
            redirectUri={appUrl}
            onSuccess={spotifyResponse}
            onFailure={spotifyResponse}
            className="transparentButton"
          >
            <Button {...props} />
          </SpotifyLogin>
        </ErrorBoundary>
      ) : (
        <Button {...props} />
      )}
    </Fragment>
  );
};

Button.propTypes = {
  item: PropTypes.object,
  type: PropTypes.string,
};

SocialLoginItem.propTypes = {
  item: PropTypes.object,
};

export default SocialLoginItem;
