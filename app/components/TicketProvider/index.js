/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';

const TicketProvider = props => {
  const {
    item: { image, status },
    setStep,
  } = props;
  let text = 'Soldout';
  let color = '#d0021b';
  if (status === 'available') {
    text = 'Available';
    color = '#7ed321';
  } else if (status === 'filling_fast') {
    text = 'Filling Fast';
    color = '#f5a623';
  }

  const handleClick = () => {
    if (status !== 'soldout') {
      setStep(2);
    }
  };

  return (
    <S.TicketProvider
      status={status}
      onClick={() => {
        handleClick();
      }}
    >
      <S.TicketActiveIcon className="arrow" src={c.rightArrow} />
      <S.TicketIcon src={image} />
      <S.TicketStatus color={color}>{text}</S.TicketStatus>
    </S.TicketProvider>
  );
};

TicketProvider.propTypes = {
  item: PropTypes.object,
  setStep: PropTypes.func,
};

export default TicketProvider;
