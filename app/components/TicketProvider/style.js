import styled, { css } from 'styled-components';

export const TicketProvider = styled.div`
  border-radius: 2px;
  box-shadow: ${props =>
    props.status === 'soldout' ? 'none' : '0 2px 25px 6px rgba(0, 0, 0, 0.05)'};
  padding: 15px;
  text-align: center;
  cursor: ${props => (props.status === 'soldout' ? 'not-allowed' : 'pointer')};
  border: 1px solid
    ${props => (props.status === 'soldout' ? '#eee' : 'transparent')};
  transition: all 0.5s ease-in-out;
  ${props =>
    props.status !== 'soldout' &&
    css`
      &:hover {
        border-color: #130aba;
        .arrow {
          opacity: 1;
        }
      }
    `};
`;

export const TicketActiveIcon = styled.img`
  display: block;
  margin: 0 0 0 auto;
  background: white;
  position: relative;
  opacity: 0;
  transition: all 0.5s ease-in-out;
`;

export const TicketIcon = styled.img`
  width: 100px;
  height: 100px;
  object-fit: contain;
  margin: -10px 0 0;
`;

export const TicketStatus = styled.div`
  font-size: 10px;
  border: 1px solid ${props => props.color};
  color: ${props => props.color};
  border-radius: 8px;
  padding: 1px 10px;
  display: inline-block;
`;
