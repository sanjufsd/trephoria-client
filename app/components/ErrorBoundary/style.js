import styled from 'styled-components';

export const Text = styled.p`
  padding: 20px 35px;
  font-size: 12px;
  text-align: center;
  border: 1px solid #ddd;
  display: inline-block;
  margin: 10px;
  background: #efefef;
  border-radius: 5px;
`;
