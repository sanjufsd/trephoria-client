import styled from 'styled-components';

export const SocialWrapper = styled.div`
  border-radius: 15px;
  background-color: #635966;
  text-align: center;
  padding: 7px 0;
`;

export const SocialLink = styled.a`
  margin: 0 7px;
`;

export const SocialIcon = styled.img`
  width: 20px;
`;
