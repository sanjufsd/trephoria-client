/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const social = [
  {
    title: 'facebook',
    icon: `${apiUrl}/images/artist_facebook.png`,
    link: '#0',
  },
  {
    title: 'twitter',
    icon: `${apiUrl}/images/artist_twitter.png`,
    link: '#0',
  },
  {
    title: 'instagram',
    icon: `${apiUrl}/images/artist_instagram.png`,
    link: '#0',
  },
  {
    title: 'spotify',
    icon: `${apiUrl}/images/artist_spotify.png`,
    link: '#0',
  },
];
