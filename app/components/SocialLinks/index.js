/* eslint-disable react/no-array-index-key */
import React from 'react';
import * as S from './style';
import * as c from './constants';

const SocialLinks = props => (
  <S.SocialWrapper>
    {c.social.map(
      (item, index) =>
        props[item.title] && (
          <S.SocialLink
            key={index}
            target="_blank"
            href={props[item.title] || item.link}
          >
            <S.SocialIcon src={item.icon} />
          </S.SocialLink>
        ),
    )}
  </S.SocialWrapper>
);

export default SocialLinks;
