/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
import * as u from '../../config/url';

export const profilePic = `${apiUrl}/images/dummy-profile-pic.jpg`;
export const activeRightArrow = `${apiUrl}/images/active-link-right-arrow.svg`;
export const tabs = [
  {
    title: 'Activities',
    icon: `${apiUrl}/images/user-activities.svg`,
    activeIcon: `${apiUrl}/images/user-activities-active.svg`,
    url: u.userActivities,
  },
  {
    title: 'Concerts',
    icon: `${apiUrl}/images/user-concerts.svg`,
    activeIcon: `${apiUrl}/images/user-concerts-active.svg`,
    url: u.userConcerts,
  },
  {
    title: 'Artist List',
    icon: `${apiUrl}/images/user-artists.svg`,
    activeIcon: `${apiUrl}/images/user-artists-active.svg`,
    url: u.userArtists,
  },
  {
    title: 'Bookmarked',
    icon: `${apiUrl}/images/user-bookmark.svg`,
    activeIcon: `${apiUrl}/images/user-bookmark-active.svg`,
    url: u.userBookmarks,
  },
];
