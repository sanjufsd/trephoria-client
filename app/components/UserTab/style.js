import styled from 'styled-components';

export const Wrapper = styled.div`
  border-radius: 12px;
  border: solid 1px #e1e1e1;
  background-color: #ffffff;
  position: relative;
  overflow: hidden;
`;

export const Profile = styled.div`
  background-color: #18154f;
  color: white;
  text-align: center;
`;

export const Top = styled.div`
  border-bottom: 1px solid #ffffff38;
  padding: 20px 20px 10px;
`;

export const ProfilePic = styled.img`
  width: 100px;
  height: 100px;
  border-radius: 50%;
  object-fit: cover;
`;

export const Name = styled.h1`
  font-size: 18px;
  font-weight: bold;
  margin: 10px 0 3px;
`;

export const Place = styled.p`
  font-size: 12px;
  color: #ababab;
  margin: 0;
`;

export const Bottom = styled.div`
  padding: 15px;
  > a {
    font-size: 11px;
    background: white;
    color: #983fcc;
    border: 1px solid #983fcc;
    display: inline-block;
    padding: 5px 13px;
    border-radius: 20px;
    text-decoration: none;
  }
`;

export const CountWrapper = styled.div`
  font-size: 12px;
  margin-bottom: 10px;
`;

export const Count = styled.div`
  display: inline-block;
  margin: 0 7px;
`;

export const Tabs = styled.div`
  padding: 15px;
  position: relative;
  > a {
    display: block;
    font-size: 14px;
    padding: 10px 0;
    text-decoration: none;
    color: #030303;
    position: relative;
    padding: 12px 0 12px 65px;
  }
  > a.active {
    color: #ff5853;
    font-weight: bold;
  }
  &:before {
    content: '';
    width: 42px;
    height: calc(100% - 30px);
    border-radius: 16px;
    background-color: #f6f6f6;
    position: absolute;
    left: 15px;
    top: 15px;
  }
`;

export const ActiveIcon = styled.img`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translate(0, -50%);
`;

export const TabIconWrapper = styled.span`
  position: absolute;
  left: 0;
  width: 42px;
  text-align: center;
`;

export const TabIcon = styled.img``;
