import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as c from './constants';

const UserTab = props => {
  const { activeUrl } = props;
  return (
    <S.Wrapper>
      <S.Profile>
        <S.Top>
          <S.ProfilePic src={c.profilePic} />
          <S.Name>Naveen Karthick</S.Name>
          <S.Place>United Arab Emirates</S.Place>
        </S.Top>
        <S.Bottom>
          <S.CountWrapper>
            <S.Count>12 Groups</S.Count>
            <S.Count>10 Concerts</S.Count>
          </S.CountWrapper>
          <Link to="#0">View Profile</Link>
        </S.Bottom>
      </S.Profile>
      <S.Tabs>
        {c.tabs.map((item, index) => (
          <Link
            to={item.url}
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            className={activeUrl === item.url ? 'active' : ''}
          >
            <S.TabIconWrapper>
              <S.TabIcon
                src={activeUrl === item.url ? item.activeIcon : item.icon}
              />
            </S.TabIconWrapper>
            {item.title}
            {activeUrl === item.url && (
              <S.ActiveIcon src={c.activeRightArrow} />
            )}
          </Link>
        ))}
      </S.Tabs>
    </S.Wrapper>
  );
};

UserTab.propTypes = {
  activeUrl: PropTypes.string,
};

export default UserTab;
