import styled from 'styled-components';

export const Item = styled.div`
  width: 100%;
  position: relative;
  overflow: hidden;
  transition: all 0.5s ease-in-out;
  :hover {
    transform: scale(1.1);
  }
  a {
    display: block;
    text-decoration: none !important;
  }
`;

export const Title = styled.h5`
  height: 45px;
  font-size: 12px;
  text-transform: uppercase;
  margin: 0;
  background: white;
  display: flex;
  align-items: center;
  justify-content: center;
  color: black;
`;

export const ImgWrapper = styled.div``;

export const Img = styled.img`
  width: 100%;
  height: 150px;
  object-fit: cover;
`;
