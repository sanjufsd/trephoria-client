/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as u from '../../config/url';

const GenreMusicItem = props => {
  const {
    item: { image, title },
    _image,
  } = props;
  const url = title.toLowerCase();
  return (
    <S.Item>
      <Link to={`${u.singleGenre}/${url}`}>
        <S.ImgWrapper>
          <S.Img src={image || _image} alt="" />
        </S.ImgWrapper>
        <S.Title>{title}</S.Title>
      </Link>
    </S.Item>
  );
};

GenreMusicItem.propTypes = {
  item: PropTypes.object,
  _image: PropTypes.string,
};

export default GenreMusicItem;
