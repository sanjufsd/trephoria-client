import styled from 'styled-components';

export const Button = styled.button`
  border-radius: 20px;
  border: 0 !important;
  background-color: ${props => props.bgColor || '#983fcc'};
  font-size: 14px;
  color: ${props => props.textColor || 'white'};
  outline: 0 !important;
  padding: 7px;
  width: 100px;
  text-align: center;
`;
