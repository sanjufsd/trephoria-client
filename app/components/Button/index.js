import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';

const Button = props => {
  const { text } = props;
  return <S.Button {...props}>{text}</S.Button>;
};

Button.defaultProps = {
  type: 'submit',
};

Button.propTypes = {
  text: PropTypes.any,
  type: PropTypes.string,
};

export default Button;
