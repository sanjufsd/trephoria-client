import styled from 'styled-components';

export const TitleWrapper = styled.div`
  text-align: center;
  padding: 15px 10px;
  background: white;
  a {
    font-size: 11px;
    letter-spacing: -0.5px;
    color: #979797 !important;
  }
`;

export const Title = styled.h5`
  font-size: 15px;
  font-weight: 600;
  margin: 0;
`;

export const ImageWrapper = styled.div`
  position: relative;
  :hover .artistSocial {
    opacity: 1;
  }
`;

export const MainImg = styled.img`
  width: 100%;
`;

export const Bookmark = styled.button`
  position: absolute;
  top: 15px;
  right: 15px;
  outline: 0 !important;
  border: 0 !important;
  background: white;
  border-radius: 50%;
  padding: ${props => (props.bookmarked ? 0 : '2px 8px')};
  z-index: 2;
`;

export const BookmarkIcon = styled.img`
  width: ${props => (props.bookmarked ? 'auto' : '12px')};
`;

export const Social = styled.div`
  opacity: 0;
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: center;
  padding: 0 0 15px;
  z-index: 1;
  background: #391442a1;
  transition: all 0.5s ease-in-out;
`;

export const SocialBox = styled.div``;

export const SocialLink = styled.a`
  display: inline-block;
  width: 20px;
  margin: 0 5px;
  vertical-align: middle;
`;

export const SocialIcon = styled.img`
  width: 100%;
`;
