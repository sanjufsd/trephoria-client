/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const artistBookmark = `${apiUrl}/images/genre_bookmark.png`;
export const activeBookmark = `${apiUrl}/images/active-bookmark.svg`;
export const artistFacebook = `${apiUrl}/images/artist_facebook.png`;
export const artistInstagram = `${apiUrl}/images/artist_instagram.png`;
export const artistTwitter = `${apiUrl}/images/artist_twitter.png`;
export const artistSpotify = `${apiUrl}/images/artist_spotify.png`;
