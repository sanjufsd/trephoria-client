/* eslint-disable camelcase */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';

const ArtistItem = props => {
  const {
    item: { image_url, name },
    bookmarked,
  } = props;
  const artistUrl = name.toLowerCase();
  return (
    <Fragment>
      <S.ImageWrapper>
        <S.MainImg src={image_url} alt="" />
        <S.Bookmark bookmarked={bookmarked}>
          <S.BookmarkIcon
            bookmarked={bookmarked}
            src={bookmarked ? c.activeBookmark : c.artistBookmark}
            alt=""
          />
        </S.Bookmark>
        <S.Social className="artistSocial">
          <S.SocialBox>
            <S.SocialLink href="#0">
              <S.SocialIcon src={c.artistFacebook} alt="" />
            </S.SocialLink>
            <S.SocialLink href="#0">
              <S.SocialIcon src={c.artistTwitter} alt="" />
            </S.SocialLink>
            <S.SocialLink href="#0">
              <S.SocialIcon src={c.artistInstagram} alt="" />
            </S.SocialLink>
            <S.SocialLink href="#0">
              <S.SocialIcon src={c.artistSpotify} alt="" />
            </S.SocialLink>
          </S.SocialBox>
        </S.Social>
      </S.ImageWrapper>
      <S.TitleWrapper>
        <S.Title>{name}</S.Title>
        <Link to={`${u.singleArtist}/${artistUrl}`}>Upcoming Concerts</Link>
      </S.TitleWrapper>
    </Fragment>
  );
};

ArtistItem.propTypes = {
  item: PropTypes.object,
  bookmarked: PropTypes.bool,
};

export default ArtistItem;
