/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const concertBookmark = `${apiUrl}/images/genre_bookmark.png`;
export const activeBookmark = `${apiUrl}/images/active-bookmark.svg`;
export const concertClock = `${apiUrl}/images/concert_clock.png`;
export const concertDirection = `${apiUrl}/images/concert_direction.png`;
export const concertLocation = `${apiUrl}/images/concert_location.png`;
