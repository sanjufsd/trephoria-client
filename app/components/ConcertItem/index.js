import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';

const ConcertItem = props => {
  const {
    item: {
      date,
      month,
      title,
      image,
      tag,
      timing,
      timeLeft,
      location,
      address,
      distance,
    },
    bookmarked,
  } = props;
  return (
    <S.Item>
      <S.Heading>
        <S.Time>
          <S.Date>{date}</S.Date>
          <S.Month>{month}</S.Month>
        </S.Time>
        <S.Title>{title}</S.Title>
      </S.Heading>
      <S.ImgWrapper>
        <S.Tag>{tag}</S.Tag>
        <S.Img src={image} alt="" />
        <S.BookmarButton bookmarked={bookmarked}>
          <S.BookMarkImg
            bookmarked={bookmarked}
            src={bookmarked ? c.activeBookmark : c.concertBookmark}
            alt=""
          />
        </S.BookmarButton>
      </S.ImgWrapper>
      <S.Details>
        <S.DetailItem>
          <S.DetailInfo>
            <S.DetailIcon src={c.concertClock} alt="" />
            <S.MainText>{timing}</S.MainText>
            <S.SubText>{timeLeft}</S.SubText>
          </S.DetailInfo>
          <S.DetailInfo>
            <S.DetailIcon src={c.concertLocation} alt="" />
            <S.MainText>{location}</S.MainText>
            <S.SubText>{address}</S.SubText>
          </S.DetailInfo>
        </S.DetailItem>
        <S.ViewConcert>
          <S.Distance>
            <S.DistanceIcon src={c.concertDirection} alt="" />
            {distance}
          </S.Distance>
          <Link to={u.singleConcert}>View Concert</Link>
        </S.ViewConcert>
      </S.Details>
    </S.Item>
  );
};

ConcertItem.propTypes = {
  item: PropTypes.object,
  bookmarked: PropTypes.bool,
};

export default ConcertItem;
