import styled from 'styled-components';

export const Item = styled.div`
  border-radius: 6px;
  border: solid 1px #e3e3e3;
  background-color: #ffffff;
`;

export const Heading = styled.div`
  position: relative;
  padding-left: 60px;
  padding-right: 20px;
  display: flex;
  height: 65px;
  align-items: center;
`;

export const Time = styled.div`
  text-align: center;
  color: #f25d9d;
  width: 45px;
  line-height: 18px;
  position: absolute;
  left: 10px;
  top: 18px;
`;

export const Date = styled.div`
  font-size: 20px;
  font-weight: bold;
`;

export const Month = styled.div`
  font-size: 12px;
`;

export const Title = styled.h3`
  margin: 0;
  font-size: 18px;
  font-weight: bold;
`;

export const ImgWrapper = styled.div`
  position: relative;
`;

export const Tag = styled.div`
  position: absolute;
  left: 15px;
  top: 15px;
  font-size: 10px;
  background: #6f6eff;
  color: white;
  padding: 5px 25px;
  border-radius: 3px;
`;

export const Img = styled.img`
  width: 100%;
`;

export const BookmarButton = styled.button`
  position: absolute;
  top: 15px;
  right: 15px;
  background: white;
  border-radius: 50%;
  border: 0 !important;
  outline: 0 !important;
  padding: ${props => (props.bookmarked ? 0 : '0 7px')};
  width: 25px;
  height: 25px;
`;

export const BookMarkImg = styled.img`
  width: ${props => (props.bookmarked ? '25px' : '11px')};
`;

export const Details = styled.div``;

export const DetailItem = styled.div`
  padding: 20px 20px 0;
`;

export const DetailInfo = styled.div`
  margin: 0 0 20px;
  position: relative;
  padding-left: 40px;
`;

export const DetailIcon = styled.img`
  width: 13px;
  position: absolute;
  left: 2px;
  top: 50%;
  transform: translate(0, -50%);
`;

export const MainText = styled.p`
  font-size: 13px;
  font-weight: 500;
  margin: 0 0 -5px;
`;

export const SubText = styled.span`
  font-size: 12px;
  color: #a5a5a5;
`;

export const ViewConcert = styled.div`
  padding: 0 20px 25px;
  a {
    background: #983fcc;
    color: white !important;
    font-size: 12px;
    padding: 8px 25px;
    float: right;
    border-radius: 20px;
    text-decoration: none !important;
  }
`;

export const Distance = styled.div`
  display: inline-block;
  margin: 10px 0 0;
  font-size: 12px;
  color: #a5a5a5;
`;

export const DistanceIcon = styled.img`
  width: 13px;
  margin: 0 24px 0 3px;
`;
