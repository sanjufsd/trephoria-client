/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
import styled from 'styled-components';

export const SearchWrapper = styled.div`
  background: #e4e4e4;
  padding: 19px 0 15px;
  form {
    width: 100%;
    margin: 0;
    input {
      box-shadow: none;
    }
  }
`;

export const SearchFormWrapper = styled.div``;

export const SearchForm = styled.form`
  width: 550px;
  margin: 50px auto 0;
  position: relative;
  max-width: 100%;
  z-index: 3;
`;

export const SearchInput = styled.input`
  width: 100%;
  height: 49px;
  border-radius: ${props =>
    props.resultScreen
      ? props.full
        ? '8px 8px 0 0'
        : '4px 4px 0 0'
      : props.full
      ? '8px'
      : '4px'};
  box-shadow: 0 0 20px 0 rgba(98, 0, 63, 0.45);
  background-color: #ffffff;
  outline: 0;
  border: 0;
  font-size: 15px;
  padding: 12px 12px 12px 55px;
  z-index: 2;
  position: relative;
`;

export const SearchButton = styled.button`
  position: absolute;
  top: 0;
  left: 0;
  background: transparent;
  border: 0;
  outline: 0 !important;
  padding: 12px 20px;
  z-index: 2;
`;

export const SearchIcon = styled.img`
  width: 15px;
`;

export const SearchResults = styled.div`
  background: white;
  border-radius: 0 0 4px 4px;
  padding: 20px;
  text-align: left;
  position: absolute;
  top: 49px;
  z-index: 1;
  width: 100%;
`;

export const SearchBrands = styled.div``;

export const SearchBrandsTitle = styled.h4`
  font-size: 14px;
  color: #0b056d;
  margin: 0 0 10px;
  font-weight: 600;
`;

export const SearchBrandsGroup = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  li:last-child {
    margin: 0;
  }
`;

export const SearchBrandsItem = styled.li`
  margin: 0 0 10px;
  a {
    display: block;
    color: black !important;
    font-size: 14px;
    text-decoration: none;
  }
  ${'' /* span {
    display: block;
    color: grey;
    font-size: 14px;
  } */}
`;

export const SearchBrandsItemImg = styled.img`
  width: 40px;
  height: 40px;
  object-fit: cover;
  margin: 0 10px 0 0;
`;

export const SearchEvents = styled.div`
  margin: 20px 0 0;
`;

export const SearchEventsTitle = styled.h4`
  font-size: 14px;
  color: #0b056d;
  margin: 0 0 10px;
  font-weight: 600;
`;

export const SearchEventsGroup = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  li:last-child {
    margin: 0;
  }
`;

export const SearchEventsItem = styled.li`
  margin: 0 0 10px;
  a {
    display: block;
    color: black !important;
    font-size: 14px;
    text-decoration: none;
    padding: 0 90px 0 0;
    position: relative;
  }
`;
export const SearchEventsItemImg = styled.img`
  width: 40px;
  height: 40px;
  object-fit: cover;
  margin: 0 10px 0 0;
`;
export const SearchEventsDate = styled.span`
  font-size: 12px;
  color: #0b056d;
  position: absolute;
  right: 0;
  top: 12px;
`;

export const SearchFade = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  z-index: 1;
`;
