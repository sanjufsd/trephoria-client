/* eslint-disable indent */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Container } from 'reactstrap';
import { withRouter } from 'react-router';
import * as S from './style';
import * as c from './constants';
import { artistSearchUrl, artistBioUrl } from '../../config/apikey';
import { selectedArtist } from '../../redux/actions/artist';

class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      resultScreen: false,
      artists: [],
    };
  }

  handleChange = e => {
    const { value } = e.target;
    if (value.length > 2) {
      axios.get(`${artistSearchUrl}/${value}`).then(res => {
        if (res.data) {
          this.setState({ artists: res.data });
        }
      });
    }
    this.setState({ searchText: value });
  };

  handleFocus = () => {
    this.setState({ resultScreen: true });
  };

  handleBlur = () => {
    this.setState({ resultScreen: false });
  };

  updateArtistInfo = async artist => {
    const { dispatch } = this.props;
    let artistData = {};
    await axios
      .get(`${artistBioUrl}`, {
        params: {
          id: artist.id,
          inc: 'url-rels+genres',
        },
      })
      .then(res => {
        artistData = { artist, artistBio: res.data };
      })
      .catch(err => {
        console.log(err);
      });

    await dispatch(selectedArtist(artistData));
    this.props.history.push({
      pathname: `/artist/${artist.name}`,
      query: { name: artist.name, id: artist.id },
    });
  };

  render() {
    const { searchText, resultScreen, artists } = this.state;
    const { full } = this.props;
    return (
      <S.SearchFormWrapper>
        <S.SearchForm>
          <S.SearchInput
            type="text"
            placeholder="Search for artists or concerts"
            value={searchText}
            onChange={this.handleChange}
            resultScreen={resultScreen}
            onFocus={this.handleFocus}
            full={full}
          />
          <S.SearchButton>
            <S.SearchIcon src={full ? c.searchGreyIcon : c.searchIcon} alt="" />
          </S.SearchButton>
          {resultScreen && (searchText && searchText.length > 3) && (
            <S.SearchResults>
              <S.SearchBrands>
                <S.SearchBrandsTitle>{c.brandTitle}</S.SearchBrandsTitle>
                <S.SearchBrandsGroup>
                  {artists.length && searchText.length > 3
                    ? artists.map(item => (
                        <S.SearchBrandsItem key={item.id}>
                          <Link onClick={() => this.updateArtistInfo(item)}>
                            <S.SearchBrandsItemImg alt="" />
                            {item.name}
                          </Link>
                        </S.SearchBrandsItem>
                      ))
                    : searchText.length > 3 && (
                        <S.SearchBrandsItem>
                          <span>No Results Found </span>{' '}
                        </S.SearchBrandsItem>
                      )}
                </S.SearchBrandsGroup>
              </S.SearchBrands>
              {/* <S.SearchEvents>
                <S.SearchEventsTitle>{c.eventTitle}</S.SearchEventsTitle>
                <S.SearchEventsGroup>
                  {c.results.events.map((item, index) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <S.SearchEventsItem key={index}>
                      <Link to={item.url}>
                        <S.SearchEventsItemImg src={item.image} alt="" />
                        {item.title} @. <b>{item.location}</b>{' '}
                        <S.SearchEventsDate>{item.date}</S.SearchEventsDate>
                      </Link>
                    </S.SearchEventsItem>
                  ))}
                </S.SearchEventsGroup>
              </S.SearchEvents> */}
            </S.SearchResults>
          )}
        </S.SearchForm>
        {resultScreen && <S.SearchFade onClick={this.handleBlur} />}
      </S.SearchFormWrapper>
    );
  }
}

export const Search = props => {
  const { full } = props;
  return (
    <Fragment>
      {full ? (
        <S.SearchWrapper>
          <Container>
            <SearchForm {...props} />
          </Container>
        </S.SearchWrapper>
      ) : (
        <SearchForm {...props} />
      )}
    </Fragment>
  );
};

Search.propTypes = {
  full: PropTypes.bool,
};

SearchForm.propTypes = {
  full: PropTypes.bool,
  dispatch: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = state => state;

export default withRouter(connect(mapStateToProps)(Search));
