/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const searchIcon = `${apiUrl}/images/landing_search_icon.png`;
export const searchGreyIcon = `${apiUrl}/images/genre_search.png`;
export const results = {
  brands: [
    {
      title: 'Metallica',
      image: `${apiUrl}/images/search_brand_01.png`,
      url: '#0',
    },
    {
      title: 'Lady Gaga - Tribute Band',
      image: `${apiUrl}/images/search_brand_02.png`,
      url: '#0',
    },
  ],
  events: [
    {
      title: 'Metallica',
      image: `${apiUrl}/images/search_event_01.png`,
      location: 'Abu Dhabi - Du Arena',
      date: '19 July 2019',
      url: '#0',
    },
    {
      title: 'Metallica',
      image: `${apiUrl}/images/search_event_02.png`,
      location: 'Banglore - Palace Grounds',
      date: '21 July 2019',
      url: '#0',
    },
  ],
};

export const brandTitle = 'Brands';
export const eventTitle = 'Events';
