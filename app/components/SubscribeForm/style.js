import styled from 'styled-components';

export const FormWrapper = styled.div`
  width: 100%;
  padding: 0 50px 0 0;
  position: relative;
  .form-group {
    width: calc(100% - 150px);
    margin: 0;
    display: inline-block;
    input {
      border-radius: 4px;
      background-color: #ffffff;
      padding: 15px 20px;
      font-size: 14px;
      outline: 0 !important;
      border: 0 !important;
      box-shadow: none !important;
      width: 100%;
    }
    .invalid-feedback {
      position: absolute;
      bottom: -30px;
      display: block !important;
      color: white !important;
      font-size: 12px;
    }
  }
`;

export const Button = styled.button`
  width: 130px;
  height: 40px;
  border-radius: 20px;
  background-color: #983fcc;
  color: white;
  outline: 0 !important;
  border: 0;
  font-size: 14px;
  padding: 0;
  float: right;
  margin: 5px 0 0;
`;
