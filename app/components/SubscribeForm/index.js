import React from 'react';
import { FormGroup } from 'reactstrap';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import * as S from './style';

const subscribeSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email is invalid')
    .required('Email is required'),
});

const SubscribeForm = () => (
  <S.FormWrapper>
    <Formik
      initialValues={{
        email: '',
      }}
      validationSchema={subscribeSchema}
      // onSubmit={values => {
      //   console.log({ values });
      // }}
    >
      {({ errors, touched }) => (
        <Form>
          <FormGroup>
            <Field
              name="email"
              type="email"
              className={`${
                errors.email && touched.email ? ' is-invalid' : ''
              }`}
              autoComplete="off"
              placeholder="Enter email id"
            />
            <ErrorMessage
              name="email"
              component="div"
              className="invalid-feedback"
            />
          </FormGroup>
          <S.Button type="submit">Subscribe</S.Button>
        </Form>
      )}
    </Formik>
  </S.FormWrapper>
);

export default SubscribeForm;
