export const login = '/login';
export const signup = '/signup';
export const signupProcess = '/signup-process';
export const resetPassword = '/reset-password';
export const forgotPassword = '/forgot-password';
export const passwordSuccess = '/password-success';
export const signupSuccess = '/signup-success';
export const homepage = '/';
export const allGenre = '/genres';
export const singleGenre = '/genre'; // :id -> genre name
export const singleArtist = '/artist'; // : id -> artist name
export const singleConcert = '/concert';

export const userActivities = '/user/activities';
export const userConcerts = '/user/concerts';
export const userArtists = '/user/artists';
export const userBookmarks = '/user/bookmarks';
