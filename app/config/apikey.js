const devAppUrl = 'http://localhost:3000';
const devApiUrl = 'http://localhost:3004/api';
const productionAppUrl = 'http://trephoria.digifutura.com';
const productionApiUrl = 'http://trephoria.digifutura.com/api';
const bandsIntownUrl = 'https://rest.bandsintown.com';

export const appUrl =
  process.env.NODE_ENV === 'production' ? productionAppUrl : devAppUrl;
export const apiUrl =
  process.env.NODE_ENV === 'production' ? productionApiUrl : devApiUrl;

export const signupUrl = `${apiUrl}/users`;
export const loginUrl = `${apiUrl}/auth`;
export const genreUrl = `${apiUrl}/genres`;
export const artistUrl = `${apiUrl}/artists/genreartists`; // :id -> artist name
export const upcomingConcertUrl = `${apiUrl}/events/upcoming`; // :id -> artist name
export const artistSearchUrl = `${apiUrl}/artists/search`; // :id -> artist name
export const artistFind = `${apiUrl}/artists`;
export const artistBioUrl = `${apiUrl}/artist/`;
export const facebookAppId = '1088597931155576';
export const googleClientId =
  '658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com';
export const spotifyClientId = 'ac56fad434a3a3c1561e';
export const artistOnBIT = `${bandsIntownUrl}/artists/`;
