import React from 'react';
import Slider from 'react-slick';
import { Container } from 'reactstrap';
import { Link } from 'react-router-dom';
import {
  trendingConcertTitle,
  trendingConcertLeftArrow,
  trendingConcertRightArrow,
  trendingConcertLeftBg,
  trendingConcertRightBg,
  trendingConcerts,
} from './constants';
import * as S from './style';
import SlickSliderArrow from '../../components/SlickSliderArrow';
import TrendingConcertItem from '../../components/TrendingConcertItem';
import ErrorBoundary from '../../components/ErrorBoundary';

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  nextArrow: (
    <SlickSliderArrow icon={trendingConcertRightArrow} position="right" />
  ),
  prevArrow: (
    <SlickSliderArrow icon={trendingConcertLeftArrow} position="left" />
  ),
};

const TrendingConcertLocations = () => (
  <S.TrendingSection>
    <Container>
      <S.TrendingSectionWrapper>
        <S.TrendingConcertTitle>{trendingConcertTitle}</S.TrendingConcertTitle>
        <S.TrendingViewAll>
          <Link to="#0">View All</Link>
        </S.TrendingViewAll>
        <ErrorBoundary>
          <Slider {...settings}>
            {trendingConcerts.map((item, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <S.TrendingConcertContainer key={index}>
                <ErrorBoundary>
                  <TrendingConcertItem item={item} />
                </ErrorBoundary>
              </S.TrendingConcertContainer>
            ))}
          </Slider>
        </ErrorBoundary>
      </S.TrendingSectionWrapper>
    </Container>
    <S.TrendingBgImage>
      <S.TrendingLeftBgImage src={trendingConcertLeftBg} alt="" />
      <S.TrendingRightBgImage src={trendingConcertRightBg} alt="" />
    </S.TrendingBgImage>
  </S.TrendingSection>
);

export default TrendingConcertLocations;
