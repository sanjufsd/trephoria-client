/* eslint-disable no-unused-vars */
import React, { Fragment, useState, useEffect } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import Header from '../../components/Header';
import LandingSection from './landingSection';
import PopularMusic from './popularMusic';
import Vision from './vision';
import WorkFlow from './workflow';
import Subscribe from './subscribe';
import TrendingConcertLocations from './trendingConcertLocations';
import ErrorBoundary from '../../components/ErrorBoundary';
import Footer from '../../components/Footer';
import { Loader } from '../../components/Loader';
import { genreUrl } from '../../config/apikey';

const HomePage = props => {
  const [loading, setLoading] = useState(true);
  const [genreDatas, setGenredata] = useState([]);
  const [genreError, setGenreError] = useState('');
  useEffect(() => {
    axios
      .get(genreUrl)
      .then(res => {
        if (res.data.length > 0) {
          setGenredata(res.data);
        } else {
          setGenreError('No data available');
        }
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        setGenreError('No data available');
      });
  }, []);
  return (
    <Fragment>
      {!loading ? (
        <Fragment>
          <ErrorBoundary>
            <Header {...props} />
          </ErrorBoundary>
          <ErrorBoundary>
            <LandingSection />
          </ErrorBoundary>
          <ErrorBoundary>
            <PopularMusic data={genreDatas} error={genreError} />
          </ErrorBoundary>
          <ErrorBoundary>
            <Vision />
          </ErrorBoundary>
          <ErrorBoundary>
            <WorkFlow />
          </ErrorBoundary>
          <ErrorBoundary>
            <Subscribe />
          </ErrorBoundary>
          <ErrorBoundary>
            <TrendingConcertLocations />
          </ErrorBoundary>
          <ErrorBoundary>
            <Footer />
          </ErrorBoundary>
        </Fragment>
      ) : (
        <Loader bgColor="#010101" color="white" />
      )}
    </Fragment>
  );
};

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(HomePage);
