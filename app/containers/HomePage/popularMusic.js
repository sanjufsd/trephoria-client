import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import * as c from './constants';
import * as S from './style';
import * as u from '../../config/url';
import PopularGenreItem from '../../components/PopularGenreItem';
import ErrorBoundary from '../../components/ErrorBoundary';

const PopularMusic = props => {
  const { data, error } = props;
  const newData = data.length > 9 ? data.slice(1, 10) : data;
  return (
    <S.PopularMusicContainer id="popular_music_genre">
      <Container>
        <S.PopularMusicContainerTitle>
          {c.popularMusicTitle}
        </S.PopularMusicContainerTitle>
        <S.PopularMusicBgWrapper>
          <S.popularMusicBgImg src={c.popularMusicBgLayer} alt="" />
        </S.PopularMusicBgWrapper>
        {!error ? (
          <S.PopularMusicGenres>
            <Row>
              {newData.map((item, index) => (
                <Col lg="4" key={item.id}>
                  <S.PopularMusicGenreItemWrapper>
                    {index !== 7 ? (
                      <ErrorBoundary>
                        <PopularGenreItem
                          item={item}
                          position={index}
                          _image={c.popularMusicGenreLists[index].image}
                        />
                      </ErrorBoundary>
                    ) : (
                      <S.PopularMusicViewAll>
                        <Link to={u.allGenre}>View All</Link>
                      </S.PopularMusicViewAll>
                    )}
                  </S.PopularMusicGenreItemWrapper>
                </Col>
              ))}
            </Row>
          </S.PopularMusicGenres>
        ) : (
          <S.ErrorMsg>{error}</S.ErrorMsg>
        )}
      </Container>
    </S.PopularMusicContainer>
  );
};

PopularMusic.propTypes = {
  data: PropTypes.array,
  error: PropTypes.string,
};

export default PopularMusic;
