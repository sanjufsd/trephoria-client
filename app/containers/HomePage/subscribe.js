import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import {
  subscribeMsgIcon,
  subscribeLeftCircle,
  subscribeTitle,
  subscribeNote,
} from './constants';
import * as S from './style';
import SubscribeForm from '../../components/SubscribeForm';
import ErrorBoundary from '../../components/ErrorBoundary';

const Subscribe = () => (
  <S.SubscribeSection>
    <Container>
      <Row>
        <Col lg="6">
          <S.SubscribeNoteWrapper>
            <S.SubscribeMsgIcon src={subscribeMsgIcon} alt="" />
            <S.SubscribeMsg>
              <S.SubscribeTitle>{subscribeTitle}</S.SubscribeTitle>
              <S.SubscribeNote>{subscribeNote}</S.SubscribeNote>
            </S.SubscribeMsg>
          </S.SubscribeNoteWrapper>
        </Col>
        <Col lg="6">
          <ErrorBoundary>
            <SubscribeForm />
          </ErrorBoundary>
        </Col>
      </Row>
    </Container>
    <S.SubscribeLeftCircle src={subscribeLeftCircle} alt="" />
  </S.SubscribeSection>
);

export default Subscribe;
