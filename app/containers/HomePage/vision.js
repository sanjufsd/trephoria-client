import React from 'react';
import { Container } from 'reactstrap';
import {
  visionTitle,
  visionSubTitle,
  visionDescrption,
  visionQuote,
  visionQuoteAuthor,
  visionContent,
  visionLeftBg,
  visionQuoteIcon,
  visionQuoteBg,
} from './constants';
import * as S from './style';
import * as G from '../../global-styles';

const Vision = () => (
  <S.VisionSection id="about_us">
    <S.VisionLeftImg src={visionLeftBg} alt="" />
    <Container>
      <S.VisionTitle>{visionTitle}</S.VisionTitle>
      <S.VisionSubTitle>{visionSubTitle}</S.VisionSubTitle>
      <S.VisionDescription>{visionDescrption}</S.VisionDescription>
      <S.VisionContent>
        <S.VisionQuote>
          <S.VisionQuoteBg src={visionQuoteBg} alt="" />
          <G.Div>
            <G.Div>
              <S.VisionQuoteIcon src={visionQuoteIcon} alt="" />
              <S.VisionQuoteTitle>{visionQuote}</S.VisionQuoteTitle>
              <S.VisionQuoteAuthor>{visionQuoteAuthor}</S.VisionQuoteAuthor>
            </G.Div>
          </G.Div>
        </S.VisionQuote>
        {visionContent.map((item, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <S.VisionItem key={index}>
            <S.VisionItemImg src={item.image} alt="" />
            <S.VisionItemDescription itemColor={item.color}>
              {item.description}
            </S.VisionItemDescription>
          </S.VisionItem>
        ))}
      </S.VisionContent>
    </Container>
  </S.VisionSection>
);

export default Vision;
