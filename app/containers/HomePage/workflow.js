import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import {
  workflowTitle,
  workflowImg,
  workflowItems,
  workflowPlayIcon,
  workflowRightCircle,
  workflowBg,
} from './constants';
import * as S from './style';

const WorkFlow = () => (
  <S.WorkSection id="how_it_works" bgImg={workflowBg}>
    <Container>
      <Row>
        <Col md="6">
          <S.WorkflowImgWrapper>
            <S.WorkflowImg src={workflowImg} alt="" />
            <S.WorkFlowButton type="button">
              <S.WorkFlowButtonIcon src={workflowPlayIcon} alt="" />
            </S.WorkFlowButton>
          </S.WorkflowImgWrapper>
        </Col>
        <Col md="6">
          <S.WorkflowContentWrapper>
            <S.WorkFlowTitle>{workflowTitle}</S.WorkFlowTitle>
            <S.WorkflowContent>
              {workflowItems.map((item, index) => (
                // eslint-disable-next-line react/no-array-index-key
                <S.WorkflowItem key={index}>
                  <S.WorkflowItemImages>
                    <S.WorkflowItemImage src={item.number} alt="" />
                    <S.WorkflowItemImage src={item.icon} alt="" />
                  </S.WorkflowItemImages>
                  <S.WorkflowItemText>
                    <S.WorkFlowItemColoredText itemColor={item.color}>
                      {item.coloredText}
                    </S.WorkFlowItemColoredText>
                    &nbsp;{item.plainText}
                  </S.WorkflowItemText>
                </S.WorkflowItem>
              ))}
            </S.WorkflowContent>
          </S.WorkflowContentWrapper>
        </Col>
      </Row>
    </Container>
    <S.WorkFlowRightCircleImg src={workflowRightCircle} alt="" />
  </S.WorkSection>
);

export default WorkFlow;
