import { apiUrl } from '../../config/apikey';
/* eslint-disable global-require */
export const homepageTopText = 'Drop your earphones and go out to see';
export const homepageBottomText = 'your favourite artist live in concert';

export const landingImage = `${apiUrl}/images/landing_image.png`;
export const scrollDownIcon = `${apiUrl}/images/scroll_down.png`;

export const popularMusicBgLayer = `${apiUrl}/images/popular_music_bg_layer.png`;
export const popularMusicTitle = 'Popular Music Genres';

export const popularMusicGenreLists = [
  {
    name: 'HIP HOP MUSIC',
    image: `${apiUrl}/images/popular_genre_1.png`,
  },
  {
    name: 'JAZZ MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_01.png`,
  },
  {
    name: 'HEAVY METAL',
    image: `${apiUrl}/images/popular_genre_3.png`,
  },
  {
    name: 'FUNK MUSIC',
    image: `${apiUrl}/images/popular_genre_4.png`,
  },
  {
    name: 'CLASSICAL MUSIC',
    image: `${apiUrl}/images/popular_genre_5.png`,
  },
  {
    name: 'musical theater',
    image: `${apiUrl}/images/popular_genre_6.png`,
  },
  {
    name: 'reggae',
    image: `${apiUrl}/images/popular_genre_7.png`,
  },
  {
    name: 'Rock MUSIC',
    image: `${apiUrl}/images/popular_genre_9.png`,
  },
  {
    name: 'Rock MUSIC',
    image: `${apiUrl}/images/popular_genre_9.png`,
  },
];

export const visionTitle = 'About Us';
export const visionSubTitle = `Trephoria’s Vision`;
export const visionDescrption =
  'To connect concertgoers and create personalized concert experience.';
export const visionQuote = 'Music has the power to bring people together.';
export const visionQuoteAuthor = '- Michael Franti';
export const visionContent = [
  {
    description: 'Want to see your favourite artist perform live??',
    image: `${apiUrl}/images/vision_star.png`,
    color: '#3535c7',
  },
  {
    description: 'Want to share your concert experience with the world?',
    image: `${apiUrl}/images/vision_share.png`,
    color: '#d23244',
  },
  {
    description: 'Want to connect with other Concertgoers??',
    image: `${apiUrl}/images/vision_connect.png`,
    color: '#f86b27',
  },
  {
    description: 'Want to be part of Concertgoers community?',
    image: `${apiUrl}/images/vision_community.png`,
    color: '#2b85d7',
  },
];
export const visionLeftBg = `${apiUrl}/images/vision_left_bg.png`;
export const visionQuoteIcon = `${apiUrl}/images/vision_quote.png`;
export const visionQuoteBg = `${apiUrl}/images/vision_quote_bg.png`;

export const workflowTitle = 'How It Works';
export const workflowImg = `${apiUrl}/images/work_flow_concert.png`;
export const workflowBg = `${apiUrl}/images/work_flow_bg.png`;
export const workflowPlayIcon = `${apiUrl}/images/work_flow_play.png`;
export const workflowRightCircle = `${apiUrl}/images/work_flow_right_circle.png`;
export const workflowItems = [
  {
    coloredText: 'Choose the artist',
    plainText: 'you want to see LIVE and let Trephoria do its magic!',
    number: `${apiUrl}/images/work_flow_01.png`,
    icon: `${apiUrl}/images/work_flow_01_guitarist.png`,
    color: '#f4c43b',
  },
  {
    coloredText: 'Join exclusive groups',
    plainText: 'to connect and socialize with other concertgoers.',
    number: `${apiUrl}/images/work_flow_02.png`,
    icon: `${apiUrl}/images/work_flow_02_people.png`,
    color: '#59b8ff',
  },
  {
    coloredText: 'Share your concert experience',
    plainText: 'with the world and be a part of global community!',
    number: `${apiUrl}/images/work_flow_03.png`,
    icon: `${apiUrl}/images/work_flow_03_image.png`,
    color: '#e55d33',
  },
];

export const subscribeMsgIcon = `${apiUrl}/images/subscribe_msg.png`;
export const subscribeLeftCircle = `${apiUrl}/images/subscribe_left_circle.png`;
export const subscribeTitle = `Don’t miss updates from us!`;
export const subscribeNote = `Subscribe to our email and get updates right in your inbox`;

export const trendingConcertTitle = 'Trending Concert Locations';
export const trendingConcertLeftArrow = `${apiUrl}/images/trending_concert_left_arrow.png`;
export const trendingConcertRightArrow = `${apiUrl}/images/trending_concert_right_arrow.png`;
export const trendingConcertLeftBg = `${apiUrl}/images/trending_concert_left_bg.png`;
export const trendingConcertRightBg = `${apiUrl}/images/trending_concert_right_bg.png`;
export const trendingConcerts = [
  {
    title: 'Madison Square Garden',
    capacity: '20,789',
    image: `${apiUrl}/images/trending_concert_01.png`,
    city: 'New York City',
    description: `Madison Square Garden (MSG) is a landmark indoor arena located in Manhattan. The Garden that we know today is the…..`,
    upcomingConcrets: '8+',
    location: '4 Pennsylvania Plaza, New York, NY 10001, USA',
    phone: '+1 212-465-6741',
    url: '#0',
  },
  {
    title: 'Red Rocks Amphitheatre',
    capacity: '9,525',
    image: `${apiUrl}/images/trending_concert_02.png`,
    city: 'Morrison, USA',
    description: `Red Rocks Amphitheatre is an open-air amphitheatre built into a rock structure near Morrison, Colorado, 10 miles west of Denver.`,
    upcomingConcrets: '3+',
    location: '18300 W Alameda Pkwy, Morrison, CO 80465, USA',
    phone: '+1 720-865-2494',
    url: '#0',
  },
  {
    title: 'Sydney Opera House',
    capacity: '20,789',
    image: `${apiUrl}/images/trending_concert_03.png`,
    city: 'Sydney, Australia',
    description: `The Sydney Opera House is a multi-venue performing arts centre at Sydney Harbour in Sydney, New South Wales, Australia.`,
    upcomingConcrets: '8+',
    location: 'Bennelong Point, Sydney NSW 2000, Australia',
    phone: '+61 2 9250 7111',
    url: '#0',
  },
  {
    title: 'Madison Square Garden',
    capacity: '20,789',
    image: `${apiUrl}/images/trending_concert_01.png`,
    city: 'New York City',
    description: `Madison Square Garden (MSG) is a landmark indoor arena located in Manhattan. The Garden that we know today is the…..`,
    upcomingConcrets: '8+',
    location: '4 Pennsylvania Plaza, New York, NY 10001, USA',
    phone: '+1 212-465-6741',
    url: '#0',
  },
  {
    title: 'Red Rocks Amphitheatre',
    capacity: '9,525',
    image: `${apiUrl}/images/trending_concert_02.png`,
    city: 'Morrison, USA',
    description: `Red Rocks Amphitheatre is an open-air amphitheatre built into a rock structure near Morrison, Colorado, 10 miles west of Denver.`,
    upcomingConcrets: '3+',
    location: '18300 W Alameda Pkwy, Morrison, CO 80465, USA',
    phone: '+1 720-865-2494',
    url: '#0',
  },
  {
    title: 'Sydney Opera House',
    capacity: '20,789',
    image: `${apiUrl}/images/trending_concert_03.png`,
    city: 'Sydney, Australia',
    description: `The Sydney Opera House is a multi-venue performing arts centre at Sydney Harbour in Sydney, New South Wales, Australia.`,
    upcomingConcrets: '8+',
    location: 'Bennelong Point, Sydney NSW 2000, Australia',
    phone: '+61 2 9250 7111',
    url: '#0',
  },
];
