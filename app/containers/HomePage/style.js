import styled from 'styled-components';

export const HomePageSection = styled.div`
  position: relative;
`;

export const HomepageBgImage = styled.img`
  width: 100%;
`;

export const HomePageText = styled.div`
  position: absolute;
  top: 30%;
  left: 0;
  width: 100%;
  text-align: center;
  color: white;
`;

export const HomePageTopText = styled.p`
  font-size: 20px;
  font-weight: 300;
  letter-spacing: 5.56px;
`;

export const HomePageBottomText = styled.h3`
  font-size: 35px;
  font-weight: bold;
  letter-spacing: 10px;
`;

export const LeftCircle = styled.div`
  position: absolute;
  left: 0;
  z-index: 2;
  top: 25%;
`;

const LeftCircleDiv = styled.div`
  opacity: 0.15;
  border: solid 1px #ffffff;
  border-radius: 50%;
  position: absolute;
`;

export const LeftSmallCircle = styled(LeftCircleDiv)`
  width: 250px;
  height: 250px;
  left: -125px;
  top: 0;
`;

export const LeftBigCircle = styled(LeftCircleDiv)`
  width: 500px;
  height: 500px;
  left: -250px;
  top: -125px;
`;

export const ScrollDown = styled.div`
  position: absolute;
  right: 15%;
  bottom: 0;
  cursor: pointer;
`;

export const ScrollDownIcon = styled.img`
  width: 24px;
`;

const ScrollDownCircle = styled.div`
  opacity: 0.15;
  border: solid 1px #ffffff;
  border-radius: 50%;
  position: absolute;
  z-index: 2;
  img {
    height: 35px;
  }
`;

export const ScrollDownSmallCircle = styled(ScrollDownCircle)`
  width: 100px;
  height: 100px;
  right: -38px;
  bottom: -33px;
`;

export const ScrollDownBigCircle = styled(ScrollDownCircle)`
  width: 230px;
  height: 230px;
  right: -105px;
  bottom: -95px;
`;

export const PopularMusicContainer = styled.div`
  background: #010101;
  padding: 50px 0 0;
  position: relative;
`;

export const PopularMusicContainerTitle = styled.h2`
  font-size: 26px;
  font-weight: bold;
  line-height: 1.15;
  text-align: center;
  color: #ffffff;
  margin: 0;
`;

export const PopularMusicBgWrapper = styled.div`
  width: 100%;
  position: relative;
`;

export const popularMusicBgImg = styled.img`
  width: 100%;
  position: absolute;
  left: 0;
  top: 0;
`;

export const PopularMusicGenres = styled.div`
  width: 780px;
  margin: 0 auto;
  max-width: 100%;
`;

export const PopularMusicGenreItemWrapper = styled.div`
  width: calc(100% + 20px);
  height: 260px;
  margin: 0 -10px 10px;
`;

export const PopularMusicViewAll = styled.div`
  margin: 0;
  transform: translate(0, 100px);
  text-align: center;
  a {
    color: white !important;
    background: #983fcc;
    font-size: 15px;
    display: inline-block;
    text-decoration: none !important;
    padding: 8px;
    border-radius: 20px;
    width: 130px;
  }
`;

export const VisionSection = styled.div`
  background: white;
  position: relative;
  padding: 50px 0;
  text-align: center;
`;

export const VisionLeftImg = styled.img`
  position: absolute;
  left: 0;
  top: 50px;
  width: 420px;
`;

export const VisionTitle = styled.h2`
  opacity: 0.1;
  font-size: 20px;
  font-weight: bold;
  line-height: 1.5;
  letter-spacing: 3px;
  color: #1d1d1d;
  margin: 0 0 20px;
`;

export const VisionSubTitle = styled.h3`
  font-size: 26px;
  font-weight: bold;
  line-height: 1.15;
  color: #1d1d1d;
  margin: 0 0 15px;
`;

export const VisionDescription = styled.h4`
  width: 340px;
  font-size: 14px;
  line-height: 1.43;
  letter-spacing: 0.5px;
  color: #8a8a8a;
  margin: 0 auto;
`;

export const VisionContent = styled.div`
  max-width: 100%;
  width: 760px;
  margin: 0 0 0 auto;
  z-index: 3;
  position: relative;
  min-height: 455px;
`;

export const VisionQuote = styled.div`
  position: absolute;
  left: 0;
  width: 230px;
  bottom: 0;
  > div {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 35px 40px 15px;
  }
`;

export const VisionQuoteBg = styled.img`
  width: 220px;
`;

export const VisionQuoteIcon = styled.img`
  width: 65px;
`;

export const VisionQuoteTitle = styled.h5`
  font-size: 18px;
  line-height: 1.33;
  letter-spacing: 0.07px;
  color: #050505;
  margin: 15px 0;
`;

export const VisionQuoteAuthor = styled.p`
  font-size: 12px;
  line-height: 1.67;
  letter-spacing: 0.43px;
  color: #636363;
  margin: 0;
`;

export const VisionItem = styled.div`
  width: 150px;
  position: absolute;
  left: 280px;
  top: 60px;
  :nth-child(3) {
    bottom: 15px;
    top: auto;
    left: 375px;
  }
  :nth-child(4) {
    right: 100px;
    left: auto;
    top: 5px;
  }
  :nth-child(5) {
    bottom: 75px;
    top: auto;
    left: auto;
    right: 0;
  }
`;

export const VisionItemImg = styled.img`
  height: 80px;
`;

export const VisionItemDescription = styled.p`
  font-size: 18px;
  line-height: 1.33;
  letter-spacing: 0.07px;
  margin: 10px 0 0;
  color: ${props => props.itemColor || 'black'};
`;

export const WorkSection = styled.div`
  padding: 50px 0 80px;
  background-color: black;
  background-image: url(${props => props.bgImg});
  position: relative;
  overflow: hidden;
  background-position: bottom;
  background-size: cover;
`;

export const WorkFlowRightCircleImg = styled.img`
  width: 220px;
  position: absolute;
  right: 0;
  top: 50px;
`;

export const WorkflowImgWrapper = styled.div`
  margin: 75px 25px 0 0;
  position: relative;
`;

export const WorkflowImg = styled.img`
  width: 100%;
`;

export const WorkFlowButton = styled.button`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  background: transparent;
  outline: 0 !important;
  border: 0;
`;

export const WorkFlowButtonIcon = styled.img`
  width: 70px;
`;

export const WorkflowContentWrapper = styled.div`
  color: white;
`;

export const WorkFlowTitle = styled.h2`
  font-size: 20px;
  font-weight: bold;
  line-height: 1.5;
  letter-spacing: 3px;
`;

export const WorkflowContent = styled.div``;

export const WorkflowItem = styled.div`
  margin-top: 30px;
  div {
    img:nth-child(1) {
      height: 120px;
      margin: 0 0 0 -20px;
    }
    img:nth-child(2) {
      height: 60px;
      vertical-align: top;
      margin: 23px 0 0 10px;
    }
  }
  :nth-child(2) {
    padding-left: 100px;
  }
`;

export const WorkflowItemImages = styled.div``;

export const WorkflowItemImage = styled.img``;

export const WorkflowItemText = styled.p`
  width: 295px;
  font-size: 16px;
  line-height: 1.25;
  letter-spacing: 0.06px;
  max-width: 100%;
  margin: 0;
`;

export const WorkFlowItemColoredText = styled.span`
  color: ${props => props.itemColor || 'white'};
`;

export const SubscribeSection = styled.div`
  background-color: #0b056d;
  padding: 50px 0;
  color: white;
  position: relative;
  overflow: hidden;
`;

export const SubscribeLeftCircle = styled.img`
  position: absolute;
  left: 0;
  top: 50%;
  width: 60px;
  transform: translate(0, -50%);
`;

export const SubscribeNoteWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

export const SubscribeMsg = styled.div`
  padding: 0 0 0 35px;
`;

export const SubscribeTitle = styled.h4`
  font-size: 26px;
  font-weight: bold;
  line-height: 1.15;
  margin: 0 0 10px;
`;

export const SubscribeNote = styled.p`
  font-size: 14px;
  line-height: 1.43;
  letter-spacing: 0.5px;
  color: #a7a7a7;
  margin: 0;
`;

export const SubscribeMsgIcon = styled.img`
  width: 55px;
  height: 55px;
  transform: translate(0, 5px);
`;

export const TrendingSection = styled.div`
  padding: 70px 0;
  background-color: #221e23;
  color: white;
  position: relative;
  overflow: hidden;
`;

export const TrendingSectionWrapper = styled.div`
  padding: 0 50px;
`;

export const TrendingConcertTitle = styled.h2`
  font-size: 26px;
  font-weight: bold;
  line-height: 0.69;
`;

export const TrendingBgImage = styled.div`
  img {
    position: absolute;
  }
`;

export const TrendingLeftBgImage = styled.img`
  left: 0;
  bottom: 50px;
  width: 130px;
`;

export const TrendingRightBgImage = styled.img`
  right: 0;
  top: 50px;
  width: 80px;
`;

export const TrendingViewAll = styled.div`
  text-align: right;
  margin: -15px 0 20px;
  a {
    width: 90px;
    background-color: #983fcc;
    border-radius: 20px;
    color: white;
    text-decoration: none;
    display: inline-block;
    padding: 7px 0;
    font-size: 11px;
    text-align: center;
  }
`;
export const TrendingConcertContainer = styled.div`
  padding: 10px;
`;

export const ErrorMsg = styled.div`
  color: white;
  margin: 100px 0;
  font-size: 15px;
  text-align: center;
`;
