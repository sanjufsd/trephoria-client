import React from 'react';
import { Container } from 'reactstrap';
import {
  homepageTopText,
  homepageBottomText,
  landingImage,
  scrollDownIcon,
} from './constants';
import * as S from './style';
import ErrorBoundary from '../../components/ErrorBoundary';
import Search from '../../components/Search';
import { scrollToDiv } from '../../components/ScrollToDiv';

const LandingSection = () => (
  <S.HomePageSection id="home">
    <S.HomepageBgImage src={landingImage} alt="" />
    <S.HomePageText>
      <Container>
        <S.HomePageTopText>{homepageTopText}</S.HomePageTopText>
        <S.HomePageBottomText>{homepageBottomText}</S.HomePageBottomText>
        <ErrorBoundary>
          <Search />
        </ErrorBoundary>
      </Container>
    </S.HomePageText>
    <S.LeftCircle>
      <S.LeftSmallCircle />
      <S.LeftBigCircle />
    </S.LeftCircle>
    <S.ScrollDown
      onClick={() => {
        scrollToDiv('popular_music_genre');
      }}
    >
      <S.ScrollDownSmallCircle />
      <S.ScrollDownBigCircle />
      <S.ScrollDownIcon src={scrollDownIcon} alt="" />
    </S.ScrollDown>
  </S.HomePageSection>
);

export default LandingSection;
