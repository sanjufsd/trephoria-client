import styled from 'styled-components';

export const SignupFormContainer = styled.div``;

export const FormWrapper = styled.div`
  form {
    padding: 15px 0;
    .form-group {
      margin-bottom: 10px;
      > label {
        font-size: 12px;
        margin: 0 0 10px;
        display: block;
      }
      > input {
        box-shadow: none !important;
        border-color: #363138;
        outline: 0;
        font-size: 12px;
        padding: 10px 15px !important;
        height: auto !important;
        color: black;
        background-image: none;
      }
    }
    .invalid-feedback {
      font-size: 12px;
      margin: 10px 0 0;
    }
  }
`;

export const Button = styled.button`
  border-radius: 20px;
  border: 0 !important;
  background-color: #983fcc;
  font-size: 14px;
  color: white;
  outline: 0 !important;
  padding: 7px;
  width: 100px;
  text-align: center;
`;

export const PasswordField = styled.div`
  position: relative;
`;

export const ViewPassword = styled.span`
  cursor: pointer;
  display: block;
  position: absolute;
  right: 2px;
  top: 30px;
  padding: 6px 10px;
  background: white;
`;

export const EyeIcon = styled.img`
  width: 22px;
`;

export const SignupSuccessContainer = styled.div`
  padding: 30px 0 80px;
  a {
    border-radius: 20px;
    border: solid 2px #983fcc;
    background-color: #983fcc;
    display: inline-block;
    color: white;
    padding: 5px 30px;
    text-decoration: none;
    font-size: 14px;
  }
`;

export const VerifyMsg = styled.p`
  font-size: 12px;
  color: #929699;
  margin: 0 0 30px;
  width: 200px;
`;

export const SuccessMsg = styled.h2`
  font-size: 20px;
  font-weight: bold;
  line-height: 1.3;
  color: #3e4c5b;
  max-width: 100%;
  margin: 0 0 15px;
  width: 200px;
`;

export const SuccessIcon = styled.img`
  width: 83px;
  margin: 0 0 20px;
`;

export const SignupSuccessStep = styled.h3`
  font-size: 14px;
  margin: 0 0 80px;
  font-weight: bold;
`;

export const SignupSuccessImgWrapper = styled.div`
  width: 400px;
  position: absolute;
  bottom: 0;
  left: 50%;
  transform: translate(-50%, 0);
`;

export const SignupSuccessImg = styled.img`
  width: 100%;
`;

export const SignupSuccessQuote = styled.div`
  position: absolute;
  right: 50px;
  top: 50px;
  width: 210px;
  padding: 25px;
  border-radius: 10px;
  box-shadow: 0 -2px 17px 0 rgba(0, 0, 0, 0.11);
`;

export const QuoteIcon = styled.img`
  width: 18px;
`;

export const QuoteMsg = styled.h5`
  font-size: 14px;
  line-height: 1.43;
  letter-spacing: 0.58px;
  color: #34404d;
  margin: 10px 0;
`;

export const QuoteDetails = styled.div`
  position: relative;
  padding-left: 45px;
`;

export const AuthorImg = styled.img`
  width: 35px;
  border-radius: 50%;
  position: absolute;
  left: 0;
  top: 0;
`;

export const AuthorName = styled.p`
  font-size: 10px;
  color: #34404d;
  margin: 0 0 -2px;
`;

export const AuthorPost = styled.span`
  font-size: 10px;
  color: #aaaaaa;
`;
