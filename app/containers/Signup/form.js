import React, { useState } from 'react';
import { FormGroup, Label } from 'reactstrap';
import { connect } from 'react-redux';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import * as S from './style';
import * as u from '../../config/url';
import Button from '../../components/Button';
import { signupDetails } from '../../redux/actions/signup';

const signupSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email is invalid')
    .required('Email is required'),
  password: Yup.string()
    .min(6, 'Password must be at least 6 characters')
    .required('Password is required'),
});

const SignupFormWrapper = props => {
  const {
    eye,
    dispatch,
    history: { push },
  } = props;
  const [viewPass, setViewPass] = useState(false);
  return (
    <S.SignupFormContainer>
      <S.FormWrapper>
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          validationSchema={signupSchema}
          onSubmit={values => {
            dispatch(signupDetails(values));
            push(u.signupProcess);
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <FormGroup>
                <Label>Email*</Label>
                <Field
                  name="email"
                  type="email"
                  className={`form-control${
                    errors.email && touched.email ? ' is-invalid' : ''
                  }`}
                  autoComplete="off"
                  placeholder="Enter your email id"
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="invalid-feedback"
                />
              </FormGroup>
              <S.PasswordField>
                <FormGroup>
                  <Label>Password*</Label>
                  <Field
                    name="password"
                    type={viewPass ? 'text' : 'password'}
                    className={`form-control${
                      errors.password && touched.password ? ' is-invalid' : ''
                    }`}
                    placeholder="Enter your password"
                  />
                  <ErrorMessage
                    name="password"
                    component="div"
                    className="invalid-feedback"
                  />
                </FormGroup>
                <S.ViewPassword
                  onClick={() => {
                    setViewPass(!viewPass);
                  }}
                >
                  <S.EyeIcon src={eye} alt="" />
                </S.ViewPassword>
              </S.PasswordField>
              <Button type="submit" text="Sign Up" />
            </Form>
          )}
        </Formik>
      </S.FormWrapper>
    </S.SignupFormContainer>
  );
};

SignupFormWrapper.propTypes = {
  eye: PropTypes.string,
  dispatch: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = state => state;

export default connect(mapStateToProps)(SignupFormWrapper);
