import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import { removeSignupDetails } from '../../redux/actions/signup';

const SignupSuccess = props => {
  const { dispatch, signup } = props;
  useEffect(() => {
    if (Object.keys(signup).length > 0) {
      dispatch(removeSignupDetails());
    }
  }, []);
  return (
    <S.SignupSuccessContainer>
      <S.SignupSuccessStep>
        Just a step away from Awesomness….
      </S.SignupSuccessStep>
      <S.SuccessIcon src={c.successIcon} alt="" />
      <S.SuccessMsg>Sign Up Successfull With Trephoria</S.SuccessMsg>
      <S.VerifyMsg>
        Please check your registered email to verify account and become a
        trephorian
      </S.VerifyMsg>
      <Link to={u.login}>Login</Link>
      <S.SignupSuccessQuote>
        <S.QuoteIcon src={c.quoteIcon} alt="" />
        <S.QuoteMsg>Music is like a dream. One that I cannot hear</S.QuoteMsg>
        <S.QuoteDetails>
          <S.AuthorImg src={c.quoteAuthor} alt="" />
          <S.AuthorName>Ludwig Van Beethoven</S.AuthorName>
          <S.AuthorPost>Composer and Pianist</S.AuthorPost>
        </S.QuoteDetails>
      </S.SignupSuccessQuote>
      <S.SignupSuccessImgWrapper>
        <S.SignupSuccessImg src={c.signupSuccessRightImg} alt="" />
      </S.SignupSuccessImgWrapper>
    </S.SignupSuccessContainer>
  );
};

SignupSuccess.propTypes = {
  dispatch: PropTypes.func,
  signup: PropTypes.object,
};

const mapStateToProps = ({ signup }) => ({ signup });

export default connect(mapStateToProps)(SignupSuccess);
