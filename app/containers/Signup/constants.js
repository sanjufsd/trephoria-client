/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const successIcon = `${apiUrl}/images/password_success_icon.png`;
export const quoteIcon = `${apiUrl}/images/quote_icon.png`;
export const quoteAuthor = `${apiUrl}/images/quote_author.png`;
export const signupSuccessRightImg = `${apiUrl}/images/signup_success_img.png`;
