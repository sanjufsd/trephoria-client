import styled from 'styled-components';

export const UserActivities = styled.div`
  margin-top: 30px;
`;

export const ConcertsWrapper = styled.div``;

export const ConcertHead = styled.div`
  margin: 0 0 13px;
  > button {
    float: right;
  }
`;

export const ConcertTitle = styled.h2`
  font-size: 18px;
  font-weight: bold;
  color: #000000;
  display: inline-block;
  margin: 0;
`;

export const Concerts = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: calc(100% + 15px);
`;

export const Concert = styled.div`
  width: calc(33.3333% - 15px);
  margin: 0 15px 15px 0;
`;
