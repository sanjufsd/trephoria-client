/* eslint-disable global-require */
import { apiUrl } from '../../../config/apikey';
export const concertLists = [
  {
    date: 30,
    month: 'Aug',
    title: 'Bumbershoot 2019',
    image: `${apiUrl}/images/concert_01.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 14,
    month: 'Sep',
    title: 'Drake (21 + Event)',
    image: `${apiUrl}/images/concert_02.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 22,
    month: 'Sep',
    title: `Hot 97's On Da Reggae Soca Tip`,
    image: `${apiUrl}/images/concert_03.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 30,
    month: 'Aug',
    title: 'Bumbershoot 2019',
    image: `${apiUrl}/images/concert_01.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 14,
    month: 'Sep',
    title: 'Drake (21 + Event)',
    image: `${apiUrl}/images/concert_02.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 22,
    month: 'Sep',
    title: `Hot 97's On Da Reggae Soca Tip`,
    image: `${apiUrl}/images/concert_03.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
];
