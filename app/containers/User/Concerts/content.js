import React from 'react';
import * as S from './style';
import * as c from './constants';
import ErrorBoundary from '../../../components/ErrorBoundary';
import ConcertItem from '../../../components/ConcertItem';
import Filter from '../../../components/Filter';

const Concerts = () => (
  <S.ConcertsWrapper>
    <S.ConcertHead>
      <S.ConcertTitle>Concerts</S.ConcertTitle>
      <ErrorBoundary>
        <Filter />
      </ErrorBoundary>
    </S.ConcertHead>
    <S.Concerts>
      {c.concertLists.map((item, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <S.Concert key={index}>
          <ErrorBoundary>
            <ConcertItem item={item} />
          </ErrorBoundary>
        </S.Concert>
      ))}
    </S.Concerts>
  </S.ConcertsWrapper>
);

export default Concerts;
