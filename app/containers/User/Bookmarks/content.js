import React, { useState } from 'react';
import * as S from './style';
import * as c from './constants';
import ErrorBoundary from '../../../components/ErrorBoundary';
import ConcertItem from '../../../components/ConcertItem';
import ArtistItem from '../../../components/ArtistItem';

const Content = () => {
  const [active, setActive] = useState(1);
  return (
    <S.BookmarkWrapper>
      <S.Title>My Bookmarks</S.Title>
      <S.Tabs>
        {c.tabs.map(item => (
          <S.Tab
            key={item.id}
            active={active === item.id}
            onClick={() => {
              setActive(item.id);
            }}
          >
            {item.title}
          </S.Tab>
        ))}
      </S.Tabs>
      {active === 1 ? (
        <S.Concerts>
          {c.concertLists.map((item, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <S.Concert key={index}>
              <ErrorBoundary>
                <ConcertItem item={item} bookmarked />
              </ErrorBoundary>
            </S.Concert>
          ))}
        </S.Concerts>
      ) : (
        <S.Artists>
          {c.artistLists.map(item => (
            <S.Artist key={item.id}>
              <ErrorBoundary>
                <ArtistItem item={item} bookmarked />
              </ErrorBoundary>
            </S.Artist>
          ))}
        </S.Artists>
      )}
    </S.BookmarkWrapper>
  );
};

export default Content;
