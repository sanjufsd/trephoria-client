import styled, { css } from 'styled-components';

export const BookmarkWrapper = styled.div``;

export const Title = styled.h2`
  font-size: 18px;
  font-weight: bold;
  color: #000000;
  margin: 0 0 13px;
`;

export const Tabs = styled.ul`
  list-style: none;
  display: block;
  padding: 0;
  margin: 0 0 15px;
`;

export const Tab = styled.li`
  font-size: 10px;
  color: #686868;
  display: inline-block;
  opacity: 0.5;
  border-radius: 20px;
  border: solid 1px #686868;
  padding: 3px 15px;
  margin: 0 10px 0 0;
  cursor: pointer;
  ${props =>
    props.active &&
    css`
      border-color: #983fcc;
      color: #983fcc;
      opacity: 1;
    `}
`;

export const Concerts = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: calc(100% + 15px);
`;

export const Concert = styled.div`
  width: calc(33.3333% - 15px);
  margin: 0 15px 15px 0;
`;

export const Artists = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: calc(100% + 30px);
`;

export const Artist = styled.div`
  width: calc(20% - 30px);
  margin: 0 30px 5px 0;
  h5 {
    font-size: 12px;
  }
  a {
    font-size: 9px;
  }
`;
