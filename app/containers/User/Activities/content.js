import React from 'react';
import Activities from '../../../components/Activities';
import ErrorBoundary from '../../../components/ErrorBoundary';

const Content = () => (
  <ErrorBoundary>
    <Activities filterTab />
  </ErrorBoundary>
);

export default Content;
