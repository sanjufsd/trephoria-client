import React from 'react';
import { connect } from 'react-redux';
import UserWrapper from '../../../components/UserWrapper';

const UserArtists = props => <UserWrapper {...props} />;

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(UserArtists);
