import React from 'react';
import * as S from './style';
import * as c from './constants';
import ErrorBoundary from '../../../components/ErrorBoundary';
import ArtistItem from '../../../components/ArtistItem';

const Content = () => (
  <S.ArtistWrapper>
    <S.ArtistTitle>Artist List</S.ArtistTitle>
    <S.Artists>
      {c.artistLists.map(item => (
        <S.Artist key={item.id}>
          <ErrorBoundary>
            <ArtistItem item={item} />
          </ErrorBoundary>
        </S.Artist>
      ))}
    </S.Artists>
  </S.ArtistWrapper>
);

export default Content;
