import styled from 'styled-components';

export const ArtistWrapper = styled.div``;

export const ArtistTitle = styled.h2`
  font-size: 18px;
  font-weight: bold;
  color: #000000;
  margin: 0 0 13px;
`;

export const Artists = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: calc(100% + 30px);
`;

export const Artist = styled.div`
  width: calc(20% - 30px);
  margin: 0 30px 5px 0;
  h5 {
    font-size: 12px;
  }
  a {
    font-size: 9px;
  }
`;
