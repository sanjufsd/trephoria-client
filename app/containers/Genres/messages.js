/*
 * PopularMusicGenres Messages
 *
 * This contains all the text for the PopularMusicGenres container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.PopularMusicGenres';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PopularMusicGenres container!',
  },
});
