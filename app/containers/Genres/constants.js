/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const filterIcon = `${apiUrl}/images/filter_icon.png`;
export const popularMusicGenreLists = [
  {
    name: 'JAZZ MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_01.png`,
  },
  {
    name: 'HIP HOP MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_02.png`,
  },
  {
    name: 'HEAVY METAL',
    image: `${apiUrl}/images/popularMusicGenres_03.png`,
  },
  {
    name: 'FUNK MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_04.png`,
  },
  {
    name: 'CLASSICAL MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_05.png`,
  },
  {
    name: 'musical theater',
    image: `${apiUrl}/images/popularMusicGenres_06.png`,
  },
  {
    name: 'reggae',
    image: `${apiUrl}/images/popularMusicGenres_07.png`,
  },
  {
    name: 'Rock MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_08.png`,
  },
  {
    name: 'POP MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_09.png`,
  },
  {
    name: 'FOLK MUSIC',
    image: `${apiUrl}/images/popularMusicGenres_10.png`,
  },
  {
    name: 'Lorem Ipsum',
    image: `${apiUrl}/images/popularMusicGenres_11.png`,
  },
  {
    name: 'Lorem Ipsum',
    image: `${apiUrl}/images/popularMusicGenres_12.png`,
  },
  {
    name: 'Lorem Ipsum',
    image: `${apiUrl}/images/popularMusicGenres_13.png`,
  },
  {
    name: 'Lorem Ipsum',
    image: `${apiUrl}/images/popularMusicGenres_14.png`,
  },
  {
    name: 'Lorem Ipsum',
    image: `${apiUrl}/images/popularMusicGenres_15.png`,
  },
];

export const popularMusicLeftBg = `${apiUrl}/images/popular_music_genre_left_bg.png`;
export const popularMusicMiddleBg = `${apiUrl}/images/popular_music_genre_middle_bg.png`;
export const popularMusicRightBg = `${apiUrl}/images/popular_music_genre_right_bg.png`;
