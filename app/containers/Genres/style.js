import styled from 'styled-components';

export const PopularMusicContainer = styled.div`
  background-image: linear-gradient(to bottom, #231f24, #000000);
  padding-top: 90px;
  min-height: calc(100vh - 97px);
  > .container {
    position: relative;
    z-index: 2;
  }
`;

export const Breadcrumb = styled.div`
  opacity: 0.5;
  font-size: 12px;
  color: #ffffff;
  margin: 0 0 25px;
  a {
    font-size: 12px;
    color: #ffffff !important;
    text-decoration: none;
  }
`;

export const HeadingWrapper = styled.div`
  position: relative;
  z-index: 3;
  form {
    margin-top: 0;
  }
  .col-lg-3 {
    > button {
      display: block;
      margin: 10px 0 0 auto;
    }
  }
`;

export const Heading = styled.h2`
  font-size: 26px;
  font-weight: bold;
  line-height: inherit;
  color: #ffffff;
  margin: 8px 0 0;
`;

export const GenreGroup = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 15px;
  position: relative;
  z-index: 2;
`;

export const GenreItemContainer = styled.div`
  width: 20%;
  padding: 7px;
`;

export const PopularMusicLeftBg = styled.img`
  position: absolute;
  left: 0;
  top: 100px;
  width: 170px;
`;

export const PopularMusicMiddleBg = styled.img`
  position: absolute;
  top: 0;
  left: -50px;
  width: calc(100% + 100px);
  z-index: 1;
  pointer-events: none;
`;

export const PopularMusicRightBg = styled.img`
  position: absolute;
  right: 0;
  top: 300px;
  width: 170px;
`;

export const GenreErrorMsg = styled.div`
  color: white;
  margin: 100px 0;
  font-size: 15px;
  text-align: center;
`;
