/* eslint-disable no-unused-vars */
import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import axios from 'axios';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import Search from '../../components/Search';
import GenreMusicItem from '../../components/GenreMusicItem';
import ErrorBoundary from '../../components/ErrorBoundary';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Filter from '../../components/Filter';
import { genreUrl } from '../../config/apikey';
import { Loader } from '../../components/Loader';

const PopularMusicGenres = props => {
  const [genreDatas, setGenredata] = useState([]);
  const [error, setError] = useState('');
  const constantDataSize = c.popularMusicGenreLists.length;
  useEffect(() => {
    axios
      .get(genreUrl)
      .then(res => {
        if (res.data.length > 0) {
          setGenredata(res.data);
        } else {
          setError('No data available');
        }
      })
      .catch(err => {
        setError('No data available');
      });
  }, []);
  return (
    <Fragment>
      <ErrorBoundary>
        <Header {...props} />
      </ErrorBoundary>
      <S.PopularMusicContainer>
        <Container>
          <S.Breadcrumb>
            <Link to={u.homepage}>Home</Link> / All Genre Page
          </S.Breadcrumb>
          <S.HeadingWrapper>
            <Row>
              <Col lg="4">
                <S.Heading>Popular Music Genres</S.Heading>
              </Col>
              <Col lg="5">
                <ErrorBoundary>
                  <Search />
                </ErrorBoundary>
              </Col>
              <Col lg="3">
                <ErrorBoundary>
                  <Filter colorType="white" />
                </ErrorBoundary>
              </Col>
            </Row>
          </S.HeadingWrapper>
          {!error ? (
            <S.GenreGroup>
              {genreDatas.length > 0 ? (
                genreDatas.map((item, index) => {
                  const number =
                    index < constantDataSize ? index : index % constantDataSize;
                  return (
                    <S.GenreItemContainer key={item.id}>
                      <ErrorBoundary>
                        <GenreMusicItem
                          item={item}
                          _image={c.popularMusicGenreLists[number].image}
                        />
                      </ErrorBoundary>
                    </S.GenreItemContainer>
                  );
                })
              ) : (
                <ErrorBoundary>
                  <Loader
                    bgColor="transparent"
                    color="white"
                    padding="100px 0"
                  />
                </ErrorBoundary>
              )}
            </S.GenreGroup>
          ) : (
            <S.GenreErrorMsg>{error}</S.GenreErrorMsg>
          )}
          <S.PopularMusicMiddleBg src={c.popularMusicMiddleBg} alr="" />
        </Container>
        <S.PopularMusicLeftBg src={c.popularMusicLeftBg} alt="" />
        <S.PopularMusicRightBg src={c.popularMusicRightBg} alt="" />
      </S.PopularMusicContainer>
      <ErrorBoundary>
        <Footer />
      </ErrorBoundary>
    </Fragment>
  );
};

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(PopularMusicGenres);
