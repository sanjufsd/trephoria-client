/**
 *
 * Asynchronously loads the component for PopularMusicGenres
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
