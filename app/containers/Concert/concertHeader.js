import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import * as S from './style';
import * as G from '../../global-styles';
import * as c from './constants';
import * as u from '../../config/url';

const ConcertHeader = () => (
  <S.Header bgImage={c.concertHeader}>
    <Container>
      <S.Breadcrumb>
        <Link to={u.homepage}>Home</Link> /{' '}
        <Link to={u.allGenre}>All Genre Page</Link> /{' '}
        <Link to={`${u.singleGenre}/jazz`}>Jazz Music</Link> /{' '}
        <Link to={`${u.singleArtist}/metallica`}>Metallica</Link> / Event Page
      </S.Breadcrumb>
      <S.HeaderWrapper>
        <Row>
          <Col lg="6">
            <S.HeaderLeftWrapper>
              <S.HeaderLeftTime>
                <S.HeaderLeftDate>06</S.HeaderLeftDate>
                <S.HeaderLeftMonth>Sep</S.HeaderLeftMonth>
              </S.HeaderLeftTime>
              <S.HeaderLeftTitleWrapper>
                <G.Div>
                  <S.HeaderLeftTitle>
                    Metallica: World Tour 2019 San Francisco
                  </S.HeaderLeftTitle>
                  <S.HeaderLeftSubTitle>Chase Center</S.HeaderLeftSubTitle>
                </G.Div>
              </S.HeaderLeftTitleWrapper>
            </S.HeaderLeftWrapper>
          </Col>
          <Col lg="6">
            <S.HeaderRightWrapper>
              <S.HeaderRightTime>
                <S.HeaderRightTimeTitle>Date Of Event</S.HeaderRightTimeTitle>
                <S.HeaderRightFullDate>
                  Friday 06 September 2019
                </S.HeaderRightFullDate>
                <S.HeaderRightTiming>Time 16:30</S.HeaderRightTiming>
              </S.HeaderRightTime>
              <S.AddToCalender>
                <S.AddToCalenderButton>
                  <S.AddToCalenderIcon src={c.calenderPlus} />
                  Add To Calender
                </S.AddToCalenderButton>
              </S.AddToCalender>
            </S.HeaderRightWrapper>
          </Col>
        </Row>
      </S.HeaderWrapper>
    </Container>
  </S.Header>
);

export default ConcertHeader;
