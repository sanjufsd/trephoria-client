import React, { useState } from 'react';
import * as S from './style';
import * as c from './constants';
import TicketAvailabilityList from './ticketAvailabilityList';
import TicketAvailabilityStadium from './ticketAvailabilityStadium';
import ErrorBoundary from '../../components/ErrorBoundary';

const TicketAvailability = () => {
  const [active, setActive] = useState('list');
  return (
    <S.TicketAvailability>
      <S.TicketAvailHeader>
        <S.TicketAvailImageWrapper>
          <S.TicketAvailImage src={c.ticketProviders[0].image} />
        </S.TicketAvailImageWrapper>
        <S.TicketAvailTitle>
          <S.TicketAvailText>www.vividseats.com</S.TicketAvailText>
          <S.TicketAvailStatus>Available</S.TicketAvailStatus>
        </S.TicketAvailTitle>
        <S.TicketAvailTabs>
          {c.tabs.map(item => (
            <S.TicketAvailTab
              key={item.type}
              active={active}
              type={item.type}
              onClick={() => {
                setActive(item.type);
              }}
            >
              <S.TicketAvailTabIcon src={item.image} />
            </S.TicketAvailTab>
          ))}
        </S.TicketAvailTabs>
      </S.TicketAvailHeader>
      {active === 'list' ? (
        <ErrorBoundary>
          <TicketAvailabilityList tickets={c.tickets} />
        </ErrorBoundary>
      ) : (
        <ErrorBoundary>
          <TicketAvailabilityStadium tickets={c.tickets} />
        </ErrorBoundary>
      )}
    </S.TicketAvailability>
  );
};

export default TicketAvailability;
