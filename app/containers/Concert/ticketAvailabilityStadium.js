import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import * as G from '../../global-styles';

const TicketAvailabilityStadium = props => {
  const { tickets } = props;
  return (
    <S.TicketAvailabilityStadium>
      <S.StadiumLists>
        <G.Table>
          <G.THead>
            <G.TRow>
              <G.THeading>Ticket Details</G.THeading>
            </G.TRow>
          </G.THead>
          <G.TBody>
            {tickets.map((item, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <G.TRow key={index}>
                <G.TData>
                  <S.TicketDetails>
                    <S.TicketDetailsTitle>
                      {item.title}
                      <S.TicketInfoLogo src={c.infoIcon} />
                    </S.TicketDetailsTitle>
                    Row {item.row}
                  </S.TicketDetails>
                </G.TData>
              </G.TRow>
            ))}
          </G.TBody>
        </G.Table>
      </S.StadiumLists>
      <S.StadiumImageWrapper>
        <S.StadiumImage src={c.stadium} />
      </S.StadiumImageWrapper>
    </S.TicketAvailabilityStadium>
  );
};

TicketAvailabilityStadium.propTypes = {
  tickets: PropTypes.array,
};

export default TicketAvailabilityStadium;
