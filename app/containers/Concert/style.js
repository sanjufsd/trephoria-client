import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 80px 0 0 0;
  min-height: calc(100vh - 97px);
`;

export const Header = styled.div`
  background-image: url(${props => props.bgImage});
  height: 160px;
  background-position: center;
  background-size: cover;
  margin-bottom: 30px;
  position: relative;
  overflow: hidden;
  > .container {
    height: 100%;
    position: relative;
  }
  &:before {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background-image: linear-gradient(168deg, #ff0813bf -72%, #071723d9 116%);
  }
`;

export const Breadcrumb = styled.div`
  font-size: 12px;
  color: #ffffff;
  text-transform: capitalize;
  padding: 15px 0 0;
  a {
    color: white !important;
    text-decoration: none !important;
  }
`;

export const HeaderWrapper = styled.div`
  margin-top: 10px;
  border-radius: 4px;
  box-shadow: 0 2px 21px 12px rgba(0, 0, 0, 0.09);
  border: solid 4px #ab3e9e;
  background-color: #ffffff;
  height: 100px;
`;

export const HeaderLeftWrapper = styled.div`
  position: relative;
  padding: 0 0 0 70px;
  height: 92px;
  overflow: hidden;
`;

export const HeaderLeftTime = styled.div`
  position: absolute;
  left: 17px;
  top: 50%;
  transform: translate(0, -50%);
  text-align: center;
  color: #f25d9d;
`;

export const HeaderLeftDate = styled.div`
  font-size: 27px;
  font-weight: bold;
`;

export const HeaderLeftMonth = styled.div`
  font-size: 17px;
  margin: -13px 0 0;
`;

export const HeaderLeftTitleWrapper = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  max-width: 300px;
`;

export const HeaderLeftTitle = styled.h1`
  font-size: 22px;
  margin: 0 0 3px;
  font-weight: bold;
  line-height: 25px;
`;

export const HeaderLeftSubTitle = styled.h2`
  font-size: 14px;
  color: #797979;
  margin: 0;
`;

export const HeaderRightWrapper = styled.div`
  height: 92px;
  display: flex;
  align-items: center;
`;

export const HeaderRightTime = styled.div`
  flex: 2;
  border-left: 2px solid #dcdcdc;
  padding: 0 0 0 30px;
`;

export const HeaderRightTimeTitle = styled.div`
  font-size: 14px;
  color: #797979;
  opacity: 0.7;
`;

export const HeaderRightFullDate = styled.div`
  font-size: 16px;
  font-weight: bold;
`;

export const HeaderRightTiming = styled.div`
  font-size: 14px;
`;

export const AddToCalender = styled.div`
  flex: 1;
`;

export const AddToCalenderButton = styled.button`
  font-size: 12px;
  border-radius: 20px;
  color: #983fcc;
  border: 1px solid #983fcc;
  background: transparent;
  outline: 0 !important;
  padding: 8px 17px 9px;
`;

export const AddToCalenderIcon = styled.img`
  vertical-align: bottom;
  margin: 0 10px 0 0;
`;

export const AboutConcert = styled.div``;

export const AboutTitle = styled.h3`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 15px;
`;

export const AboutWrapper = styled.div`
  border-radius: 8px;
  border: solid 1px #e1e1e1;
  overflow: hidden;
  margin-bottom: 20px;
`;

export const AboutMap = styled.div`
  border-bottom: solid 1px #e1e1e1;
`;

export const AboutMapIframe = styled.iframe``;

export const AboutContent = styled.div`
  padding: 15px;
  font-size: 12px;
  line-height: 22px;
`;

export const AboutVenueTitle = styled.div`
  color: #797979;
`;

export const AboutVenueCenter = styled.div`
  font-weight: bold;
  color: #4e4e4e;
`;

export const AboutVenueLocation = styled.div`
  color: #797979;
`;

export const AboutPhone = styled.div`
  color: #797979;
  margin: 15px 0 0;
`;

export const AboutWebsiteLink = styled.a`
  color: #797979 !important;
  text-decoration: none !important;
`;

export const AboutCapacity = styled.div`
  margin: 15px 0 0;
  color: #797979;
  border-bottom: solid 1px #e1e1e1;
  padding: 0 0 10px;
`;

export const AboutCapacityTitle = styled.b`
  color: #4e4e4e;
`;

export const AboutUpcomingEvents = styled.div`
  border-radius: 20px;
  border: 1px solid black;
  margin: 15px 0;
  text-align: center;
  padding: 5px 0;
`;

export const AboutUpcomingIcon = styled.img`
  width: 18px;
  vertical-align: sub;
  margin: 0 12px 0 0;
`;

export const AboutUpcomingCount = styled.span`
  font-size: 18px;
  font-weight: bold;
`;

export const TicketProviderWrapper = styled.div``;

export const TicketTitle = styled.h3`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 15px;
`;

export const TicketWrapper = styled.div`
  border: solid 2px #e9e9e9;
  padding: 30px 40px;
`;

export const TicketHead = styled.div`
  display: inline-block;
  width: 100%;
  font-size: 18px;
  .backButton {
    vertical-align: middle;
    width: 30px;
    margin: 0 20px 0 0;
    cursor: pointer;
  }
`;

export const TicketIcon = styled.img`
  margin: 0 10px 0 0;
  vertical-align: bottom;
  width: 40px;
`;

export const TicketStep = styled.div`
  float: right;
  font-size: 14px;
  color: #929699;
`;

export const TicketProviders = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -15px;
`;

export const TicketProvider = styled.div`
  width: 25%;
  margin-top: 30px;
  padding: 0 15px;
`;

export const TicketNote = styled.div`
  opacity: 0.3;
  font-size: 12px;
  color: #090909;
  margin: 10px 0 30px;
`;

export const CommunityPage = styled.div`
  margin-bottom: 30px;
`;

export const CommunityTitle = styled.h3`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 15px;
`;

export const CommunityWrapper = styled.div`
  border: solid #eee;
  border-width: ${props => (props.bg ? '0 0 2px 0' : '2px')};
  background-image: url(${props => props.bg});
  text-align: center;
  padding: 60px 0;
  background-size: cover;
`;

export const CommunityIcon = styled.img``;

export const CommunityNote = styled.div`
  font-size: 14px;
  color: #090909;
  margin: 40px 0 20px;
`;

export const CommunityButton = styled.button`
  border-radius: 20px;
  border: 0 !important;
  background-color: #983fcc;
  outline: 0 !important;
  font-size: 14px;
  color: white;
  padding: 8px 45px;
`;

export const TicketAvailability = styled.div``;

export const TicketAvailHeader = styled.div`
  padding: 0 0 0 50px;
  margin: 10px 0 0;
  > div {
    display: inline-block;
    vertical-align: middle;
  }
`;

export const TicketAvailImageWrapper = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 2px;
  box-shadow: 0 2px 25px 6px rgba(0, 0, 0, 0.05);
  border: solid 1px #130aba;
  margin: 0 18px 0 0;
  padding: 10px;
`;

export const TicketAvailImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
  object-position: center;
`;

export const TicketAvailTitle = styled.div`
  display: inline-block;
  padding: 6px 0;
  vertical-align: middle;
`;

export const TicketAvailText = styled.div`
  font-size: 16px;
  font-weight: bold;
`;

export const TicketAvailStatus = styled.div`
  display: inline-block;
  font-size: 10px;
  color: #7ed321;
  border: 1px solid #7ed321;
  border-radius: 10px;
  padding: 1px 10px;
`;

export const TicketAvailTabs = styled.div`
  float: right;
`;

export const TicketAvailTab = styled.button`
  background: transparent;
  border: 0;
  outline: 0 !important;
  padding: 15px 10px;
  opacity: ${props => (props.type === props.active ? 1 : 0.2)};
`;

export const TicketAvailTabIcon = styled.img``;

export const TicketAvailabilityList = styled.div`
  margin: 15px -40px -30px;
  table {
    width: 100%;
    thead {
      background: #130aba;
      th {
        color: white;
        font-size: 12px;
        font-weight: normal;
        padding: 10px 0;
        &:first-child {
          padding-left: 40px;
        }
        &:last-child {
          padding-right: 40px;
        }
      }
    }
    tbody {
      tr {
        border-bottom: solid 1px #e9e9e9;
        &:last-child {
          border: 0;
        }
      }
      td {
        font-size: 12px;
        padding: 10px 0;
        &:first-child {
          padding-left: 40px;
        }
        &:last-child {
          padding-right: 40px;
          width: 92px;
        }
      }
    }
  }
`;

export const TicketDetails = styled.div`
  font-size: 10px;
  color: #949494;
`;

export const TicketDetailsTitle = styled.p`
  font-size: 12px;
  color: black;
  font-weight: bold;
  margin: 0 0 2px;
`;

export const TicketInfoLogo = styled.img`
  margin: 0 0 0 10px;
`;

export const TicketQuantity = styled.p`
  margin: 0;
  font-size: 12px;
  color: #595959;
`;

export const TicketPriceWrapper = styled.div`
  font-size: 10px;
  color: #949494;
`;

export const TicketPrice = styled.p`
  margin: 0 0 -5px;
  color: #090909;
  font-size: 17px;
  font-weight: bold;
`;

export const TicketStatus = styled.p`
  font-size: 12px;
  color: ${props => props.color};
  margin: 0;
`;

export const TicketBuyButton = styled.button`
  background: #983fcc;
  color: white;
  border-radius: 20px;
  font-size: 12px;
  border: 0;
  outline: 0 !important;
  padding: 3px 15px;
`;

export const TicketAvailabilityStadium = styled.div`
  display: flex;
  width: calc(100% + 80px);
  margin: 15px -40px -30px;
`;

export const StadiumLists = styled.div`
  width: 40%;
  table {
    width: 100%;
    thead {
      background: #130aba;
      th {
        color: white;
        font-size: 12px;
        font-weight: normal;
        padding: 10px 0 10px 40px;
      }
    }
    tbody {
      tr {
        border-bottom: solid 1px #e9e9e9;
        &:last-child {
          border: 0;
        }
      }
      td {
        font-size: 12px;
        padding: 10px 0 10px 40px;
      }
    }
  }
`;

export const StadiumImageWrapper = styled.div`
  width: 60%;
  background: #f7f5f6;
`;

export const StadiumImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
`;

export const AuthenticateTicket = styled.div`
  border: solid 2px #e9e9e9;
  padding: 40px;
`;

export const AuthenticateTicketImgWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
`;

export const AuthenticateTicketImg = styled.img`
  width: 100%;
`;
