/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const images = [
  { image: `${apiUrl}/images/artist/1.jpg` },
  { image: `${apiUrl}/images/artist/2.jpg` },
  { image: `${apiUrl}/images/artist/3.jpg` },
  { image: `${apiUrl}/images/artist/11.jpg` },
  { image: `${apiUrl}/images/artist/13.jpg` },
  { image: `${apiUrl}/images/artist/14.jpg` },
  { image: `${apiUrl}/images/artist/15.jpg` },
  { image: `${apiUrl}/images/artist/16.jpg` },
  { image: `${apiUrl}/images/artist/17.jpg` },
];
export const concertHeader = `${apiUrl}/images/concert_header.png`;
export const calenderPlus = `${apiUrl}/images/calender.svg`;
export const eventIcon = `${apiUrl}/images/concert_active_icon.png`;
export const ticketIcon = `${apiUrl}/images/ticket_icon.svg`;
export const ticketProviders = [
  {
    image: `${apiUrl}/images/ticket_providers/1.jpg`,
    status: 'available',
  },
  {
    image: `${apiUrl}/images/ticket_providers/2.jpg`,
    status: 'filling_fast',
  },
  {
    image: `${apiUrl}/images/ticket_providers/3.jpg`,
    status: 'available',
  },
  {
    image: `${apiUrl}/images/ticket_providers/4.jpg`,
    status: 'soldout',
  },
  {
    image: `${apiUrl}/images/ticket_providers/5.jpg`,
    status: 'filling_fast',
  },
  {
    image: `${apiUrl}/images/ticket_providers/6.jpg`,
    status: 'available',
  },
];
export const communityNoAccess = `${apiUrl}/images/community_no_access.svg`;
export const communityNoAccessBg = `${apiUrl}/images/community_no_access_bg.jpg`;
export const backArrow = `${apiUrl}/images/genre_back_arrow.png`;
export const tabs = [
  { type: 'list', image: `${apiUrl}/images/ticket_tab_list.svg` },
  { type: 'stadium', image: `${apiUrl}/images/ticket_tab_stadium.svg` },
];
export const tickets = [
  {
    title: 'Terrace Level 310 - Side View',
    row: 'K',
    quantity: '1-10 Tickets',
    price: '$260.00',
    status: 'available',
  },
  {
    title: 'STANDING Gazon B',
    row: 'P',
    quantity: '1-5 Tickets',
    price: '$500.00',
    status: 'filling_fast',
  },
  {
    title: 'Dream No More Experience Gazon A',
    row: 'L',
    quantity: '10-15 Tickets',
    price: '$200.00',
    status: 'soldout',
  },
  {
    title: 'Shortest Straw Experience SEATED',
    row: 'M',
    quantity: '1-7 Tickets',
    price: '$350.00',
    status: 'filling_fast',
  },
  {
    title: 'Dream No More Experience Gazon D',
    row: 'N',
    quantity: '1-5 Tickets',
    price: '$200.00',
    status: 'soldout',
  },
  {
    title: 'Terrace Level 310 - Side View',
    row: 'K',
    quantity: '1-10 Tickets',
    price: '$260.00',
    status: 'available',
  },
];
export const infoIcon = `${apiUrl}/images/information.svg`;
export const stadium = `${apiUrl}/images/stadium.jpg`;

export const dragDropRightImg = require('../../images/drag-drop-right-img.svg');
export const reportIcon = require('../../images/report-icon.svg');
export const uploadSuccessImg = require('../../images/upload-success-img.svg');
