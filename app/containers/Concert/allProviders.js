import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import TicketProvider from '../../components/TicketProvider';
import ErrorBoundary from '../../components/ErrorBoundary';

const AllProviders = props => {
  const { setStep, providers } = props;
  return (
    <S.TicketProviders>
      {providers.map((item, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <S.TicketProvider key={index}>
          <ErrorBoundary>
            <TicketProvider item={item} setStep={setStep} />
          </ErrorBoundary>
        </S.TicketProvider>
      ))}
    </S.TicketProviders>
  );
};

AllProviders.propTypes = {
  setStep: PropTypes.func,
  providers: PropTypes.array,
};

export default AllProviders;
