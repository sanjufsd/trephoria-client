/**
 *
 * Asynchronously loads the component for Concert
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
