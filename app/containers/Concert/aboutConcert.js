import React from 'react';
import * as S from './style';
import * as c from './constants';
import SocialLinks from '../../components/SocialLinks';
import ErrorBoundary from '../../components/ErrorBoundary';

const AboutConcert = () => (
  <S.AboutConcert>
    <S.AboutTitle>About Concert</S.AboutTitle>
    <S.AboutWrapper>
      <S.AboutMap>
        <S.AboutMapIframe
          title="Concert Location"
          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d972.2042654418485!2d77.60123669940566!3d12.919476497646988!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc53e99dcefc5659b!2sMSys%20Technologies!5e0!3m2!1sen!2sin!4v1576744971550!5m2!1sen!2sin"
          width="100%"
          height="150"
          frameBorder="0"
          style={{ border: 0 }}
          allowFullScreen
        />
      </S.AboutMap>
      <S.AboutContent>
        <S.AboutVenueTitle>Venue</S.AboutVenueTitle>
        <S.AboutVenueCenter>Chase Center</S.AboutVenueCenter>
        <S.AboutVenueLocation>
          South St, San Francisco, CA 94158, USA
        </S.AboutVenueLocation>
        <S.AboutPhone>+1 888-479-4667</S.AboutPhone>
        <S.AboutWebsiteLink
          href="http://www.nationalarenabucuresti.ro"
          target="_blank"
        >
          www.nationalarenabucuresti.ro
        </S.AboutWebsiteLink>
        <S.AboutCapacity>
          Capacity: <S.AboutCapacityTitle>18,064</S.AboutCapacityTitle>
        </S.AboutCapacity>
        <S.AboutUpcomingEvents>
          <S.AboutUpcomingIcon src={c.eventIcon} />
          <S.AboutUpcomingCount>2</S.AboutUpcomingCount> Upcoming Events
        </S.AboutUpcomingEvents>
        <ErrorBoundary>
          <SocialLinks />
        </ErrorBoundary>
      </S.AboutContent>
    </S.AboutWrapper>
  </S.AboutConcert>
);

export default AboutConcert;
