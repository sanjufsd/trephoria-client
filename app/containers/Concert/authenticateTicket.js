import React from 'react';
import { Row, Col } from 'reactstrap';
import * as S from './style';
import * as G from '../../global-styles';
import * as c from './constants';
import DragAndDrop from '../../components/DragAndDrop';

const AuthenticateTicket = () => (
  <S.AuthenticateTicket>
    <Row>
      <Col lg="6">
        <DragAndDrop />
      </Col>
      <Col lg="6">
        <S.AuthenticateTicketImgWrapper>
          <G.Div>
            <S.AuthenticateTicketImg src={c.dragDropRightImg} />
          </G.Div>
        </S.AuthenticateTicketImgWrapper>
      </Col>
    </Row>
  </S.AuthenticateTicket>
);

AuthenticateTicket.propTypes = {};

export default AuthenticateTicket;
