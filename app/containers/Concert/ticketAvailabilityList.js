import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import * as G from '../../global-styles';

const TicketAvailabilityList = props => {
  const { tickets } = props;
  return (
    <S.TicketAvailabilityList>
      <G.Table>
        <G.THead>
          <G.TRow>
            <G.THeading>Ticket Details</G.THeading>
            <G.THeading>Ticket Quantity</G.THeading>
            <G.THeading>Price</G.THeading>
            <G.THeading>Availability</G.THeading>
            <G.THeading />
          </G.TRow>
        </G.THead>
        <G.TBody>
          {tickets.map((item, index) => {
            let text = '- Sold Out -';
            let color = '#d0021b';
            if (item.status === 'available') {
              text = 'Available';
              color = '#7ed321';
            } else if (item.status === 'filling_fast') {
              text = 'Filling Fast';
              color = '#f5a623';
            }
            return (
              // eslint-disable-next-line react/no-array-index-key
              <G.TRow key={index}>
                <G.TData>
                  <S.TicketDetails>
                    <S.TicketDetailsTitle>
                      {item.title}
                      <S.TicketInfoLogo src={c.infoIcon} />
                    </S.TicketDetailsTitle>
                    Row {item.row}
                  </S.TicketDetails>
                </G.TData>
                <G.TData>
                  <S.TicketQuantity>{item.quantity}</S.TicketQuantity>
                </G.TData>
                <G.TData>
                  <S.TicketPriceWrapper>
                    <S.TicketPrice>{item.price}</S.TicketPrice> each
                  </S.TicketPriceWrapper>
                </G.TData>
                <G.TData>
                  <S.TicketStatus color={color}>{text}</S.TicketStatus>
                </G.TData>
                <G.TData>
                  <S.TicketBuyButton>Buy</S.TicketBuyButton>
                </G.TData>
              </G.TRow>
            );
          })}
        </G.TBody>
      </G.Table>
    </S.TicketAvailabilityList>
  );
};

TicketAvailabilityList.propTypes = {
  tickets: PropTypes.array,
};

export default TicketAvailabilityList;
