import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import AllProviders from './allProviders';
import TicketAvailability from './ticketAvailability';
import ErrorBoundary from '../../components/ErrorBoundary';

const TicketProviders = props => {
  const { step, setStep } = props;
  return (
    <Fragment>
      <S.TicketProviderWrapper>
        <S.TicketTitle>Ticket Providers &amp; Availability</S.TicketTitle>
        <S.TicketWrapper>
          <S.TicketHead>
            {step === 1 ? (
              <Fragment>
                <S.TicketIcon src={c.ticketIcon} />
                Select Tickets ({c.ticketProviders.length})
              </Fragment>
            ) : (
              <Fragment>
                <S.TicketIcon
                  className="backButton"
                  src={c.backArrow}
                  onClick={() => {
                    setStep(1);
                  }}
                />
                View Ticket Availability
              </Fragment>
            )}
            <S.TicketStep>Step {step} of 2</S.TicketStep>
          </S.TicketHead>
          {step === 1 ? (
            <ErrorBoundary>
              <AllProviders providers={c.ticketProviders} setStep={setStep} />
            </ErrorBoundary>
          ) : (
            <ErrorBoundary>
              <TicketAvailability />
            </ErrorBoundary>
          )}
        </S.TicketWrapper>
      </S.TicketProviderWrapper>
      <S.TicketNote>
        * Please note that a maximum number of 20 tickets can be purchased for
        this event per order
      </S.TicketNote>
    </Fragment>
  );
};

TicketProviders.propTypes = {
  step: PropTypes.number,
  setStep: PropTypes.func,
};

export default TicketProviders;
