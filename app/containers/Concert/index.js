import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import * as S from './style';
import * as c from './constants';
import ErrorBoundary from '../../components/ErrorBoundary';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import ImagesCollage from '../../components/ImagesCollage';
import Search from '../../components/Search';
import ConcertHeader from './concertHeader';
import AboutConcert from './aboutConcert';
import TicketProviders from './ticketProviders';
import CommunityPage from './communityPage';

const Concert = props => {
  const [step, setStep] = useState(1);
  const { user, history } = props;
  return (
    <Fragment>
      <ErrorBoundary>
        <Header {...props} bg />
      </ErrorBoundary>
      <S.Wrapper>
        <ErrorBoundary>
          <Search full />
        </ErrorBoundary>
        <ErrorBoundary>
          <ConcertHeader {...props} />
        </ErrorBoundary>
        <Container>
          <Row>
            <Col lg="3">
              <ErrorBoundary>
                <AboutConcert />
              </ErrorBoundary>
              <ErrorBoundary>
                <ImagesCollage images={c.images} />
              </ErrorBoundary>
            </Col>
            <Col lg="9">
              <ErrorBoundary>
                <TicketProviders step={step} setStep={setStep} />
              </ErrorBoundary>
              <ErrorBoundary>
                <CommunityPage user={user} history={history} />
              </ErrorBoundary>
            </Col>
          </Row>
        </Container>
      </S.Wrapper>
      <ErrorBoundary>
        <Footer />
      </ErrorBoundary>
    </Fragment>
  );
};

Concert.propTypes = {
  user: PropTypes.object,
  history: PropTypes.object,
};

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(Concert);
