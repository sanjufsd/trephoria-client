import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import AuthenticateTicket from './authenticateTicket';

const CommunityPage = props => {
  const [activeTab, setTab] = useState('');
  const {
    user,
    history: { push },
  } = props;
  return (
    <S.CommunityPage>
      <S.CommunityTitle>Community Page</S.CommunityTitle>
      {activeTab === 'authenticate_ticket' ? (
        <AuthenticateTicket />
      ) : (
        <S.CommunityWrapper bg={c.communityNoAccessBg}>
          {user && user.userId ? (
            <Fragment>
              <S.CommunityIcon src={c.communityNoAccess} />
              <S.CommunityNote>
                To be part of fans community for this page, aunthenticate with
                ticket
              </S.CommunityNote>
              <S.CommunityButton
                onClick={() => {
                  setTab('authenticate_ticket');
                }}
              >
                Authenticate Ticket
              </S.CommunityButton>
            </Fragment>
          ) : (
            <Fragment>
              <S.CommunityIcon src={c.communityNoAccess} />
              <S.CommunityNote>
                Community are locked to members with authentic tickets attending
                the event
              </S.CommunityNote>
              <S.CommunityButton
                onClick={() => {
                  push(u.login);
                }}
              >
                Join Today
              </S.CommunityButton>
            </Fragment>
          )}
        </S.CommunityWrapper>
      )}
    </S.CommunityPage>
  );
};

CommunityPage.propTypes = {
  user: PropTypes.object,
  history: PropTypes.object,
};

export default CommunityPage;
