import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 80px 0 0 0;
  min-height: calc(100vh - 97px);
`;

export const Header = styled.div`
  background-image: url(${props => props.bgImage});
  height: 160px;
  background-position: center;
  background-size: cover;
  margin-bottom: 30px;
  > .container {
    height: 100%;
  }
`;

export const HeaderWrapper = styled.div`
  width: 100%;
  display: inline-block;
  height: 100%;
`;

export const HeaderLeftWrapper = styled.div`
  display: inline-block;
  width: calc(100% - 230px);
`;

export const HeaderRightWrapper = styled.div`
  float: right;
  width: 230px;
  height: 100%;
  > div {
    display: flex;
    align-items: center;
    height: 100%;
    padding-left: 60px;
    position: relative;
  }
`;

export const HeaderIcon = styled.img`
  position: absolute;
  left: 0;
  top: 50%;
  transform: translate(0, -50%);
`;

export const HeaderRightText = styled.p`
  color: white;
  margin: 0;
  font-size: 12px;
`;

export const Breadcrumb = styled.div`
  font-size: 12px;
  color: #ffffff;
  text-transform: capitalize;
  padding: 15px 0 0;
  a {
    color: white !important;
    text-decoration: none !important;
  }
`;

export const HeaderTitle = styled.h1`
  font-size: 50px;
  text-transform: uppercase;
  color: white;
  margin: 25px 0 0;
  font-weight: bold;
`;

export const HeaderSubTitle = styled.p`
  font-size: 12px;
  color: white;
  margin: 0;
  text-transform: capitalize;
`;

export const Biography = styled.div`
  margin-bottom: 20px;
`;

export const BioTitle = styled.h3`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 15px;
`;

export const BioWrapper = styled.div`
  border-radius: 8px;
  border: solid 1px #e1e1e1;
  padding: 20px;
`;

export const BioContent = styled.p`
  opacity: 0.7;
  font-size: 12px;
  line-height: 1.8;
  color: #484848;
  margin: 0 0 20px;
`;

export const BioLink = styled.a`
  color: #983fcc !important;
  font-size: 13px;
  text-decoration: none !important;
`;

export const BioMembers = styled.div`
  opacity: 0.7;
  font-size: 12px;
  font-weight: 600;
  line-height: 1.7;
  color: #000000;
  margin: 20px 0 10px;
`;

export const BioMemberTitle = styled.span`
  font-weight: 500;
  color: #484848;
`;

export const UpComingWrapper = styled.div`
  margin-bottom: 30px;
`;

export const UpcomingSlider = styled.div`
  padding: 0 5px;
`;

export const ConcertContainer = styled.div`
  padding: 0 5px;
`;

export const UpcomingHead = styled.div`
  margin-bottom: 20px;
`;

export const UpcomingTitle = styled.h3`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 0;
  display: inline-block;
`;

export const UpcomingViewAll = styled.div`
  float: right;
  a {
    font-size: 11px;
    color: #983fcc;
    display: block;
    border: 1px solid #983fcc;
    border-radius: 20px;
    padding: 5px 17px;
    text-decoration: none;
  }
`;
