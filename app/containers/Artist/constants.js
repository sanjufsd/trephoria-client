/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const headerBgImage = `${apiUrl}/images/artist_top_image.jpg`;
export const headerIcon = `${apiUrl}/images/artist_bulb.svg`;
export const images = [
  { image: `${apiUrl}/images/artist/1.jpg` },
  { image: `${apiUrl}/images/artist/2.jpg` },
  { image: `${apiUrl}/images/artist/3.jpg` },
  { image: `${apiUrl}/images/artist/11.jpg` },
  { image: `${apiUrl}/images/artist/13.jpg` },
  { image: `${apiUrl}/images/artist/14.jpg` },
  { image: `${apiUrl}/images/artist/15.jpg` },
  { image: `${apiUrl}/images/artist/16.jpg` },
  { image: `${apiUrl}/images/artist/17.jpg` },
];

export const concertLists = [
  {
    date: 30,
    month: 'Aug',
    title: 'Bumbershoot 2019',
    image: `${apiUrl}/images/concert_01.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 14,
    month: 'Sep',
    title: 'Drake (21 + Event)',
    image: `${apiUrl}/images/concert_02.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 22,
    month: 'Sep',
    title: `Hot 97's On Da Reggae Soca Tip`,
    image: `${apiUrl}/images/concert_03.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 30,
    month: 'Aug',
    title: 'Bumbershoot 2019',
    image: `${apiUrl}/images/concert_01.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 14,
    month: 'Sep',
    title: 'Drake (21 + Event)',
    image: `${apiUrl}/images/concert_02.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
  {
    date: 22,
    month: 'Sep',
    title: `Hot 97's On Da Reggae Soca Tip`,
    image: `${apiUrl}/images/concert_03.png`,
    tag: 'JAZZ',
    timing: 'Fri, 30 Aug - Sun, 1 Sep',
    timeLeft: 'in 4 days',
    location: 'Seattle Center',
    address: '305 Harrison St, Seattle, WA, USA',
    distance: '2.5km',
  },
];
export const concertLeftArrow = `${apiUrl}/images/trending_concert_left_arrow.svg`;
export const concertRightArrow = `${apiUrl}/images/trending_concert_right_arrow.png`;
