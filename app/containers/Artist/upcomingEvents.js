/* eslint-disable react/no-array-index-key */
import React from 'react';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as c from './constants';
import ErrorBoundary from '../../components/ErrorBoundary';
import SlickSliderArrow from '../../components/SlickSliderArrow';
import ConcertItem from '../../components/ConcertItem';

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  nextArrow: <SlickSliderArrow icon={c.concertRightArrow} position="right" />,
  prevArrow: <SlickSliderArrow icon={c.concertLeftArrow} position="left" />,
};

const UpComingEvents = () => (
  <S.UpComingWrapper>
    <S.UpcomingHead>
      <S.UpcomingTitle>Upcoming Events</S.UpcomingTitle>
      <S.UpcomingViewAll>
        <Link to="#0">View All</Link>
      </S.UpcomingViewAll>
    </S.UpcomingHead>
    <S.UpcomingSlider>
      <Slider {...settings}>
        {c.concertLists.map((item, index) => (
          <S.ConcertContainer key={index}>
            <ErrorBoundary>
              <ConcertItem item={item} />
            </ErrorBoundary>
          </S.ConcertContainer>
        ))}
      </Slider>
    </S.UpcomingSlider>
  </S.UpComingWrapper>
);

export default UpComingEvents;
