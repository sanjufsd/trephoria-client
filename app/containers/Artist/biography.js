import React, { useState, useEffect } from 'react';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from 'prop-types';
import parse from 'html-react-parser';
import * as S from './style';
import { artistBioUrl } from '../../config/apikey';
import SocialLinks from '../../components/SocialLinks';
import ErrorBoundary from '../../components/ErrorBoundary';
// import { social } from '../../components/SocialLinks/constants';

export const fetchBioData = data => {
  const socialNetwork = _.filter(data.relations, { type: 'social network' });
  const bandsIntown = _.find(data.relations, { type: 'bandsintown' });
  const image = _.find(data.relations, { type: 'image' });
  const official = _.find(data.relations, { type: 'official homepage' });
  const urls = {};
  if (socialNetwork.length) {
    socialNetwork.forEach(socialLink => {
      if (socialLink.url && socialLink.url.resource) {
        if (socialLink.url.resource.includes('instagram')) {
          urls.instagram = socialLink.url.resource;
        } else if (socialLink.url.resource.includes('facebook')) {
          urls.facebook = socialLink.url.resource;
        } else if (socialLink.url.resource.includes('twitter')) {
          urls.twitter = socialLink.url.resource;
        } else if (socialLink.url.resource.includes('spotify')) {
          urls.twitter = socialLink.url.resource;
        }
      }
    });
  }
  if (bandsIntown) {
    urls.bandsIntown = bandsIntown;
  }
  if (image) {
    urls.image = image;
  }
  if (official) {
    urls.official = official;
  }
  return urls;
};

const Biography = props => {
  const {
    activeArtist: { artist, artistBio },
  } = props;
  const {
    match: {
      params: { id },
    },
  } = props;
  const artistSelected = Object.prototype.hasOwnProperty.call(artist, 'id')
    ? artist
    : JSON.parse(localStorage.getItem('activeArtist')).artist;
  const artistBioSelected = Object.prototype.hasOwnProperty.call(
    artistBio,
    'id',
  )
    ? artistBio
    : JSON.parse(localStorage.getItem('activeArtist')).artistBio;
  const [state, setState] = useState('');
  const [urls, activeUrls] = useState({});

  useEffect(() => {
    axios
      .all([axios.get(`${artistBioUrl + artistSelected.id}/wikipedia-extract`)])
      .then(
        axios.spread((...responses) => {
          const wikiResponse = responses[0];
          const urlResponse = artistBioSelected;
          if (urlResponse) {
            activeUrls(fetchBioData(urlResponse));
          }
          if (
            wikiResponse.data.wikipediaExtract &&
            wikiResponse.data.wikipediaExtract.content
          ) {
            setState(wikiResponse.data.wikipediaExtract.content);
          } else {
            setState('<p>No data available</p>');
          }
        }),
      )
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
      });
  }, [id]);

  //   axios
  //     .get(`${artistBioUrl + artist.id}/wikipedia-extract`)
  //     .then(res => {
  //       console.log('res::::::::::;;;', res.data);
  //       if (res.data.wikipediaExtract && res.data.wikipediaExtract.content) {
  //         setState(res.data.wikipediaExtract.content);
  //         console.log('STATTTTE::::', state);
  //       } else {
  //         setState('<p>No data available</p>');
  //       }
  //     })
  //     .catch(err => {
  //       setState('<p>No data available</p>');
  //     });
  // }, []);
  return (
    <S.Biography>
      <S.BioTitle>Biography</S.BioTitle>
      <S.BioWrapper>
        <S.BioContent>
          <div> {parse(state)} </div>
        </S.BioContent>
        {urls.official && (
          <S.BioLink href={urls.official.url.resource} target="_blank">
            {urls.official.url.resource}
          </S.BioLink>
        )}
        {/* <S.BioMembers>
          <S.BioMemberTitle>Members: </S.BioMemberTitle>Members: James Hetfield,
          Lars Ulrich, Kirk Hammett…
        </S.BioMembers> */}
        <ErrorBoundary>
          {!_.isEmpty(urls) && (
            <SocialLinks
              {...urls.facebook && { facebook: urls.facebook }}
              {...urls.twitter && { twitter: urls.twitter }}
              {...urls.instagram && { instagram: urls.instagram }}
              {...urls.spotify && { spotify: urls.spotify }}
            />
          )}
        </ErrorBoundary>
      </S.BioWrapper>
    </S.Biography>
  );
};
Biography.propTypes = {
  activeArtist: PropTypes.object,
  match: PropTypes.object,
};

export default Biography;
