/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Container } from 'reactstrap';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as G from '../../global-styles';
import * as c from './constants';
import * as u from '../../config/url';
import { fetchBioData } from './biography';
// import { artistUrl, artistOnBIT } from '../../config/apikey';
let genres = '';
const Header = props => {
  const {
    activeArtist: { artist, artistBio },
  } = props;
  const artistData = {};
  const artistSelected = Object.prototype.hasOwnProperty.call(artist, 'id')
    ? artist
    : JSON.parse(localStorage.getItem('activeArtist')).artist;
  artistData.urls = fetchBioData(artistBio);
  try {
    genres = _.map(_.filter(artistBio.genres, 'count'), 'name').join(', ');
  } catch (error) {
    genres = '';
  }

  return (
    <S.Header bgImage={c.headerBgImage}>
      <Container>
        <S.HeaderWrapper>
          <S.HeaderLeftWrapper>
            <S.Breadcrumb>
              <Link to={u.homepage}>Home</Link> / {artistSelected.name}
              {/* <Link to={u.allGenre}>All Genre Page</Link> /{' '} */}
              {/* <Link to={`${u.singleGenre}/jazz`}>Jazz Music</Link> /{id} */}
            </S.Breadcrumb>
            <S.HeaderTitle>{artistSelected.name}</S.HeaderTitle>
            <S.HeaderSubTitle>{genres}</S.HeaderSubTitle>
          </S.HeaderLeftWrapper>
          <S.HeaderRightWrapper>
            <G.Div>
              <G.Div>
                <S.HeaderIcon src={c.headerIcon} />
                <S.HeaderRightText>
                  Metallica's "WorldWired Tour" is the 10th-highest-grossing
                  concert tour of all time ($377,850,000).
                </S.HeaderRightText>
              </G.Div>
            </G.Div>
          </S.HeaderRightWrapper>
        </S.HeaderWrapper>
      </Container>
    </S.Header>
  );
};

Header.propTypes = {
  activeArtist: PropTypes.object,
};

export default Header;
