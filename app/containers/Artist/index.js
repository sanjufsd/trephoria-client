import React, { Fragment } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import * as S from './style';
import * as c from './constants';
import ErrorBoundary from '../../components/ErrorBoundary';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import ArtistHeader from './header';
import Biography from './biography';
import ImagesCollage from '../../components/ImagesCollage';
import UpComingEvents from './upcomingEvents';
import Search from '../../components/Search';
import Activities from '../../components/Activities';

const Artist = props => (
  <Fragment>
    <ErrorBoundary>
      <Header {...props} bg />
    </ErrorBoundary>
    <S.Wrapper>
      <ErrorBoundary>
        <Search full />
      </ErrorBoundary>
      <ErrorBoundary>
        <ArtistHeader {...props} />
      </ErrorBoundary>
      <Container>
        <Row>
          <Col lg="3">
            <ErrorBoundary>
              <Biography {...props} />
            </ErrorBoundary>
            <ErrorBoundary>
              <ImagesCollage images={c.images} />
            </ErrorBoundary>
          </Col>
          <Col lg="9">
            <ErrorBoundary>
              <Activities />
            </ErrorBoundary>
            <ErrorBoundary>
              <UpComingEvents />
            </ErrorBoundary>
          </Col>
        </Row>
      </Container>
    </S.Wrapper>
    <ErrorBoundary>
      <Footer />
    </ErrorBoundary>
  </Fragment>
);

const mapStateToProps = ({ user, activeArtist }) => ({ user, activeArtist });

export default connect(mapStateToProps)(Artist);
