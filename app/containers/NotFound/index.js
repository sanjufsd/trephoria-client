import React from 'react';
import * as S from './style';

const NotFound = () => (
  <S.Wrapper>
    <S.Msg>Page not found</S.Msg>
  </S.Wrapper>
);

export default NotFound;
