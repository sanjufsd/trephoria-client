/* eslint-disable indent */
/* eslint-disable no-unused-vars */
import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import axios from 'axios';
import Header from '../../components/Header';
import ErrorBoundary from '../../components/ErrorBoundary';
import Footer from '../../components/Footer';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import GenreHeading from './heading';
import ArtistItem from '../../components/ArtistItem';
import ConcertItem from '../../components/ConcertItem';
import Filter from '../../components/Filter';
import { artistUrl } from '../../config/apikey';
import { Loader } from '../../components/Loader';

class Genre extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artistActive: true,
      artists: [],
      artistsLoading: true,
      artistsError: '',
      searchArtistText: '',
      filteredArtists: [],
      searchConcertText: '',
    };
  }

  componentDidMount() {
    this.getArtist();
  }

  getArtist = () => {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    axios
      .get(`${artistUrl}/${id}`)
      .then(res => {
        this.setState({ artistsLoading: false });
        if (res.data && res.data.artists && res.data.artists.length > 0) {
          this.setState({
            artists: res.data.artists,
            filteredArtists: res.data.artists,
          });
        } else {
          this.setState({
            // artistsError: 'No artists found',
            artists: c.artistLists,
            filteredArtists: c.artistLists,
          });
        }
      })
      .catch(err => {
        this.setState({
          artistsLoading: false,
          artistsError: 'No artists found',
        });
      });
  };

  handleActive = artistActive => {
    this.setState({ artistActive });
  };

  handleChange = e => {
    const { value } = e.target;
    const text = value ? value.toLowerCase() : value;
    this.setState(prevState => ({
      searchArtistText: prevState.artistActive
        ? value
        : prevState.searchArtistText,
      filteredArtists: prevState.artistActive
        ? prevState.artists.filter(item =>
            item.name.toLowerCase().includes(text),
          )
        : prevState.filteredArtists,
      searchConcertText: !prevState.artistActive
        ? value
        : prevState.searchConcertText,
    }));
  };

  render() {
    const {
      artistActive,
      artistsLoading,
      artists,
      artistsError,
      searchArtistText,
      filteredArtists,
      searchConcertText,
    } = this.state;
    const {
      match: {
        params: { id },
      },
    } = this.props;
    return (
      <Fragment>
        <ErrorBoundary>
          <Header bg {...this.props} />
        </ErrorBoundary>
        <S.GenreContainer>
          <ErrorBoundary>
            <GenreHeading name={id} artistCount={artists.length} />
          </ErrorBoundary>
          <S.GenreBody>
            <Container>
              <Row>
                <Col xl="2" lg="3">
                  <S.GoBack>
                    <Link to={u.allGenre}>
                      <S.GoBackArrow src={c.genreBackArrow} alt="" />
                      <S.GoBackText>Back to All Genres</S.GoBackText>
                    </Link>
                  </S.GoBack>
                  <S.Tabs>
                    <S.TabButton
                      active={artistActive}
                      onClick={() => {
                        this.handleActive(true);
                      }}
                    >
                      <S.TabIcon
                        src={
                          artistActive
                            ? c.artistActiveIcon
                            : c.artistInactiveIcon
                        }
                        alt=""
                      />{' '}
                      Artists
                    </S.TabButton>
                    <S.TabButton
                      active={!artistActive}
                      onClick={() => {
                        this.handleActive(false);
                      }}
                    >
                      <S.TabIcon
                        src={
                          !artistActive
                            ? c.concertActiveIcon
                            : c.concertInactiveIcon
                        }
                        alt=""
                      />{' '}
                      Concerts
                    </S.TabButton>
                  </S.Tabs>
                </Col>
                <Col xl="10" lg="9">
                  <S.GenreListWrapper>
                    <S.GenreTitle>
                      {artistActive ? 'Artists' : 'Concerts'} ({id})
                    </S.GenreTitle>
                    <S.SearchFilter>
                      <S.SearchForm>
                        <S.Searchinput
                          type="text"
                          placeholder={`Search for ${
                            artistActive ? 'artists' : 'concerts'
                          }`}
                          name={artistActive ? 'artists' : 'concerts'}
                          value={
                            artistActive ? searchArtistText : searchConcertText
                          }
                          onChange={this.handleChange}
                        />
                        <S.SearchIcon src={c.genreSearch} alt="" />
                      </S.SearchForm>
                      <ErrorBoundary>
                        <Filter />
                      </ErrorBoundary>
                    </S.SearchFilter>
                    {artistActive ? (
                      <Fragment>
                        <S.AllArtists>
                          {!artistsLoading ? (
                            filteredArtists.map(item => (
                              <S.Artist key={item.id}>
                                <ErrorBoundary>
                                  <ArtistItem item={item} />
                                </ErrorBoundary>
                              </S.Artist>
                            ))
                          ) : (
                            <ErrorBoundary>
                              <Loader bgColor="transparent" padding="100px 0" />
                            </ErrorBoundary>
                          )}
                        </S.AllArtists>
                        {artistsError && (
                          <S.ErrorMsg>{artistsError}</S.ErrorMsg>
                        )}
                      </Fragment>
                    ) : (
                      <S.AllConcerts>
                        {c.concertLists.map((item, index) => (
                          // eslint-disable-next-line react/no-array-index-key
                          <S.Concert key={index}>
                            <ErrorBoundary>
                              <ConcertItem item={item} />
                            </ErrorBoundary>
                          </S.Concert>
                        ))}
                      </S.AllConcerts>
                    )}
                  </S.GenreListWrapper>
                </Col>
              </Row>
            </Container>
          </S.GenreBody>
        </S.GenreContainer>
        <ErrorBoundary>
          <Footer />
        </ErrorBoundary>
      </Fragment>
    );
  }
}

Genre.propTypes = {
  match: PropTypes.object,
};

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(Genre);
