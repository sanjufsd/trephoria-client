import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Container } from 'reactstrap';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';

const GenreHeadingContainer = props => {
  const { name, artistCount } = props;
  return (
    <S.GenreHeader>
      <Container>
        <S.Breadcrumb>
          <Link to={u.homepage}>Home</Link> /{' '}
          <Link to={u.allGenre}>All Genre Page</Link> / {name}
        </S.Breadcrumb>
        <S.TitleContainer>
          <S.TitleImage src={c.jazzMusic} alt="" />
          <S.TitleDetails>
            <S.Title>
              {name}
              <S.Info>
                <S.InfoImg src={c.genreInfo} alt="" />
              </S.Info>
            </S.Title>
            <S.Description>
              Stylistic origins -{' '}
              <S.Coloured>
                Blues, Ragtime, Spirituals, Folk, Marches, Classical Music of
                West Africa.
              </S.Coloured>
            </S.Description>
            <S.GenreIconWrapper>
              <S.GenreIcon src={c.artistActiveIcon} alt="" />
              <S.GenreCount>{artistCount || 0} Artists</S.GenreCount>
            </S.GenreIconWrapper>
          </S.TitleDetails>
        </S.TitleContainer>
      </Container>
    </S.GenreHeader>
  );
};

GenreHeadingContainer.propTypes = {
  name: PropTypes.string,
  artistCount: PropTypes.number,
};

export default GenreHeadingContainer;
