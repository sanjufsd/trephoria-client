/**
 *
 * Asynchronously loads the component for Genre
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
