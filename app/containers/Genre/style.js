import styled from 'styled-components';

export const GenreContainer = styled.div`
  padding: 80px 0 0 0;
  min-height: calc(100vh - 97px);
`;

export const GenreHeader = styled.div`
  background: #18154f;
  color: white;
  padding: 15px 0;
`;

export const Breadcrumb = styled.div`
  opacity: 0.5;
  font-size: 12px;
  color: #ffffff;
  margin: 0 0 15px;
  text-transform: capitalize;
  a {
    color: white !important;
    text-decoration: none !important;
  }
`;

export const TitleContainer = styled.div`
  position: relative;
  padding-left: 115px;
`;

export const TitleImage = styled.img`
  width: 95px;
  height: 95px;
  border-radius: 50%;
  object-fit: cover;
  position: absolute;
  left: 0;
  top: 0;
`;

export const TitleGenreContainer = styled.div``;

export const TitleDetails = styled.div`
  padding: 5px 0 0;
`;

export const Description = styled.p`
  font-size: 12px;
  color: #8a8a8a;
  margin: 0 0 10px;
`;

export const Coloured = styled.span`
  color: white;
`;

export const GenreIconWrapper = styled.div``;

export const GenreIcon = styled.img`
  width: 24px;
  vertical-align: middle;
  margin: 0 5px 0 0;
`;

export const GenreCount = styled.span`
  font-size: 12px;
  vertical-align: middle;
  display: inline-block;
`;

export const Title = styled.div`
  font-size: 22px;
  font-weight: 500;
  line-height: 1.36;
  margin: 0 0 5px;
  text-transform: capitalize;
`;

export const Info = styled.span`
  display: inline-block;
  margin: 0 0 0 10px;
`;

export const InfoImg = styled.img`
  width: 18px;
`;

export const GenreBody = styled.div`
  padding: 20px 0;
`;

export const GoBack = styled.div`
  margin: 0 0 20px;
  a {
    color: #1c1c1c !important;
    font-size: 10px;
    text-decoration: none !important;
  }
`;

export const GoBackArrow = styled.img`
  width: 25px;
  vertical-align: middle;
  margin: -1px 10px 0 0;
`;

export const GoBackText = styled.span`
  opacity: 0.8;
`;

export const Tabs = styled.div`
  border-radius: 8px;
  border: 1px solid #e1e1e1;
  padding: 30px 15px 15px;
  position: relative;
  :before {
    content: '';
    position: absolute;
    background: #f1f1f1;
    width: 30px;
    height: 100px;
    top: 12px;
    left: 15px;
    border-radius: 16px;
    z-index: -1;
  }
`;

export const TabButton = styled.button`
  display: block;
  background: transparent;
  margin: 0 0 15px;
  outline: 0 !important;
  border: 0 !important;
  font-size: 12px;
  color: ${props => (props.active ? '#ff5853' : 'black')};
`;

export const TabIcon = styled.img`
  width: 20px;
  margin: 0 15px 0 0;
`;

export const GenreListWrapper = styled.div``;

export const GenreTitle = styled.h2`
  font-size: 18px;
  font-weight: bold;
  line-height: 1.67;
  color: #000000;
  margin: 0 0 15px;
  text-transform: capitalize;
`;

export const SearchFilter = styled.div`
  margin: 0 0 20px;
  > button {
    float: right;
  }
`;

export const SearchForm = styled.form`
  display: inline-block;
  width: 400px;
  position: relative;
`;

export const Searchinput = styled.input`
  width: 100%;
  background: #f7f8f8;
  border: 1px solid #ececec;
  font-size: 12px;
  border-radius: 8px;
  padding: 10px 20px 8px 50px;
  outline: 0 !important;
  box-shadow: none;
`;

export const SearchIcon = styled.img`
  height: 12px;
  position: absolute;
  top: 14px;
  left: 20px;
`;

export const AllArtists = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: calc(100% + 30px);
`;

export const Artist = styled.div`
  width: calc(20% - 30px);
  margin: 0 30px 5px 0;
`;

export const AllConcerts = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: calc(100% + 15px);
`;

export const Concert = styled.div`
  width: calc(33.3333% - 15px);
  margin: 0 15px 15px 0;
`;

export const ErrorMsg = styled.div`
  text-align: center;
  font-size: 13px;
  margin: 20px 0;
`;
