/*
 * Genre Messages
 *
 * This contains all the text for the Genre container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Genre';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Genre container!',
  },
});
