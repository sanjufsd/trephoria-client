import styled from 'styled-components';

export const ForgotHeading = styled.div`
  display: inline-block;
  width: 100%;
  padding: 0 50px;
  font-size: 14px;
`;

export const GoBackButton = styled.button`
  background: transparent;
  outline: 0 !important;
  border: 0;
  cursor: pointer;
  padding: 9px 9px 9px 0;
`;

export const GoBackIcon = styled.img`
  width: 45px;
`;

export const ForgotFormContainer = styled.div``;

export const FormWrapper = styled.div`
  padding: 30px 0;
  form {
    padding: 15px 0;
    .form-group {
      margin-bottom: 20px;
      > label {
        font-size: 12px;
        margin: 0 0 10px;
        display: block;
      }
      > input {
        box-shadow: none !important;
        border-color: #363138;
        outline: 0;
        font-size: 12px;
        padding: 10px 15px !important;
        height: auto !important;
        color: black;
        background-image: none;
      }
      .invalid-feedback {
        font-size: 12px;
        margin: 10px 0 0;
      }
    }
  }
`;
