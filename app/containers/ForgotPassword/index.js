import React from 'react';

import ModalWrapper from '../../components/ModalWrapper';

const ForgotPassword = props => <ModalWrapper {...props} />;

export default ForgotPassword;
