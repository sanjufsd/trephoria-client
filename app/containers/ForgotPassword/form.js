import React from 'react';
import { FormGroup, Label } from 'reactstrap';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import * as S from './style';
import Button from '../../components/Button';

const forgotPasswordSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email is invalid')
    .required('Email is required'),
  password: Yup.string()
    .min(6, 'Password must be at least 6 characters')
    .required('Password is required'),
});

const ForgotFormWrapper = () => (
  <S.ForgotFormContainer>
    <S.FormWrapper>
      <Formik
        initialValues={{
          email: '',
        }}
        validationSchema={forgotPasswordSchema}
        // onSubmit={values => {
        //   console.log({ values });
        // }}
      >
        {({ errors, touched }) => (
          <Form>
            <FormGroup>
              <Label>Email*</Label>
              <Field
                name="email"
                type="email"
                className={`form-control${
                  errors.email && touched.email ? ' is-invalid' : ''
                }`}
                autoComplete="off"
                placeholder="Enter your email id"
              />
              <ErrorMessage
                name="email"
                component="div"
                className="invalid-feedback"
              />
            </FormGroup>
            <Button type="submit" text="Submit" />
          </Form>
        )}
      </Formik>
    </S.FormWrapper>
  </S.ForgotFormContainer>
);

export default ForgotFormWrapper;
