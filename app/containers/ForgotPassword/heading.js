import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import { goBackArrow } from './constants';

const Heading = props => {
  const { history } = props;
  return (
    <S.ForgotHeading>
      <S.GoBackButton
        onClick={() => {
          history.goBack();
        }}
      >
        <S.GoBackIcon src={goBackArrow} alt="" />
      </S.GoBackButton>
      Forgot Password
    </S.ForgotHeading>
  );
};

Heading.propTypes = {
  history: PropTypes.object,
};

export default Heading;
