import React, { useState } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { CustomInput, FormGroup, Label } from 'reactstrap';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Button from '../../components/Button';
import * as S from './style';
import * as u from '../../config/url';
import { loginUrl } from '../../config/apikey';
import { saveUser } from '../../redux/actions/user';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email is invalid')
    .required('Email is required'),
  password: Yup.string()
    .min(6, 'Password must be at least 6 characters')
    .required('Password is required'),
});

const LoginFormWrapper = props => {
  const { eye, dispatch, history } = props;
  const [viewPass, setViewPass] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  return (
    <S.LoginFormContainer>
      <S.FormWrapper>
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          validationSchema={loginSchema}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(true);
            setErrorMsg('');
            axios
              .post(loginUrl, values)
              .then(res => {
                setSubmitting(false);
                if (res.data.accessToken && res.data.refreshToken) {
                  dispatch(saveUser(res.data));
                  localStorage.setItem('user', JSON.stringify(res.data));
                  history.push(u.userActivities);
                } else {
                  setErrorMsg('Something went wrong');
                }
              })
              .catch(err => {
                setSubmitting(false);
                if (err.response.status === 400) {
                  setErrorMsg(err.response.data.errors[0]);
                } else if (err.response.status === 404) {
                  setErrorMsg('Invalid e-mail or password');
                } else {
                  setErrorMsg('Something went wrong');
                }
              });
          }}
        >
          {({ errors, touched, isSubmitting }) => (
            <Form>
              <FormGroup>
                <Label>Email*</Label>
                <Field
                  name="email"
                  type="email"
                  className={`form-control${
                    errors.email && touched.email ? ' is-invalid' : ''
                  }`}
                  autoComplete="off"
                  placeholder="Enter your email id"
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="invalid-feedback"
                />
              </FormGroup>
              <S.PasswordField>
                <FormGroup>
                  <Label>Password*</Label>
                  <Field
                    name="password"
                    type={viewPass ? 'text' : 'password'}
                    className={`form-control${
                      errors.password && touched.password ? ' is-invalid' : ''
                    }`}
                    placeholder="Enter your password"
                  />
                  <ErrorMessage
                    name="password"
                    component="div"
                    className="invalid-feedback"
                  />
                </FormGroup>
                <S.ViewPassword
                  onClick={() => {
                    setViewPass(!viewPass);
                  }}
                >
                  <S.EyeIcon src={eye} alt="" />
                </S.ViewPassword>
              </S.PasswordField>
              <S.Options>
                <FormGroup>
                  <CustomInput
                    type="checkbox"
                    id="remember-me"
                    label="Remember me"
                  />
                </FormGroup>
                <Link to={u.forgotPassword}>Forgot password ?</Link>
              </S.Options>
              <Button
                type="submit"
                text={
                  isSubmitting ? (
                    <i className="fas fa-spinner fa-spin" />
                  ) : (
                    'Login'
                  )
                }
                disabled={isSubmitting}
              />
              {errorMsg && <S.CustomError>{errorMsg}</S.CustomError>}
            </Form>
          )}
        </Formik>
      </S.FormWrapper>
    </S.LoginFormContainer>
  );
};

LoginFormWrapper.propTypes = {
  eye: PropTypes.string,
  dispatch: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(LoginFormWrapper);
