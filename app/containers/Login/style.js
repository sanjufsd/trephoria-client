import styled from 'styled-components';

export const LoginFormContainer = styled.div``;

export const FormWrapper = styled.div`
  form {
    padding: 15px 0;
    .form-group {
      margin-bottom: 10px;
      > label {
        font-size: 12px;
        margin: 0 0 10px;
        display: block;
      }
      > input {
        box-shadow: none !important;
        border-color: #363138;
        outline: 0;
        font-size: 12px;
        padding: 10px 15px !important;
        height: auto !important;
        color: black;
        background-image: none;
      }
    }
    .invalid-feedback {
      font-size: 12px;
      margin: 10px 0 0;
    }
  }
`;

export const PasswordField = styled.div`
  position: relative;
`;

export const ViewPassword = styled.span`
  cursor: pointer;
  display: block;
  position: absolute;
  right: 2px;
  top: 30px;
  padding: 6px 10px;
  background: white;
`;

export const CustomError = styled.p`
  color: #dc3545;
  margin: 15px 0 0;
  font-size: 12px;
`;

export const EyeIcon = styled.img`
  width: 22px;
`;

export const Options = styled.div`
  margin: 0 0 10px;
  .form-group {
    display: inline-block;
    margin: 0 !important;
    font-size: 12px;
  }
  a {
    float: right;
    color: #2b2b2b !important;
    font-size: 12px;
    margin: 4px 0 0;
    text-decoration: none;
  }
`;
