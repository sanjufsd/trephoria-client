import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';

const Heading = props => {
  const { history } = props;
  return (
    <S.ResetHeading>
      <S.CloseButton
        onClick={() => {
          history.push(u.homepage);
        }}
      >
        <S.CloseIcon src={c.resetClose} alt="" />
      </S.CloseButton>
      Enter New Password
    </S.ResetHeading>
  );
};

Heading.propTypes = {
  history: PropTypes.object,
};

export default Heading;
