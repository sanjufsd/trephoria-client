import React from 'react';
import { Link } from 'react-router-dom';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';

const PasswordSuccess = () => (
  <S.PasswordSuccessContainer>
    <S.PasswordSuccessIcon src={c.successIcon} alt="" />
    <S.PasswordSuccessTitle>
      Password Changed Successfully!
    </S.PasswordSuccessTitle>
    <S.PasswordSuccessMsg>Kindly login again to continue</S.PasswordSuccessMsg>
    <Link to={u.login}>Login</Link>
  </S.PasswordSuccessContainer>
);

export default PasswordSuccess;
