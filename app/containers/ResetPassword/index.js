import React from 'react';

import ModalWrapper from '../../components/ModalWrapper';

const ResetPassword = props => <ModalWrapper {...props} />;

export default ResetPassword;
