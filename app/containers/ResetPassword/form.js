import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FormGroup, Label } from 'reactstrap';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import * as S from './style';
import Button from '../../components/Button';

const resetPasswordSchema = Yup.object().shape({
  password: Yup.string()
    .min(6, 'Password must be at least 6 characters')
    .required('Password is required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Confirm Password is required'),
});

const ResetFormWrapper = props => {
  const { eye } = props;
  const [viewPass, setViewPass] = useState(false);
  const [viewConfirmPass, setViewConfirmPass] = useState(false);
  return (
    <S.ResetFormContainer>
      <S.FormWrapper>
        <Formik
          initialValues={{
            password: '',
            confirmPassword: '',
          }}
          validationSchema={resetPasswordSchema}
          // onSubmit={values => {
          //   console.log({ values });
          // }}
        >
          {({ errors, touched }) => (
            <Form>
              <S.PasswordField>
                <FormGroup>
                  <Label>Enter New Password*</Label>
                  <Field
                    name="password"
                    type="password"
                    className={`form-control${
                      errors.password && touched.password ? ' is-invalid' : ''
                    }`}
                    autoComplete="off"
                    placeholder="Enter new password"
                  />
                  <ErrorMessage
                    name="password"
                    component="div"
                    className="invalid-feedback"
                  />
                </FormGroup>
                <S.ViewPassword
                  onClick={() => {
                    setViewPass(!viewPass);
                  }}
                >
                  <S.EyeIcon src={eye} alt="" />
                </S.ViewPassword>
              </S.PasswordField>
              <S.PasswordField>
                <FormGroup>
                  <Label>Re-Enter New Password*</Label>
                  <Field
                    name="confirmPassword"
                    type="password"
                    className={`form-control${
                      errors.confirmPassword && touched.confirmPassword
                        ? ' is-invalid'
                        : ''
                    }`}
                    autoComplete="off"
                    placeholder="Re-Enter new password"
                  />
                  <ErrorMessage
                    name="confirmPassword"
                    component="div"
                    className="invalid-feedback"
                  />
                </FormGroup>
                <S.ViewPassword
                  onClick={() => {
                    setViewConfirmPass(!viewConfirmPass);
                  }}
                >
                  <S.EyeIcon src={eye} alt="" />
                </S.ViewPassword>
              </S.PasswordField>
              <Button type="submit" text="Submit" />
            </Form>
          )}
        </Formik>
      </S.FormWrapper>
    </S.ResetFormContainer>
  );
};

ResetFormWrapper.propTypes = {
  eye: PropTypes.string,
};

export default ResetFormWrapper;
