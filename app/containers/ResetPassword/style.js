import styled from 'styled-components';

export const ResetHeading = styled.div`
  display: inline-block;
  width: 100%;
  padding: 0 50px;
  font-size: 14px;
`;

export const CloseButton = styled.button`
  background: transparent;
  outline: 0 !important;
  border: 0;
  cursor: pointer;
  padding: 17px 17px 17px 0;
`;

export const CloseIcon = styled.img`
  width: 11px;
`;

export const ResetFormContainer = styled.div``;

export const FormWrapper = styled.div`
  form {
    padding: 15px 0;
    .form-group {
      margin-bottom: 10px;
      > label {
        font-size: 12px;
        margin: 0 0 10px;
        display: block;
      }
      > input {
        box-shadow: none !important;
        border-color: #363138;
        outline: 0;
        font-size: 12px;
        padding: 10px 15px !important;
        height: auto !important;
        color: black;
        background-image: none;
      }
    }
    .invalid-feedback {
      font-size: 12px;
      margin: 10px 0 0;
    }
  }
`;

export const PasswordField = styled.div`
  position: relative;
  > span {
    cursor: pointer;
    display: block;
    position: absolute;
    right: 2px;
    top: 30px;
    padding: 6px 10px;
    background: white;
    img {
      width: 22px;
    }
  }
`;

export const ViewPassword = styled.span`
  cursor: pointer;
  display: block;
  position: absolute;
  right: 2px;
  top: 30px;
  padding: 6px 10px;
  background: white;
`;

export const EyeIcon = styled.img`
  width: 22px;
`;

export const PasswordSuccessContainer = styled.div`
  padding-left: 50px;
  a {
    border-radius: 20px;
    border: solid 2px #983fcc;
    background-color: #983fcc;
    display: inline-block;
    color: white;
    padding: 5px 30px;
    text-decoration: none;
    font-size: 14px;
  }
`;

export const PasswordSuccessIcon = styled.img`
  width: 83px;
  margin: 0 0 20px;
`;

export const PasswordSuccessTitle = styled.h2`
  font-size: 20px;
  font-weight: bold;
  line-height: 1.3;
  color: #3e4c5b;
  width: 250px;
  max-width: 100%;
  margin: 0 0 15px;
`;

export const PasswordSuccessMsg = styled.div`
  font-size: 12px;
  color: #929699;
  margin: 0 0 30px;
`;
