import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './style';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { GlobalStyle } from '../../global-styles';
import * as u from '../../config/url';

import ScrollTop from './scrollTop';
import Login from '../Login';
import Genre from '../Genre';
import Signup from '../Signup';
import SignupProcess from '../SignupProcess';
import ResetPassword from '../ResetPassword';
import ForgotPassword from '../ForgotPassword';
import HomePage from '../HomePage';
import NotFound from '../NotFound';
import Genres from '../Genres';
import Artist from '../Artist';
import Concert from '../Concert';
import UserActivities from '../User/Activities';
import UserConcerts from '../User/Concerts';
import UserArtists from '../User/Artists';
import UserBookmarks from '../User/Bookmarks';

export default function App() {
  return (
    <div className="app">
      <ScrollTop>
        <Switch>
          <Route exact path={u.login} component={Login} />
          <Route exact path={u.signup} component={Signup} />
          <Route exact path={u.signupSuccess} component={Signup} />
          <Route exact path={u.signupProcess} component={SignupProcess} />
          <Route exact path={u.resetPassword} component={ResetPassword} />
          <Route exact path={u.forgotPassword} component={ForgotPassword} />
          <Route exact path={u.passwordSuccess} component={ResetPassword} />
          <Route exact path={u.homepage} component={HomePage} />
          <Route exact path={u.allGenre} component={Genres} />
          <Route exact path={`${u.singleGenre}/:id`} component={Genre} />
          <Route exact path={`${u.singleArtist}/:id`} component={Artist} />
          <Route exact path={`${u.singleConcert}`} component={Concert} />
          <Route
            exact
            path={`${u.userActivities}`}
            component={UserActivities}
          />
          <Route exact path={`${u.userConcerts}`} component={UserConcerts} />
          <Route exact path={`${u.userArtists}`} component={UserArtists} />
          <Route exact path={`${u.userBookmarks}`} component={UserBookmarks} />
          <Route component={NotFound} />
        </Switch>
      </ScrollTop>
      <GlobalStyle />
    </div>
  );
}
