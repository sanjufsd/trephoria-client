import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import ModalWrapper from '../../components/ModalWrapper';
import Heading from './heading';
import Content from './content';
import * as u from '../../config/url';

const SignupProcess = props => {
  const {
    signup: { email, password },
  } = props;
  return (
    <Fragment>
      {email && password ? (
        <ModalWrapper
          {...props}
          headingComponent={<Heading {...props} />}
          contentComponent={<Content {...props} />}
        />
      ) : (
        <Redirect to={u.signup} />
      )}
    </Fragment>
  );
};

SignupProcess.propTypes = {
  signup: PropTypes.object,
};

const mapStateToProps = ({ signup }) => ({ signup });

export default connect(mapStateToProps)(SignupProcess);
