/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
import styled from 'styled-components';

export const SignupHeading = styled.div`
  display: inline-block;
  width: 100%;
  padding: 0 50px;
  font-size: 14px;
`;

export const CloseButton = styled.button`
  background: transparent;
  outline: 0 !important;
  border: 0;
  cursor: pointer;
  padding: 17px 17px 17px 0;
`;

export const CloseIcon = styled.img`
  width: 11px;
`;

export const ContentWrapper = styled.div`
  padding: 30px 50px 0;
`;

export const LeftWrapper = styled.div`
  border-right: 1px solid rgba(0, 0, 0, 0.1);
  padding-right: 30px;
  height: 100%;
`;

export const RightWrapper = styled.div`
  position: relative;
  height: 100%;
`;

export const LeftTabsWrapper = styled.div`
  position: relative;
  border-radius: 8px;
  border: solid 1px #e1e1e1;
  padding: 20px 30px 20px 60px;
  margin-bottom: 30px;
  :before {
    content: '';
    position: absolute;
    border-radius: 16px;
    background-color: #f6f6f6;
    width: 30px;
    height: calc(100% - 25px);
    top: 15px;
    left: 15px;
  }
`;

export const LeftTabButton = styled.button`
  border: 0 !important;
  outline: 0 !important;
  background: transparent;
  cursor: pointer;
  font-size: 12px;
  padding: 10px 0;
  line-height: 1;
  text-align: left;
  position: relative;
  width: 100%;
  color: ${props =>
    props.step > props.index
      ? '#37b487'
      : props.step === props.index
      ? '#ff5853'
      : 'black'};
  &:before {
    content: '';
    width: 5px;
    height: 5px;
    background: ${props =>
      props.step === props.index ? '#ff5853' : 'transparent'};
    position: absolute;
    left: -32px;
    top: 13px;
    border-radius: 50%;
  }
`;

export const RightIcon = styled.img`
  position: absolute;
  right: 0;
  top: 10px;
  width: 6px;
`;

export const SuccessIcon = styled.img`
  position: absolute;
  left: -35px;
  top: 12px;
`;

export const LeftTabButtonNote = styled.span`
  display: block;
  color: #9c9c9c !important;
  padding: 5px 0 0;
`;

export const ProfilePicWrapper = styled.div`
  position: relative;
  overflow: hidden;
`;

export const ProfilePicInputWrapper = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  width: 50px;
`;

export const ProfilePicInput = styled.input`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
`;

export const ProfilePicInputicon = styled.img`
  width: 100%;
`;

export const ProfilePicImage = styled.img`
  width: 100%;
  border-radius: 50%;
  height: 220px;
  object-fit: cover;
`;

export const ProfilePicError = styled.p`
  margin: 10px 0 0;
  text-align: center;
  font-size: 12px;
  color: #ff5853;
`;

export const SignupStep1Form = styled.div`
  form {
    .form-group {
      margin-bottom: 20px;
      > label {
        font-size: 12px;
        margin: 0 0 10px;
        display: block;
      }
      > input,
      select {
        box-shadow: none !important;
        border-color: #363138;
        outline: 0;
        font-size: 12px;
        padding: 10px 15px !important;
        height: auto !important;
        color: black;
        background-image: none;
      }
    }
    .invalid-feedback {
      font-size: 12px;
      margin: 10px 0 0;
      color: #ff5853;
    }
  }
`;

export const Inlineradio = styled.div`
  > div {
    display: inline-block;
    margin: 0 15px 0 0;
  }
`;

export const PasswordField = styled.div`
  position: relative;
`;

export const ViewPassword = styled.span`
  cursor: pointer;
  display: block;
  position: absolute;
  right: 2px;
  top: 30px;
  padding: 6px 10px;
  background: white;
`;

export const CustomError = styled.p`
  color: #ff5853;
  margin: 0;
  font-size: 12px;
`;

export const EyeIcon = styled.img`
  width: 22px;
`;

export const Footer = styled.div`
  background: #1e1b1f;
  height: 65px;
  margin: 30px -50px 0;
`;

export const StepWrapper = styled.div`
  text-align: right;
  font-size: 12px;
  position: absolute;
  right: 0;
  bottom: -15px;
`;

export const StepArrowIcon = styled.img`
  width: 17px;
  vertical-align: baseline;
  margin: 0 0 0 5px;
`;

export const StepButtons = styled.div`
  position: absolute;
  bottom: -80px;
  right: 0;
  button {
    margin-left: 20px;
  }
`;

export const ConnectSocialWrapper = styled.div``;

export const ConnectSocialSkipWrapper = styled.div`
  position: absolute;
  right: 0;
  top: -70px;
`;

export const ConnectSocialSkipBtn = styled.button`
  border-radius: 20px;
  border: solid 1px #983fcc;
  background: transparent;
  font-size: 12px;
  color: #983fcc;
  outline: 0 !important;
  padding: 4px 30px 3px;
`;

export const ConnectSocialTitle = styled.h2`
  font-size: 12px;
  margin-bottom: 30px;
`;

export const ConnectSocialSubTitle = styled.span`
  display: inline-block;
  color: #a5a5a5;
  margin: 0 0 0 5px;
`;

export const ConnectSocialItems = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0 -15px;
`;

export const ConnectSocialItem = styled.div`
  width: 25%;
  margin-bottom: 30px;
  padding: 0 15px;
`;
