import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import axios from 'axios';
import * as S from './style';
import * as c from './constants';
import LeftTabs from './leftTabs';
import ProfilePicture from './profilePicture';
import CreateProfile from './createProfile';
import SocialAccounts from './socialAccounts';
import Personalization from './personalization';
import ErrorBoundary from '../../components/ErrorBoundary';
import { signupUrl } from '../../config/apikey';
import { Loader } from '../../components/Loader';
import { signupDetails } from '../../redux/actions/signup';

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      gender: '',
      birthDate: '',
      birthMonth: '',
      birthYear: '',
      email: props.signup.email || '',
      password: props.signup.password || '',
      profilePic: '',
      profilePicPreview: '',
      profileSubmitting: false,
      profileSubmitted: false,
      profileCreationError: '',
      step: 0,
      socialAccounts: [],
      selectedArtists: [],
    };
  }

  handleSteps = step => {
    this.setState({ step });
  };

  handleProfilePic = (image, preview) => {
    this.setState({
      profilePic: image,
      profilePicPreview: preview,
    });
  };

  updateProfileDetails = data => {
    const { profileSubmitted } = this.state;
    const { dispatch } = this.props;
    if (!profileSubmitted) {
      const {
        firstname,
        lastname,
        gender,
        birthDate,
        birthMonth,
        birthYear,
        email,
        password,
      } = data;
      const dob = Date.parse(
        `${birthMonth || 'January'} ${birthDate || 1}, ${birthYear || 2019}`,
      );
      const formData = { firstname, lastname, gender, dob, email, password };
      this.setState({ profileSubmitting: true });
      axios
        .post(signupUrl, formData)
        .then(res => {
          this.setState({ profileSubmitting: false });
          if (res.data.id) {
            this.setState({
              profileSubmitted: true,
              step: 1,
              ...data,
            });
            dispatch(signupDetails({ id: res.data.id }));
          } else {
            this.setState({ profileCreationError: 'Something went wrong' });
          }
        })
        // eslint-disable-next-line no-unused-vars
        .catch(err => {
          this.setState({ profileCreationError: 'Something went wrong' });
        });
    } else {
      this.setState({ step: 1 });
    }
  };

  handleSocialConnect = item => {
    this.setState(prevState => ({
      socialAccounts: prevState.socialAccounts.concat(item),
    }));
  };

  handleSocialDisconnect = item => {
    this.setState(prevState => ({
      socialAccounts: prevState.socialAccounts.filter(
        social => social.title !== item.title,
      ),
    }));
  };

  addArtist = item => {
    this.setState(prevState => ({
      selectedArtists: [...prevState.selectedArtists, ...[item]],
    }));
  };

  removeArtist = item => {
    this.setState(prevState => ({
      selectedArtists: prevState.selectedArtists.filter(
        artist => artist.id !== item.id,
      ),
    }));
  };

  render() {
    const {
      firstname,
      lastname,
      gender,
      birthDate,
      birthMonth,
      birthYear,
      email,
      password,
      step,
      profilePic,
      profilePicPreview,
      socialAccounts,
      selectedArtists,
      profileSubmitting,
      profileCreationError,
    } = this.state;
    const { history } = this.props;
    return (
      <S.ContentWrapper>
        <Row>
          <Col lg="4">
            <S.LeftWrapper>
              <ErrorBoundary>
                <LeftTabs step={step} handleSteps={this.handleSteps} />
              </ErrorBoundary>
              {step === 0 && (
                <ErrorBoundary>
                  <ProfilePicture
                    profilePic={profilePic}
                    profilePicPreview={profilePicPreview}
                    handleProfilePic={this.handleProfilePic}
                  />
                </ErrorBoundary>
              )}
            </S.LeftWrapper>
          </Col>
          <Col lg="8">
            <S.RightWrapper>
              {step === 0 && (
                <ErrorBoundary>
                  <CreateProfile
                    firstname={firstname}
                    lastname={lastname}
                    gender={gender}
                    email={email}
                    password={password}
                    birthDate={birthDate}
                    birthMonth={birthMonth}
                    birthYear={birthYear}
                    history={history}
                    updateProfileDetails={this.updateProfileDetails}
                    profileCreationError={profileCreationError}
                  />
                </ErrorBoundary>
              )}
              {step === 1 && (
                <ErrorBoundary>
                  <SocialAccounts
                    handleSteps={this.handleSteps}
                    history={history}
                    socialAccounts={socialAccounts}
                    handleSocialConnect={this.handleSocialConnect}
                    handleSocialDisconnect={this.handleSocialDisconnect}
                  />
                </ErrorBoundary>
              )}
              {step === 2 && (
                <ErrorBoundary>
                  <Personalization
                    handleSteps={this.handleSteps}
                    history={history}
                    selectedArtists={selectedArtists}
                    addArtist={this.addArtist}
                    removeArtist={this.removeArtist}
                  />
                </ErrorBoundary>
              )}
              <S.StepWrapper>
                {step + 1} of 3 <S.StepArrowIcon src={c.rightStepArrow} />
              </S.StepWrapper>
            </S.RightWrapper>
          </Col>
        </Row>
        <S.Footer />
        {profileSubmitting && <Loader />}
      </S.ContentWrapper>
    );
  }
}

Content.propTypes = {
  history: PropTypes.object,
  signup: PropTypes.object,
  dispatch: PropTypes.func,
};

const mapStateToProps = ({ signup }) => ({ signup });

export default connect(mapStateToProps)(Content);
