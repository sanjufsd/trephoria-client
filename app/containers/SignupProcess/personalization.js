import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as P from './personalizationStyle';
import * as c from './constants';
import Filter from '../../components/Filter';
import ArtistSearchItem from '../../components/ArtistSearchItem';
import Button from '../../components/Button';
import * as u from '../../config/url';
import ErrorBoundary from '../../components/ErrorBoundary';

class Personalization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'Likes',
      artists: c.artistLists,
    };
  }

  handleActive = activeTab => {
    this.setState({ activeTab });
  };

  render() {
    const { activeTab, artists } = this.state;
    const tabs = ['Likes', 'Subscriptions'];
    const {
      handleSteps,
      history,
      selectedArtists,
      addArtist,
      removeArtist,
    } = this.props;
    return (
      <P.Wrapper>
        <P.Title>Last Step! Manage artists to follow</P.Title>
        <P.SubTitle>
          Below artists were pulled from your social accounts
        </P.SubTitle>
        <P.AddArtistsButton>Add Artist Manually +</P.AddArtistsButton>
        <P.Tabs>
          {tabs.map(item => (
            <P.Tab
              key={item}
              active={activeTab === item}
              onClick={() => {
                this.handleActive(item);
              }}
            >
              {item}
            </P.Tab>
          ))}
        </P.Tabs>
        <P.SearchWrapper>
          <P.SearchInputWrapper space={selectedArtists.length}>
            <P.SearchIcon src={c.searchIcon} />
            <P.SearchResults>
              {selectedArtists.map(item => (
                <P.SearchResult key={item.id}>
                  {item.name}
                  <P.SearchResultClose
                    onClick={() => {
                      removeArtist(item);
                    }}
                  >
                    x
                  </P.SearchResultClose>
                </P.SearchResult>
              ))}
            </P.SearchResults>
            {/* <P.Searchinput /> */}
          </P.SearchInputWrapper>
          <ErrorBoundary>
            <Filter />
          </ErrorBoundary>
        </P.SearchWrapper>
        <P.ArtistResults>
          {artists.map(item => (
            <P.ArtistResult key={item.id}>
              <ErrorBoundary>
                <ArtistSearchItem
                  item={item}
                  addArtist={addArtist}
                  removeArtist={removeArtist}
                  selectedArtists={selectedArtists}
                />
              </ErrorBoundary>
            </P.ArtistResult>
          ))}
        </P.ArtistResults>
        <S.StepButtons>
          <ErrorBoundary>
            <Button
              type="button"
              text="Cancel"
              bgColor="#696969"
              textColor="white"
              onClick={() => {
                history.push(u.homepage);
              }}
            />
          </ErrorBoundary>
          <ErrorBoundary>
            <Button
              type="button"
              text="Back"
              bgColor="white"
              textColor="black"
              onClick={() => {
                handleSteps(1);
              }}
            />
          </ErrorBoundary>
          <ErrorBoundary>
            <Button
              text="Save"
              onClick={() => {
                history.push(u.signupSuccess);
              }}
            />
          </ErrorBoundary>
        </S.StepButtons>
      </P.Wrapper>
    );
  }
}

Personalization.propTypes = {
  handleSteps: PropTypes.func,
  history: PropTypes.object,
  selectedArtists: PropTypes.array,
  addArtist: PropTypes.func,
  removeArtist: PropTypes.func,
};

export default Personalization;
