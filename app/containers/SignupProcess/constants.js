/* eslint-disable no-plusplus */
/* eslint-disable global-require */
import { apiUrl } from '../../config/apikey';
export const signupClose = `${apiUrl}/images/login_close.png`;
export const rightArrow = `${apiUrl}/images/right-arrow-active.svg`;
export const activeTick = `${apiUrl}/images/active-tick.svg`;
export const imageInputIcon = `${apiUrl}/images/image-input-icon.svg`;
export const dummyProfilePic = `${apiUrl}/images/dummy-profile-pic.png`;
export const eye = `${apiUrl}/images/login_eye.png`;
export const rightStepArrow = `${apiUrl}/images/right-arrow.svg`;
export const searchIcon = `${apiUrl}/images/genre_search.png`;

export const leftTabButtons = [
  { title: 'Create Profile', note: '' },
  { title: 'Social Accounts', note: '(Optional)' },
  { title: 'Add Personalization', note: '' },
];

export const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
const years = [];
for (let i = new Date().getFullYear(); i >= 1950; i--) {
  years.push(i);
}
const dates = [];
for (let i = 1; i <= 31; i++) {
  dates.push(i);
}
export { years, dates };

export const artistLists = [
  {
    id: 1,
    name: 'Metallica',
    social: 'Facebook',
    image: `${apiUrl}/images/artist_01.png`,
  },
  {
    id: 2,
    name: 'The Beatles',
    social: 'Spotify',
    image: `${apiUrl}/images/artist_02.png`,
  },
  {
    id: 3,
    name: 'The Rolling Stones',
    social: 'Bandsintown',
    image: `${apiUrl}/images/artist_03.png`,
  },
  {
    id: 4,
    name: 'Shura',
    social: 'Bandsintown',
    image: `${apiUrl}/images/artist_04.png`,
  },
  {
    id: 5,
    name: 'The Chainsmokers',
    social: 'Spotify',
    image: `${apiUrl}/images/artist_05.png`,
  },
  {
    id: 6,
    name: 'Eminem',
    social: 'Bandsintown',
    image: `${apiUrl}/images/artist_06.png`,
  },
  {
    id: 7,
    name: 'Justin Bieber',
    social: 'Facebook',
    image: `${apiUrl}/images/artist_07.png`,
  },
  {
    id: 8,
    name: 'Drake',
    social: 'Spotify',
    image: `${apiUrl}/images/artist_08.png`,
  },
];

export const socialItems = [
  {
    title: 'Facebook',
    icon: `${apiUrl}/images/facebook.svg`,
  },
  {
    title: 'Instagram',
    icon: `${apiUrl}/images/instagram.svg`,
  },
  {
    title: 'Twitter',
    icon: `${apiUrl}/images/twitter.svg`,
  },
  {
    title: 'Spotify',
    icon: `${apiUrl}/images/spotify.svg`,
  },
  {
    title: 'Bandsintown',
    icon: `${apiUrl}/images/bandsintown.svg`,
  },
];
