import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, FormGroup, Label } from 'reactstrap';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import Button from '../../components/Button';

const signUpSchema = Yup.object().shape({
  firstname: Yup.string().required('Firstname is required'),
  lastname: Yup.string().required('Lastname is required'),
  gender: Yup.string(),
  birthDate: Yup.string(),
  birthMonth: Yup.string(),
  birthYear: Yup.string(),
  email: Yup.string()
    .email('Email is invalid')
    .required('Email is required'),
  password: Yup.string()
    .min(6, 'Password must be at least 6 characters')
    .required('Password is required'),
});

const CreateProfile = props => {
  const {
    firstname,
    lastname,
    gender,
    birthDate,
    birthMonth,
    birthYear,
    email,
    password,
    updateProfileDetails,
    profileCreationError,
  } = props;
  const { history } = props;
  const [viewPass, setViewPass] = useState(false);
  return (
    <S.SignupStep1Form>
      <Formik
        initialValues={{
          firstname,
          lastname,
          gender,
          birthDate,
          birthMonth,
          birthYear,
          email,
          password,
        }}
        validationSchema={signUpSchema}
        onSubmit={values => {
          updateProfileDetails(values);
        }}
      >
        {({ errors, touched, values }) => (
          <Form>
            <Row>
              <Col md="6">
                <FormGroup>
                  <Label>First Name*</Label>
                  <Field
                    name="firstname"
                    type="text"
                    className={`form-control${
                      errors.firstname && touched.firstname ? ' is-invalid' : ''
                    }`}
                    autoComplete="off"
                    placeholder="Enter your firstname"
                  />
                  <ErrorMessage
                    name="firstname"
                    component="div"
                    className="invalid-feedback"
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <Label>Last Name*</Label>
                  <Field
                    name="lastname"
                    type="text"
                    className={`form-control${
                      errors.lastname && touched.lastname ? ' is-invalid' : ''
                    }`}
                    autoComplete="off"
                    placeholder="Enter your lastname"
                  />
                  <ErrorMessage
                    name="lastname"
                    component="div"
                    className="invalid-feedback"
                  />
                </FormGroup>
              </Col>
              <Col md="12">
                <FormGroup>
                  <Label>Your Gender*</Label>
                  <S.Inlineradio>
                    {/* {['Male', 'Female', 'Other'].map(item => (
                      <CustomInput
                        key={item}
                        type="radio"
                        id={item}
                        label={item}
                        name="gender"
                        value={item}
                        checked={item === values.gender}
                      />
                    ))} */}
                    {['Male', 'Female', 'Other'].map(item => (
                      <div className="custom-control custom-radio" key={item}>
                        <Field
                          className="custom-control-input"
                          type="radio"
                          id={item}
                          name="gender"
                          value={item}
                          checked={item === values.gender}
                        />
                        <Label className="custom-control-label" htmlFor={item}>
                          {item}
                        </Label>
                      </div>
                    ))}
                  </S.Inlineradio>
                </FormGroup>
              </Col>
              <Col md="12">
                <FormGroup>
                  <Label>Date Of Birth*</Label>
                  <Row>
                    <Col md="3">
                      <Field
                        name="birthDate"
                        as="select"
                        className="form-control"
                      >
                        {c.dates.map(item => (
                          <option value={item} key={item}>
                            {item}
                          </option>
                        ))}
                      </Field>
                    </Col>
                    <Col md="6">
                      <Field
                        name="birthMonth"
                        as="select"
                        className="form-control"
                      >
                        {c.months.map(item => (
                          <option value={item} key={item}>
                            {item}
                          </option>
                        ))}
                      </Field>
                    </Col>
                    <Col md="3">
                      <Field
                        name="birthYear"
                        as="select"
                        className="form-control"
                      >
                        {c.years.map(item => (
                          <option value={item} key={item}>
                            {item}
                          </option>
                        ))}
                      </Field>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <Label>Email Id*</Label>
                  <Field
                    name="email"
                    type="text"
                    className={`form-control${
                      errors.email && touched.email ? ' is-invalid' : ''
                    }`}
                    autoComplete="off"
                    placeholder="Enter your email id"
                  />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="invalid-feedback"
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <S.PasswordField>
                  <FormGroup>
                    <Label>Password*</Label>
                    <Field
                      name="password"
                      type={viewPass ? 'text' : 'password'}
                      className={`form-control${
                        errors.password && touched.password ? ' is-invalid' : ''
                      }`}
                      placeholder="Enter your password"
                    />
                    <ErrorMessage
                      name="password"
                      component="div"
                      className="invalid-feedback"
                    />
                  </FormGroup>
                  <S.ViewPassword
                    onClick={() => {
                      setViewPass(!viewPass);
                    }}
                  >
                    <S.EyeIcon src={c.eye} alt="" />
                  </S.ViewPassword>
                </S.PasswordField>
              </Col>
            </Row>
            {profileCreationError && (
              <S.CustomError>{profileCreationError}</S.CustomError>
            )}
            <S.StepButtons>
              <Button
                type="button"
                text="Cancel"
                bgColor="#696969"
                textColor="white"
                onClick={() => {
                  history.push(u.homepage);
                }}
              />
              <Button text="Next" />
            </S.StepButtons>
          </Form>
        )}
      </Formik>
    </S.SignupStep1Form>
  );
};

CreateProfile.propTypes = {
  firstname: PropTypes.string,
  lastname: PropTypes.string,
  gender: PropTypes.string,
  birthDate: PropTypes.string,
  birthMonth: PropTypes.string,
  birthYear: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  history: PropTypes.object,
  updateProfileDetails: PropTypes.func,
  profileCreationError: PropTypes.string,
};

export default CreateProfile;
