/**
 *
 * Asynchronously loads the component for SignupProcess
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
