import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';
import * as u from '../../config/url';
import SocialConnect from '../../components/SocialConnect';
import Button from '../../components/Button';
import ErrorBoundary from '../../components/ErrorBoundary';

class SocialAccounts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allSocialAccounts: c.socialItems,
    };
  }

  render() {
    const {
      handleSteps,
      history,
      socialAccounts,
      handleSocialConnect,
      handleSocialDisconnect,
    } = this.props;
    const { allSocialAccounts } = this.state;
    return (
      <S.ConnectSocialWrapper>
        <S.ConnectSocialSkipWrapper>
          <S.ConnectSocialSkipBtn
            onClick={() => {
              handleSteps(2);
            }}
          >
            Skip
          </S.ConnectSocialSkipBtn>
        </S.ConnectSocialSkipWrapper>
        <S.ConnectSocialTitle>
          Connect your social accounts with Trephoria
          <S.ConnectSocialSubTitle>(Optional)</S.ConnectSocialSubTitle>
        </S.ConnectSocialTitle>
        <S.ConnectSocialItems>
          {allSocialAccounts.map(item => (
            <S.ConnectSocialItem key={item.title}>
              <ErrorBoundary>
                <SocialConnect
                  item={item}
                  selectedSocialAccounts={socialAccounts}
                  handleSocialConnect={handleSocialConnect}
                  handleSocialDisconnect={handleSocialDisconnect}
                />
              </ErrorBoundary>
            </S.ConnectSocialItem>
          ))}
        </S.ConnectSocialItems>
        <S.StepButtons>
          <ErrorBoundary>
            <Button
              type="button"
              text="Cancel"
              bgColor="#696969"
              textColor="white"
              onClick={() => {
                history.push(u.homepage);
              }}
            />
          </ErrorBoundary>
          <ErrorBoundary>
            <Button
              type="button"
              text="Back"
              bgColor="white"
              textColor="black"
              onClick={() => {
                handleSteps(0);
              }}
            />
          </ErrorBoundary>
          <ErrorBoundary>
            <Button
              type="button"
              text="Next"
              onClick={() => {
                handleSteps(2);
              }}
            />
          </ErrorBoundary>
        </S.StepButtons>
      </S.ConnectSocialWrapper>
    );
  }
}

SocialAccounts.propTypes = {
  handleSteps: PropTypes.func,
  history: PropTypes.object,
  socialAccounts: PropTypes.array,
  handleSocialConnect: PropTypes.func,
  handleSocialDisconnect: PropTypes.func,
};

export default SocialAccounts;
