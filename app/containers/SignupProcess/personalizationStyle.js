import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-bottom: 30px;
`;

export const Title = styled.h2`
  font-size: 12px;
  font-weight: bold;
  margin: 0 0 10px;
`;

export const SubTitle = styled.p`
  font-size: 10px;
  color: #9c9c9c;
  margin: 0 0 10px;
`;

export const AddArtistsButton = styled.button`
  border-radius: 6px;
  border: solid 1px #983fcc;
  font-size: 10px;
  outline: 0 !important;
  background: transparent;
  padding: 3px 10px;
  margin: 0 0 20px;
  color: #983fcc;
`;

export const Tabs = styled.div`
  position: relative;
  margin: 0 0 20px;
  :before {
    content: '';
    position: absolute;
    background: rgba(0, 0, 0, 0.1);
    width: calc(100% + 80px);
    height: 1px;
    bottom: 0;
    left: -30px;
  }
`;

export const Tab = styled.button`
  font-size: 10px;
  color: ${props => (props.active ? '#040404' : '#9c9c9c')};
  background: transparent;
  outline: 0 !important;
  border: 0;
  padding: 0 10px 8px;
  margin-right: 10px;
  border-bottom: 1px solid
    ${props => (props.active ? '#ff5853' : 'transparent')};
`;

export const SearchWrapper = styled.div`
  > button {
    float: right;
    margin: 4px 0 0;
  }
`;

export const SearchInputWrapper = styled.div`
  width: calc(100% - 85px);
  display: inline-block;
  background: #f2f2f2;
  border-radius: 15px;
  position: relative;
  padding: ${props => (props.space ? '5px 20px 0 40px' : '5px 20px 6px 40px')};
`;

export const SearchIcon = styled.img`
  position: absolute;
  left: 15px;
  width: 13px;
  top: 12px;
`;

export const SearchResults = styled.div`
  display: inline-block;
`;

export const SearchResult = styled.div`
  border-radius: 15px;
  border: solid 1px #00a3e0;
  color: #00a3e0;
  font-size: 10px;
  padding: 3px 10px;
  display: inline-block;
  background: white;
  margin: 0 5px 5px 0;
`;

export const SearchResultClose = styled.button`
  background: transparent;
  border: 0;
  outline: 0 !important;
  padding: 0;
  vertical-align: top;
  margin: 0 0 0 10px;
`;

export const Searchinput = styled.input`
  display: inline-block;
  width: 100px;
  border: 0 !important;
  outline: 0 !important;
  background: transparent;
  font-size: 10px;
  margin-bottom: 5px;
  height: 23px;
`;

export const ArtistResults = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 20px -10px 0;
`;

export const ArtistResult = styled.div`
  width: 25%;
  padding: 0 10px;
  margin-bottom: 20px;
`;
