import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';

const ProfilePicture = props => {
  const { handleProfilePic, profilePicPreview } = props;
  const [imagePreview, setImagePreview] = useState('');
  const [error, setError] = useState('');
  const handleChange = e => {
    const file = e.target.files[0];
    if (file) {
      if (!['image/jpeg', 'image/png'].includes(file.type)) {
        setError('Only jpeg & png is allowed');
      } else if (file.size / 1000000 > 1) {
        setError('Max upload size is 1MB');
      } else {
        const reader = new FileReader();
        reader.addEventListener('load', () => {
          setImagePreview(reader.result);
          setError('');
          handleProfilePic(file, reader.result);
        });
        reader.readAsDataURL(file);
      }
    }
  };
  return (
    <S.ProfilePicWrapper>
      <S.ProfilePicInputWrapper>
        <S.ProfilePicInput type="file" onChange={handleChange} />
        <S.ProfilePicInputicon src={c.imageInputIcon} />
      </S.ProfilePicInputWrapper>
      <S.ProfilePicImage
        src={profilePicPreview || imagePreview || c.dummyProfilePic}
      />
      {error && <S.ProfilePicError>{error}</S.ProfilePicError>}
    </S.ProfilePicWrapper>
  );
};

ProfilePicture.propTypes = {
  handleProfilePic: PropTypes.func,
  profilePicPreview: PropTypes.string,
};

export default ProfilePicture;
