import React from 'react';
import PropTypes from 'prop-types';
import * as S from './style';
import * as c from './constants';

const LeftTabs = props => {
  const { step, handleSteps } = props;
  return (
    <S.LeftTabsWrapper>
      {c.leftTabButtons.map((item, index) => (
        <S.LeftTabButton
          // eslint-disable-next-line react/no-array-index-key
          key={index}
          index={index}
          step={step}
          onClick={() => {
            handleSteps(index);
          }}
        >
          {step > index && <S.SuccessIcon src={c.activeTick} />}
          {item.title}
          {item.note && <S.LeftTabButtonNote>{item.note}</S.LeftTabButtonNote>}
          {step === index && <S.RightIcon src={c.rightArrow} />}
        </S.LeftTabButton>
      ))}
    </S.LeftTabsWrapper>
  );
};

LeftTabs.propTypes = {
  step: PropTypes.number,
  handleSteps: PropTypes.func,
};

export default LeftTabs;
