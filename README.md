
## Quick start

1.  Make sure that you have Node.js v8.15.1 and npm v5 or above installed.
2.  run `npm start` to see the app at `http://localhost:3000`._
3.  Run `npm run clean` to delete the example app.